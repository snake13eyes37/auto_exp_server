// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_car_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CreateCarModel _$CreateCarModelFromJson(Map<String, dynamic> json) =>
    CreateCarModel(
      vin: json['vin'] as String,
      modelId: (json['modelId'] as num).toInt(),
      createdDate: DateTime.parse(json['createdDate'] as String),
      range: (json['range'] as num).toDouble(),
      voluem: (json['voluem'] as num).toInt(),
      engineId: (json['engineId'] as num).toInt(),
    );

Map<String, dynamic> _$CreateCarModelToJson(CreateCarModel instance) =>
    <String, dynamic>{
      'vin': instance.vin,
      'modelId': instance.modelId,
      'createdDate': instance.createdDate.toIso8601String(),
      'range': instance.range,
      'voluem': instance.voluem,
      'engineId': instance.engineId,
    };

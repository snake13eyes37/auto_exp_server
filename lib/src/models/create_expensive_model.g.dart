// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_expensive_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CreateExpensiveModel _$CreateExpensiveModelFromJson(
        Map<String, dynamic> json) =>
    CreateExpensiveModel(
      categoryId: (json['categoryId'] as num).toInt(),
      description: json['description'] as String,
      name: json['name'] as String,
      createdDate: DateTime.parse(json['createdDate'] as String),
      range: (json['range'] as num).toDouble(),
      price: (json['price'] as num).toDouble(),
      liters: (json['liters'] as num?)?.toInt(),
      fuelTypeId: (json['fuelTypeId'] as num?)?.toInt(),
    );

Map<String, dynamic> _$CreateExpensiveModelToJson(
        CreateExpensiveModel instance) =>
    <String, dynamic>{
      'categoryId': instance.categoryId,
      'description': instance.description,
      'name': instance.name,
      'createdDate': instance.createdDate.toIso8601String(),
      'range': instance.range,
      'price': instance.price,
      'liters': instance.liters,
      'fuelTypeId': instance.fuelTypeId,
    };

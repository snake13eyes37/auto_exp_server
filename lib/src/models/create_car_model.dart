import 'package:json_annotation/json_annotation.dart';

part 'create_car_model.g.dart';

@JsonSerializable()
class CreateCarModel {
  CreateCarModel({
    required this.vin,
    required this.modelId,
    required this.createdDate,
    required this.range,
    required this.voluem,
    required this.engineId,
  });

  final String vin;
  final int modelId;
  final DateTime createdDate;
  final double range;
  final int voluem;
  final int engineId;

  factory CreateCarModel.fromJson(Map<String, dynamic> json) =>
      _$CreateCarModelFromJson(json);

  Map<String, dynamic> toJson() => _$CreateCarModelToJson(this);
}

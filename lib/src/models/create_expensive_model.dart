import 'package:json_annotation/json_annotation.dart';

part 'create_expensive_model.g.dart';

@JsonSerializable()
class CreateExpensiveModel {
  CreateExpensiveModel({
    required this.categoryId,
    required this.description,
    required this.name,
    required this.createdDate,
    required this.range,
    required this.price,
    this.liters,
    this.fuelTypeId,
  });

  final int categoryId;
  final String description;
  final String name;
  final DateTime createdDate;
  final double range;
  final double price;
  final int? liters;
  final int? fuelTypeId;

  factory CreateExpensiveModel.fromJson(Map<String, dynamic> json) =>
      _$CreateExpensiveModelFromJson(json);

  Map<String, dynamic> toJson() => _$CreateExpensiveModelToJson(this);
}

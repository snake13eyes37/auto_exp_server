import 'dart:io';

import 'package:dart_frog/dart_frog.dart';
import 'package:orm/orm.dart';

import '../../prisma/generated_dart_client/client.dart';
import '../../prisma/generated_dart_client/prisma.dart';
import '../index.dart';

Future<Response> onRequest(RequestContext context) {
  return switch (context.request.method) {
    HttpMethod.get => _getUser(context),
    HttpMethod.put => _updateUser(context),
    _ => Future.value(Response(statusCode: HttpStatus.badRequest))
  };
}

//
Future<Response> _getUser(RequestContext context) async {
  final prisma = context.read<PrismaClient>();
  try {
    final user = await prisma.user.findFirst(
      where: UserWhereInput(
        id: PrismaUnion.$1(IntFilter(equals: getUserIdFromToken(context))),
      ),
    );

    return Response.json(body: user?.toJson());
  } catch (e) {
    return Response(statusCode: HttpStatus.badRequest);
  }
}

Future<Response> _updateUser(RequestContext context) async {
  final prisma = context.read<PrismaClient>();
  final requestBody =
      await (await context.request.json()) as Map<String, dynamic>;

  try {
    final model = UserUpdateInput(
      name: requestBody['name'] != null
          ? PrismaUnion.$2(
              StringFieldUpdateOperationsInput(
                set: requestBody['name'] as String,
              ),
            )
          : null,
      phone: requestBody['phone'] != null
          ? PrismaUnion.$2(
              StringFieldUpdateOperationsInput(
                set: requestBody['phone'] as String,
              ),
            )
          : null,
      birthday: requestBody['birthday'] != null
          ? PrismaUnion.$2(
              DateTimeFieldUpdateOperationsInput(
                set: DateTime.tryParse(requestBody['birthday'] as String),
              ),
            )
          : null,
    );
    final dbModel = await prisma.user.update(
      data: PrismaUnion.$1(model),
      where: UserWhereUniqueInput(
        id: getUserIdFromToken(context),
      ),
    );
    return Response.json(body: dbModel);
  } catch (e) {
    print(e.toString());
    return Response(statusCode: HttpStatus.badRequest);
  }
}

import 'package:dart_frog/dart_frog.dart';
import 'package:dart_frog_auth/dart_frog_auth.dart';
import 'package:dart_jsonwebtoken/dart_jsonwebtoken.dart';
import 'package:orm/orm.dart';
import 'package:shelf_cors_headers/shelf_cors_headers.dart';

import '../../prisma/generated_dart_client/client.dart';
import '../../prisma/generated_dart_client/prisma.dart';
import '../index.dart';

Handler middleware(Handler handler) {
  return handler.use(
    bearerAuthentication<int>(
      authenticator: (context, token) async {
        final prisma = context.read<PrismaClient>();
        try {
          final newTok = JWT.verify(token, SecretKey("Qwer1234"));
          print("newTOken: ${newTok}  \n${token}");
          final user = await prisma.user.findFirst(
            where: UserWhereInput(
              id: PrismaUnion.$1(IntFilter(equals: newTok.payload as int)),
            ),
          );
          if (user != null) {
            EnvironmentValue.instance().userId = user.id;
            print(EnvironmentValue.instance().userId);
            return user.id;
          } else {
            return null;
          }
        } catch (e) {
          return null;
        }
      },
    ),
  ).use(
    fromShelfMiddleware(
      corsHeaders(
        headers: {
          ACCESS_CONTROL_ALLOW_ORIGIN: '*',
          'Content-Type': 'application/json',
          'Accept': '*/*',
        },
      ),
    ),
  );
}

import 'dart:io';

import 'package:dart_frog/dart_frog.dart';
import 'package:orm/orm.dart';

import '../../prisma/generated_dart_client/client.dart';
import '../../prisma/generated_dart_client/prisma.dart';

Future<Response> onRequest(RequestContext context) {
  return switch (context.request.method) {
    HttpMethod.get => _getCategories(context),
    HttpMethod.post => _createCategories(context),
    HttpMethod.delete => _deleteCategories(context),
    HttpMethod.put => _updateCategories(context),
    _ => Future.value(Response(statusCode: HttpStatus.badRequest))
  };
}

Future<Response> _getCategories(RequestContext context) async {
  final prisma = context.read<PrismaClient>();
  try {
    final expensives = await prisma.category.findMany();
    final todos = [...expensives.map((e) => e.toJson())];
    return Response.json(body: todos);
  } catch (e) {
    return Response(statusCode: HttpStatus.badRequest, body: e.toString());
  }
}

//
Future<Response> _createCategories(RequestContext context) async {
  final prisma = context.read<PrismaClient>();
  final requestBody =
      await (await context.request.json()) as Map<String, dynamic>;

  try {
    final newModel = CategoryCreateInput(name: requestBody['name'] as String);
    final dbModel =
        await prisma.category.create(data: PrismaUnion.$1(newModel));
    return Response.json(body: dbModel);
  } catch (e) {
    return Response(statusCode: HttpStatus.badRequest, body: e.toString());
  }
}

//
Future<Response> _updateCategories(RequestContext context) async {
  final prisma = context.read<PrismaClient>();
  final requestBody =
      await (await context.request.json()) as Map<String, dynamic>;

  try {
    final id = context.request.uri.queryParameters['id']!;
    final model = CategoryUpdateInput(
      name: PrismaUnion.$2(
        StringFieldUpdateOperationsInput(set: requestBody['name'] as String),
      ),
    );

    final dbModel = await prisma.category.update(
      data: PrismaUnion.$1(model),
      where: CategoryWhereUniqueInput(id: int.tryParse(id)),
    );
    return Response.json(body: dbModel);
  } catch (e) {
    print(e.toString());
    return Response(statusCode: HttpStatus.badRequest, body: e.toString());
  }
}

//
Future<Response> _deleteCategories(RequestContext context) async {
  final prisma = context.read<PrismaClient>();

  try {
    final id = context.request.uri.queryParameters["id"]!;
    final dbModel = await prisma.category.delete(
      where: CategoryWhereUniqueInput(
        id: int.tryParse(id),
      ),
    );
    return Response.json(body: dbModel);
  } catch (e) {
    return Response(statusCode: HttpStatus.badRequest, body: e.toString());
  }
}

import 'dart:io';

import 'package:auto_exp_server/src/models/create_expensive_model.dart';
import 'package:dart_frog/dart_frog.dart';
import 'package:orm/orm.dart';

import '../../prisma/generated_dart_client/client.dart';
import '../../prisma/generated_dart_client/model.dart';
import '../../prisma/generated_dart_client/prisma.dart';
import '../index.dart';

Future<Response> onRequest(RequestContext context) {
  return switch (context.request.method) {
    HttpMethod.get => _getExpensives(context),
    HttpMethod.post => _createExpensive(context),
    HttpMethod.delete => _deleteExpensive(context),
    HttpMethod.put => _updateExpensive(context),
    _ => Future.value(Response(statusCode: HttpStatus.badRequest))
  };
}

//
Future<Response> _getExpensives(RequestContext context) async {
  final prisma = context.read<PrismaClient>();
  try {
    final expensives = await prisma.expensive.findMany(
      where: ExpensiveWhereInput(
        userId: PrismaUnion.$1(IntFilter(equals: getUserIdFromToken(context))),
        user: PrismaUnion.$2(
          UserWhereInput(
            id: PrismaUnion.$1(
              IntFilter(
                equals: getUserIdFromToken(context),
              ),
            ),
          ),
        ),
      ),
    );
    final todos = [...expensives.map((e) => e.toJson())];
    return Response.json(body: todos);
  } catch (e) {
    return Response(statusCode: HttpStatus.badRequest);
  }
}

//
Future<Response> _createExpensive(RequestContext context) async {
  final prisma = context.read<PrismaClient>();
  final requestBody =
      await (await context.request.json()) as Map<String, dynamic>;

  try {
    final newModel = CreateExpensiveModel.fromJson(requestBody);
    final dbModel = await prisma.expensive.create(
      data: PrismaUnion.$1(
        ExpensiveCreateInput(
          description: newModel.description,
          name: newModel.name,
          createdDate: newModel.createdDate,
          range: newModel.range,
          price: newModel.price,
          user: UserCreateNestedOneWithoutExpensiveInput(
            connect: UserWhereUniqueInput(
              id: getUserIdFromToken(context),
            ),
          ),
          category: CategoryCreateNestedOneWithoutExpensiveInput(
            connect: CategoryWhereUniqueInput(
              id: newModel.categoryId,
            ),
          ),
          fuel: newModel.fuelTypeId != null
              ? FuelCreateNestedOneWithoutExpensiveInput(
                  connect: FuelWhereUniqueInput(
                    id: newModel.fuelTypeId,
                  ),
                )
              : null,
          liters: newModel.liters != null
              ? PrismaUnion.$1(newModel.liters!.toDouble())
              : const PrismaUnion.$2(PrismaNull()),
        ),
      ),
    );
    return Response.json(body: dbModel);
  } catch (e) {
    print(e.toString());
    return Response(statusCode: HttpStatus.badRequest);
  }
}

//
Future<Response> _updateExpensive(RequestContext context) async {
  final prisma = context.read<PrismaClient>();
  final requestBody =
      await (await context.request.json()) as Map<String, dynamic>;

  try {
    final id = context.request.uri.queryParameters['id']! as String;
    final model = ExpensiveUpdateInput(
      createdDate: requestBody['date'] != null
          ? PrismaUnion.$2(
              DateTimeFieldUpdateOperationsInput(
                set: DateTime.tryParse(
                  requestBody['date'] as String,
                ),
              ),
            )
          : null,
      name: requestBody['name'] != null
          ? PrismaUnion.$2(
              StringFieldUpdateOperationsInput(
                set: requestBody['name'] as String,
              ),
            )
          : null,
      description: requestBody['description'] != null
          ? PrismaUnion.$2(
              StringFieldUpdateOperationsInput(
                set: requestBody['description'] as String,
              ),
            )
          : null,
      price: requestBody['price'] != null
          ? PrismaUnion.$2(
              FloatFieldUpdateOperationsInput(
                set: requestBody['price'] as double,
              ),
            )
          : null,
      range: requestBody['range'] != null
          ? PrismaUnion.$2(
              FloatFieldUpdateOperationsInput(
                set: requestBody['range'] as double,
              ),
            )
          : null,
      category: requestBody['categoryId'] != null
          ? CategoryUpdateOneRequiredWithoutExpensiveNestedInput(
              connect: CategoryWhereUniqueInput(
                id: requestBody['categoryId'] as int,
              ),
            )
          : null,
      liters: requestBody['liters'] != null
          ? PrismaUnion.$2(
              PrismaUnion.$1(
                NullableFloatFieldUpdateOperationsInput(
                  set: PrismaUnion.$1(requestBody['liters'] as double),
                ),
              ),
            )
          : null,
      fuel: requestBody['fuelTypeId'] != null
          ? FuelUpdateOneWithoutExpensiveNestedInput(
              connect: FuelWhereUniqueInput(
                id: requestBody['fuelTypeId'] as int,
              ),
            )
          : null,
    );
    final dbModel = await prisma.expensive.update(
      data: PrismaUnion.$1(model),
      where: ExpensiveWhereUniqueInput(
        id: int.tryParse(id),
      ),
    );
    return Response.json(body: dbModel);
  } catch (e) {
    print(e.toString());
    return Response(statusCode: HttpStatus.badRequest);
  }
}

//
Future<Response> _deleteExpensive(RequestContext context) async {
  final prisma = context.read<PrismaClient>();

  try {
    final id = context.request.uri.queryParameters["id"]! as String;
    final dbModel = await prisma.expensive.delete(
      where: ExpensiveWhereUniqueInput(
        id: int.tryParse(id),
      ),
    );
    return Response.json(body: dbModel);
  } catch (e) {
    return Response(statusCode: HttpStatus.badRequest, body: e.toString());
  }
}

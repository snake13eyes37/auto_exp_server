
import 'package:dart_frog/dart_frog.dart';
import 'package:shelf_cors_headers/shelf_cors_headers.dart';

import '../prisma/generated_dart_client/client.dart';

final _prisma = PrismaClient(
  // stdout: ['query', 'info', 'warn', 'error'],
  //datasourceUrl:
  // stdout: Event.values, // print all events to the console
  // datasources: {
  //   'db': 'postgresql://aleksandrpanin:qwer1234@localhost:5432/postgres?schema=public',
  // },
);

Handler middleware(Handler handler) {
  return handler
      .use(requestLogger())
      .use(
        fromShelfMiddleware(
          corsHeaders(
            headers: {
              ACCESS_CONTROL_ALLOW_METHODS: '*',
              ACCESS_CONTROL_ALLOW_CREDENTIALS: '*',
              ACCESS_CONTROL_ALLOW_HEADERS: '*',
              ACCESS_CONTROL_ALLOW_ORIGIN: '*',
              'Content-Type': 'application/json',
              'Accept': '*/*',
            },
          ),
        ),
      )
      .use(provider<PrismaClient>((context) => _prisma));
}

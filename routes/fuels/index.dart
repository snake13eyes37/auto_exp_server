import 'dart:io';

import 'package:dart_frog/dart_frog.dart';
import 'package:orm/orm.dart';

import '../../prisma/generated_dart_client/client.dart';
import '../../prisma/generated_dart_client/prisma.dart';

Future<Response> onRequest(RequestContext context) {
  return switch (context.request.method) {
    HttpMethod.get => _getFuels(context),
    HttpMethod.post => _createFuel(context),
    HttpMethod.delete => _deleteFuel(context),
    HttpMethod.put => _updateFuel(context),
    _ => Future.value(Response(statusCode: HttpStatus.badRequest))
  };
}

Future<Response> _getFuels(RequestContext context) async {
  final prisma = context.read<PrismaClient>();
  try {
    final expensives = await prisma.fuel.findMany();
    final todos = [...expensives.map((e) => e.toJson())];
    return Response.json(body: todos);
  } catch (e) {
    return Response(statusCode: HttpStatus.badRequest, body: e.toString());
  }
}

//
Future<Response> _createFuel(RequestContext context) async {
  final prisma = context.read<PrismaClient>();
  final requestBody =
      await (await context.request.json()) as Map<String, dynamic>;

  try {
    final newModel = FuelCreateInput(name: requestBody['name'] as String);
    final dbModel = await prisma.fuel.create(data: PrismaUnion.$1(newModel));
    return Response.json(body: dbModel);
  } catch (e) {
    return Response(statusCode: HttpStatus.badRequest, body: e.toString());
  }
}

//
Future<Response> _updateFuel(RequestContext context) async {
  final prisma = context.read<PrismaClient>();
  final requestBody =
      await (await context.request.json()) as Map<String, dynamic>;

  try {
    final id = context.request.uri.queryParameters['id']!;
    final model = FuelUpdateInput(
      name: PrismaUnion.$2(
        StringFieldUpdateOperationsInput(set: requestBody['name'] as String),
      ),
    );

    final dbModel = await prisma.fuel.update(
      data: PrismaUnion.$1(model),
      where: FuelWhereUniqueInput(id: int.tryParse(id)),
    );
    return Response.json(body: dbModel);
  } catch (e) {
    print(e.toString());
    return Response(statusCode: HttpStatus.badRequest, body: e.toString());
  }
}

//
Future<Response> _deleteFuel(RequestContext context) async {
  final prisma = context.read<PrismaClient>();

  try {
    final id = context.request.uri.queryParameters["id"]!;
    final dbModel = await prisma.fuel.delete(
      where: FuelWhereUniqueInput(id: int.tryParse(id)),
    );
    return Response.json(body: dbModel);
  } catch (e) {
    return Response(statusCode: HttpStatus.badRequest, body: e.toString());
  }
}

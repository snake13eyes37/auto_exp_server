import 'dart:io';

import 'package:dart_frog/dart_frog.dart';
import 'package:orm/orm.dart';

import '../prisma/generated_dart_client/client.dart';
import '../prisma/generated_dart_client/prisma.dart';

Future<Response> onRequest(RequestContext context) {
  return switch (context.request.method) {
    HttpMethod.post => _createExpensive(context),
    _ => Future.value(Response(statusCode: HttpStatus.notAcceptable))
  };
}

Future<Response> _createExpensive(RequestContext context) async {
  final prisma = context.read<PrismaClient>();
  final requestBody =
      await (await context.request.json()) as Map<String, dynamic>;
  print("success");

  try {
    final newModel = UserCreateInput(
      name: requestBody['name'] as String,
      phone: requestBody['phone'] as String,
      car: CarCreateNestedOneWithoutUserInput(
        create: PrismaUnion.$1(
          CarCreateWithoutUserInput(
            vin: requestBody['vin'] as String,
            range: requestBody['range'] as double,
            model: ModelCreateNestedOneWithoutCarInput(
              connect: ModelWhereUniqueInput(
                id: requestBody['modelId'] as int,
              ),
            ),
            createdDate: requestBody['createdDate'] as DateTime,
            voluem: requestBody['voluem'] as int,
            engine: EngineCreateNestedOneWithoutCarInput(
              connect: EngineWhereUniqueInput(
                id: requestBody['engineId'] as int,
              ),
            ),
          ),
        ),
      ),
      birthday: requestBody['birthday'] as DateTime,
    );
    final dbModel = await prisma.user.create(data: PrismaUnion.$1(newModel));
    return Response.json(body: dbModel);
  } catch (e) {
    print(e.toString());
    return Response(statusCode: HttpStatus.lengthRequired);
  }
}

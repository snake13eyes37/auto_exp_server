import 'dart:io';

import 'package:dart_frog/dart_frog.dart';
import 'package:dart_jsonwebtoken/dart_jsonwebtoken.dart';
import 'package:orm/orm.dart';

import '../prisma/generated_dart_client/client.dart';
import '../prisma/generated_dart_client/prisma.dart';

Future<Response> onRequest(RequestContext context) {
  return switch (context.request.method) {
    HttpMethod.post => _auth(context),
    _ => Future.value(Response(statusCode: HttpStatus.badRequest))
  };
}

Future<Response> _auth(RequestContext context) async {
  final prisma = context.read<PrismaClient>();
  final json = (await context.request.json()) as Map<String, dynamic>;
  final vin = json['vin'] as String?;
  print(vin);

  try {
    // final users = await prisma.user.findMany();
    // users.forEach((element) {
    //   print("${element.name}  ${element.password} \n");
    // });

    final cars = await prisma.car.findFirst(
        where: CarWhereInput(vin: PrismaUnion.$1(StringFilter(equals: vin))));
    final user = await prisma.user.findFirst(
      where: UserWhereInput(
        carId: PrismaUnion.$1(IntFilter(equals: cars?.id)),
      ),
    );
    if (user != null) {
      final jwt = JWT(user.id);
      final token = jwt.sign(SecretKey("Qwer1234"));
      final jsonMap = user.toJson();
      jsonMap.addAll({"token": token});
      return Response.json(body: jsonMap);
    } else {
      return Response(statusCode: HttpStatus.notFound, body: "user not found");
    }
  } catch (e) {
    return Response(statusCode: HttpStatus.badRequest, body: e.toString());
  }
}

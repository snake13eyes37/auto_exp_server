import 'dart:io';

import 'package:auto_exp_server/src/models/create_car_model.dart';
import 'package:dart_frog/dart_frog.dart';
import 'package:orm/orm.dart';

import '../../prisma/generated_dart_client/client.dart';
import '../../prisma/generated_dart_client/model.dart';
import '../../prisma/generated_dart_client/prisma.dart';
import '../index.dart';

Future<Response> onRequest(RequestContext context) {
  return switch (context.request.method) {
    HttpMethod.get => _getCar(context),
    HttpMethod.post => _createCar(context),
    HttpMethod.delete => _deleteCars(context),
    HttpMethod.put => _updateCars(context),
    _ => Future.value(Response(statusCode: HttpStatus.badRequest))
  };
}

//
Future<Response> _getCar(RequestContext context) async {
  final prisma = context.read<PrismaClient>();
  try {
    final user = await prisma.user.findFirst(
      where: UserWhereInput(
        id: PrismaUnion.$1(IntFilter(equals: getUserIdFromToken(context))),
      ),
    );

    final car = await prisma.car.findFirst(
      where: CarWhereInput(
        id: PrismaUnion.$1(
          IntFilter(equals: user?.carId),
        ),
      ),
    );

    return Response.json(body: car?.toJson());
  } catch (e) {
    return Response(statusCode: HttpStatus.badRequest);
  }
}

//
Future<Response> _createCar(RequestContext context) async {
  final prisma = context.read<PrismaClient>();
  final requestBody =
      await (await context.request.json()) as Map<String, dynamic>;

  try {
    final newModel = CreateCarModel.fromJson(requestBody);
    final dbModel = await prisma.car.create(
      data: PrismaUnion.$1(
        CarCreateInput(
          range: newModel.range,
          vin: newModel.vin,
          createdDate: newModel.createdDate,
          model: ModelCreateNestedOneWithoutCarInput(
            connect: ModelWhereUniqueInput(id: newModel.modelId),
          ),
          voluem: newModel.voluem,
          engine: EngineCreateNestedOneWithoutCarInput(
              connect: EngineWhereUniqueInput(id: newModel.engineId)),
        ),
      ),
    );
    final user = await prisma.user.findFirst(
      where: UserWhereInput(
        id: PrismaUnion.$1(IntFilter(equals: getUserIdFromToken(context))),
      ),
    );
    await prisma.user.update(
      data: PrismaUnion.$1(
        UserUpdateInput(
          car: CarUpdateOneRequiredWithoutUserNestedInput(
            connect: CarWhereUniqueInput(id: dbModel.id),
          ),
        ),
      ),
      where: UserWhereUniqueInput(id: user?.id),
    );
    return Response.json(body: dbModel);
  } catch (e) {
    print(e.toString());
    return Response(statusCode: HttpStatus.badRequest, body: e.toString());
  }
}

//
Future<Response> _updateCars(RequestContext context) async {
  final prisma = context.read<PrismaClient>();
  final requestBody =
      await (await context.request.json()) as Map<String, dynamic>;

  try {
    final user = await prisma.user.findFirst(
      where: UserWhereInput(
        id: PrismaUnion.$1(
          IntFilter(
            equals: getUserIdFromToken(context),
          ),
        ),
      ),
    );
    final model = CarUpdateInput(
      vin: requestBody['vin'] != null
          ? PrismaUnion.$2(StringFieldUpdateOperationsInput(
              set: requestBody['vin'] as String))
          : null,
      range: requestBody['range'] != null
          ? PrismaUnion.$2(FloatFieldUpdateOperationsInput(
              set: requestBody['range'] as double))
          : null,
      createdDate: requestBody['date'] != null
          ? PrismaUnion.$2(DateTimeFieldUpdateOperationsInput(
              set: DateTime.tryParse(requestBody['date'] as String),
            ))
          : null,
      model: requestBody['modelsId'] != null
          ? ModelUpdateOneRequiredWithoutCarNestedInput(
              connect: ModelWhereUniqueInput(
                id: requestBody['modelsId'] as int,
              ),
            )
          : null,
      voluem: requestBody['voluem'] != null
          ? PrismaUnion.$2(
              IntFieldUpdateOperationsInput(set: requestBody['voluem'] as int))
          : null,
      engine: requestBody['engineId'] != null
          ? EngineUpdateOneRequiredWithoutCarNestedInput(
              connect: EngineWhereUniqueInput(
                id: requestBody['engineId'] as int,
              ),
            )
          : null,
    );
    final dbModel = await prisma.car.update(
      data: PrismaUnion.$1(model),
      where: CarWhereUniqueInput(
        id: user?.carId,
      ),
    );
    return Response.json(body: dbModel);
  } catch (e) {
    print(e.toString());
    return Response(statusCode: HttpStatus.badRequest);
  }
}

//
Future<Response> _deleteCars(RequestContext context) async {
  final prisma = context.read<PrismaClient>();

  try {
    final id = context.request.uri.queryParameters["id"]! as String;
    final dbModel = await prisma.car.delete(
      where: CarWhereUniqueInput(
        id: int.tryParse(id),
      ),
    );
    return Response.json(body: dbModel);
  } catch (e) {
    return Response(statusCode: HttpStatus.badRequest, body: e.toString());
  }
}

import 'dart:io';

import 'package:dart_frog/dart_frog.dart';
import 'package:orm/orm.dart';

import '../../prisma/generated_dart_client/client.dart';
import '../../prisma/generated_dart_client/model.dart';
import '../../prisma/generated_dart_client/prisma.dart';

Future<Response> onRequest(RequestContext context) {
  return switch (context.request.method) {
    HttpMethod.get => _getCountries(context),
    HttpMethod.post => _createCountry(context),
    HttpMethod.delete => _deleteCountry(context),
    HttpMethod.put => _updateCountry(context),
    _ => Future.value(Response(statusCode: HttpStatus.badRequest))
  };
}

Future<Response> _getCountries(RequestContext context) async {
  final prisma = context.read<PrismaClient>();
  try {
    final expensives = await prisma.country.findMany();
    final todos = [...expensives.map((e) => e.toJson())];
    return Response.json(body: todos);
  } catch (e) {
    return Response(statusCode: HttpStatus.badRequest, body: e.toString());
  }
}

Future<Response> _createCountry(RequestContext context) async {
  final prisma = context.read<PrismaClient>();
  final requestBody =
      await (await context.request.json()) as Map<String, dynamic>;

  try {
    final newModel = CountryCreateInput(
      name: requestBody['name'] as String,
      code: requestBody['code'] as String,
    );
    final dbModel = await prisma.country.create(data: PrismaUnion.$1(newModel));
    return Response.json(body: dbModel);
  } catch (e) {
    return Response(statusCode: HttpStatus.badRequest, body: e.toString());
  }
}

//
Future<Response> _updateCountry(RequestContext context) async {
  final prisma = context.read<PrismaClient>();
  final requestBody =
      await (await context.request.json()) as Map<String, dynamic>;

  try {
    final id = context.request.uri.queryParameters['id']!;
    final model = CountryUpdateInput(
      name: requestBody['name'] != null
          ? PrismaUnion.$2(
              StringFieldUpdateOperationsInput(
                set: requestBody['name'] as String,
              ),
            )
          : null,
      code: requestBody['code'] != null
          ? PrismaUnion.$2(
              StringFieldUpdateOperationsInput(
                set: requestBody['code'] as String?,
              ),
            )
          : null,
    );

    final dbModel = await prisma.country.update(
      data: PrismaUnion.$1(model),
      where: CountryWhereUniqueInput(id: int.tryParse(id)),
    );
    return Response.json(body: dbModel);
  } catch (e) {
    print(e.toString());
    return Response(statusCode: HttpStatus.badRequest, body: e.toString());
  }
}

//
Future<Response> _deleteCountry(RequestContext context) async {
  final prisma = context.read<PrismaClient>();

  try {
    final id = context.request.uri.queryParameters["id"]!;
    final dbModel = await prisma.country.delete(
      where: CountryWhereUniqueInput(id: int.tryParse(id)),
    );
    return Response.json(body: dbModel);
  } catch (e) {
    return Response(statusCode: HttpStatus.badRequest, body: e.toString());
  }
}

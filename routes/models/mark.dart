import 'dart:io';

import 'package:dart_frog/dart_frog.dart';
import 'package:orm/orm.dart';

import '../../prisma/generated_dart_client/client.dart';
import '../../prisma/generated_dart_client/prisma.dart';

Future<Response> onRequest(RequestContext context) {
  return switch (context.request.method) {
    HttpMethod.get => _getMarks(context),
    HttpMethod.post => _createMarks(context),
    HttpMethod.delete => _deleteMarks(context),
    HttpMethod.put => _updateMarks(context),
    _ => Future.value(Response(statusCode: HttpStatus.badRequest))
  };
}

Future<Response> _getMarks(RequestContext context) async {
  final prisma = context.read<PrismaClient>();
  try {
    final expensives = await prisma.mark.findMany();
    final todos = [...expensives.map((e) => e.toJson())];
    return Response.json(body: todos);
  } catch (e) {
    return Response(statusCode: HttpStatus.badRequest, body: e.toString());
  }
}

//
Future<Response> _createMarks(RequestContext context) async {
  final prisma = context.read<PrismaClient>();
  final requestBody =
      await (await context.request.json()) as Map<String, dynamic>;

  try {
    final newModel = MarkCreateInput(name: requestBody['name'] as String);
    final dbModel = await prisma.mark.create(data: PrismaUnion.$1(newModel));
    return Response.json(body: dbModel);
  } catch (e) {
    return Response(statusCode: HttpStatus.badRequest, body: e.toString());
  }
}

//
Future<Response> _updateMarks(RequestContext context) async {
  final prisma = context.read<PrismaClient>();
  final requestBody =
      await (await context.request.json()) as Map<String, dynamic>;

  try {
    final id = context.request.uri.queryParameters['id']!;
    final model = MarkUpdateInput(
      name: PrismaUnion.$2(
        StringFieldUpdateOperationsInput(set: requestBody['name'] as String),
      ),
    );

    final dbModel = await prisma.mark.update(
      data: PrismaUnion.$1(model),
      where: MarkWhereUniqueInput(id: int.tryParse(id)),
    );
    return Response.json(body: dbModel);
  } catch (e) {
    print(e.toString());
    return Response(statusCode: HttpStatus.badRequest, body: e.toString());
  }
}

//
Future<Response> _deleteMarks(RequestContext context) async {
  final prisma = context.read<PrismaClient>();

  try {
    final id = context.request.uri.queryParameters["id"]!;
    final dbModel = await prisma.mark.delete(
      where: MarkWhereUniqueInput(id: int.tryParse(id)),
    );
    return Response.json(body: dbModel);
  } catch (e) {
    return Response(statusCode: HttpStatus.badRequest, body: e.toString());
  }
}

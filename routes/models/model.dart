import 'dart:io';

import 'package:dart_frog/dart_frog.dart';
import 'package:orm/orm.dart';

import '../../prisma/generated_dart_client/client.dart';
import '../../prisma/generated_dart_client/model.dart';
import '../../prisma/generated_dart_client/prisma.dart';

Future<Response> onRequest(RequestContext context) {
  return switch (context.request.method) {
    HttpMethod.get => _getModels(context),
    HttpMethod.post => _createModels(context),
    HttpMethod.delete => _deleteModel(context),
    HttpMethod.put => _updateModels(context),
    _ => Future.value(Response(statusCode: HttpStatus.badRequest))
  };
}

Future<Response> _getModels(RequestContext context) async {
  final prisma = context.read<PrismaClient>();
  try {
    final expensives = await prisma.model.findMany();
    final marks = await prisma.mark.findMany();
    final countries = await prisma.country.findMany();

    final todos = expensives.map(
      (e) {
        Mark mark = marks.firstWhere((element) => element.id == e.markId);
        Country country =
            countries.firstWhere((element) => element.id == e.countryId);

        return Model(
          id: e.id,
          name: e.name,
          mark: mark,
          markId: e.markId,
          countryId: e.countryId,
          country: country,
        ).toJson();
      },
    ).toList();
    return Response.json(body: todos);
  } catch (e) {
    return Response(statusCode: HttpStatus.badRequest, body: e.toString());
  }
}

//
Future<Response> _createModels(RequestContext context) async {
  final prisma = context.read<PrismaClient>();
  final requestBody =
      await (await context.request.json()) as Map<String, dynamic>;

  try {
    final newModel = ModelCreateInput(
      name: requestBody['name'] as String,
      mark: MarkCreateNestedOneWithoutModelInput(
        connect: MarkWhereUniqueInput(id: requestBody['markId'] as int),
      ),
      country: CountryCreateNestedOneWithoutModelInput(
        connect: CountryWhereUniqueInput(id: requestBody['countryId'] as int),
      ),
    );
    final dbModel = await prisma.model.create(data: PrismaUnion.$1(newModel));
    return Response.json(body: dbModel);
  } catch (e) {
    return Response(statusCode: HttpStatus.badRequest, body: e.toString());
  }
}

//
Future<Response> _updateModels(RequestContext context) async {
  final prisma = context.read<PrismaClient>();
  final requestBody =
      await (await context.request.json()) as Map<String, dynamic>;

  try {
    final id = context.request.uri.queryParameters['id']!;
    final model = ModelUpdateInput(
      name: requestBody['name'] != null
          ? PrismaUnion.$2(
              StringFieldUpdateOperationsInput(
                  set: requestBody['name'] as String),
            )
          : null,
      mark: requestBody['markId'] != null
          ? MarkUpdateOneRequiredWithoutModelNestedInput(
              connect: MarkWhereUniqueInput(id: requestBody['markId'] as int),
            )
          : null,
      country: requestBody['countryId'] != null
          ? CountryUpdateOneRequiredWithoutModelNestedInput(
              connect: CountryWhereUniqueInput(
                id: requestBody['countryId'] as int,
              ),
            )
          : null,
    );

    final dbModel = await prisma.model.update(
      data: PrismaUnion.$1(model),
      where: ModelWhereUniqueInput(id: int.tryParse(id)),
    );
    return Response.json(body: dbModel);
  } catch (e) {
    print(e.toString());
    return Response(statusCode: HttpStatus.badRequest, body: e.toString());
  }
}

//
Future<Response> _deleteModel(RequestContext context) async {
  final prisma = context.read<PrismaClient>();

  try {
    final id = context.request.uri.queryParameters["id"]!;
    final dbModel = await prisma.model.delete(
      where: ModelWhereUniqueInput(id: int.tryParse(id)),
    );
    return Response.json(body: dbModel);
  } catch (e) {
    return Response(statusCode: HttpStatus.badRequest, body: e.toString());
  }
}

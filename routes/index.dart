import 'package:dart_frog/dart_frog.dart';
import 'package:dart_jsonwebtoken/dart_jsonwebtoken.dart';

Response onRequest(RequestContext context) {
  return Response(body: 'Welcome to Dart Frog!');
}

class EnvironmentValue {
  EnvironmentValue._singleton();

  factory EnvironmentValue.instance() => _instance;
  static final EnvironmentValue _instance = EnvironmentValue._singleton();

  int? userId;
}

int getUserIdFromToken(RequestContext context) {
  final authToken = context.request.headers['Authorization'];
  return JWT
      .verify(authToken?.split(' ')[1] ?? '', SecretKey('Qwer1234'))
      .payload as int;
}

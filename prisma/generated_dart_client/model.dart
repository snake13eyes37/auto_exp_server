// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'model.dart' as _i1;
import 'prisma.dart' as _i2;

class Country {
  const Country({
    this.id,
    this.name,
    this.code,
    this.model,
    this.$count,
  });

  factory Country.fromJson(Map json) => Country(
        id: json['id'],
        name: json['name'],
        code: json['code'],
        model: (json['Model'] as Iterable?)
            ?.map((json) => _i1.Model.fromJson(json)),
        $count: json['_count'] is Map
            ? _i2.CountryCountOutputType.fromJson(json['_count'])
            : null,
      );

  final int? id;

  final String? name;

  final String? code;

  final Iterable<_i1.Model>? model;

  final _i2.CountryCountOutputType? $count;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'code': code,
        'Model': model?.map((e) => e.toJson()),
        '_count': $count?.toJson(),
      };
}

class Engine {
  const Engine({
    this.id,
    this.name,
    this.power,
    this.car,
    this.$count,
  });

  factory Engine.fromJson(Map json) => Engine(
        id: json['id'],
        name: json['name'],
        power: json['power'],
        car: (json['Car'] as Iterable?)?.map((json) => _i1.Car.fromJson(json)),
        $count: json['_count'] is Map
            ? _i2.EngineCountOutputType.fromJson(json['_count'])
            : null,
      );

  final int? id;

  final String? name;

  final int? power;

  final Iterable<_i1.Car>? car;

  final _i2.EngineCountOutputType? $count;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'power': power,
        'Car': car?.map((e) => e.toJson()),
        '_count': $count?.toJson(),
      };
}

class Category {
  const Category({
    this.id,
    this.name,
    this.expensive,
    this.$count,
  });

  factory Category.fromJson(Map json) => Category(
        id: json['id'],
        name: json['name'],
        expensive: (json['Expensive'] as Iterable?)
            ?.map((json) => _i1.Expensive.fromJson(json)),
        $count: json['_count'] is Map
            ? _i2.CategoryCountOutputType.fromJson(json['_count'])
            : null,
      );

  final int? id;

  final String? name;

  final Iterable<_i1.Expensive>? expensive;

  final _i2.CategoryCountOutputType? $count;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'Expensive': expensive?.map((e) => e.toJson()),
        '_count': $count?.toJson(),
      };
}

class Fuel {
  const Fuel({
    this.id,
    this.name,
    this.expensive,
    this.$count,
  });

  factory Fuel.fromJson(Map json) => Fuel(
        id: json['id'],
        name: json['name'],
        expensive: (json['Expensive'] as Iterable?)
            ?.map((json) => _i1.Expensive.fromJson(json)),
        $count: json['_count'] is Map
            ? _i2.FuelCountOutputType.fromJson(json['_count'])
            : null,
      );

  final int? id;

  final String? name;

  final Iterable<_i1.Expensive>? expensive;

  final _i2.FuelCountOutputType? $count;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'Expensive': expensive?.map((e) => e.toJson()),
        '_count': $count?.toJson(),
      };
}

class Expensive {
  const Expensive({
    this.id,
    this.name,
    this.categoryId,
    this.description,
    this.createdDate,
    this.range,
    this.price,
    this.userId,
    this.fuelTypeId,
    this.liters,
    this.category,
    this.user,
    this.fuel,
  });

  factory Expensive.fromJson(Map json) => Expensive(
        id: json['id'],
        name: json['name'],
        categoryId: json['category_id'],
        description: json['description'],
        createdDate: json['created_date'],
        range: json['range'],
        price: json['price'],
        userId: json['user_id'],
        fuelTypeId: json['fuel_type_id'],
        liters: json['liters'],
        category: json['category'] is Map
            ? _i1.Category.fromJson(json['category'])
            : null,
        user: json['User'] is Map ? _i1.User.fromJson(json['User']) : null,
        fuel: json['Fuel'] is Map ? _i1.Fuel.fromJson(json['Fuel']) : null,
      );

  final int? id;

  final String? name;

  final int? categoryId;

  final String? description;

  final DateTime? createdDate;

  final double? range;

  final double? price;

  final int? userId;

  final int? fuelTypeId;

  final double? liters;

  final _i1.Category? category;

  final _i1.User? user;

  final _i1.Fuel? fuel;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'category_id': categoryId,
        'description': description,
        'created_date': createdDate?.toIso8601String(),
        'range': range,
        'price': price,
        'user_id': userId,
        'fuel_type_id': fuelTypeId,
        'liters': liters,
        'category': category?.toJson(),
        'User': user?.toJson(),
        'Fuel': fuel?.toJson(),
      };
}

class User {
  const User({
    this.id,
    this.name,
    this.phone,
    this.birthday,
    this.carId,
    this.car,
    this.expensive,
    this.$count,
  });

  factory User.fromJson(Map json) => User(
        id: json['id'],
        name: json['name'],
        phone: json['phone'],
        birthday: json['birthday'] ,
        carId: json['car_id'],
        car: json['car'] is Map ? _i1.Car.fromJson(json['car']) : null,
        expensive: (json['Expensive'] as Iterable?)
            ?.map((json) => _i1.Expensive.fromJson(json)),
        $count: json['_count'] is Map
            ? _i2.UserCountOutputType.fromJson(json['_count'])
            : null,
      );

  final int? id;

  final String? name;

  final String? phone;

  final DateTime? birthday;

  final int? carId;

  final _i1.Car? car;

  final Iterable<_i1.Expensive>? expensive;

  final _i2.UserCountOutputType? $count;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'phone': phone,
        'birthday': birthday?.toIso8601String(),
        'car_id': carId,
        'car': car?.toJson(),
        'Expensive': expensive?.map((e) => e.toJson()),
        '_count': $count?.toJson(),
      };
}

class Car {
  const Car({
    this.id,
    this.vin,
    this.modelId,
    this.range,
    this.createdDate,
    this.voluem,
    this.engineId,
    this.model,
    this.engine,
    this.user,
    this.$count,
  });

  factory Car.fromJson(Map json) => Car(
        id: json['id'],
        vin: json['vin'],
        modelId: json['model_id'],
        range: json['range'],
        createdDate: json['created_date'],
        voluem: json['voluem'],
        engineId: json['engine_id'],
        model: json['model'] is Map ? _i1.Model.fromJson(json['model']) : null,
        engine:
            json['engine'] is Map ? _i1.Engine.fromJson(json['engine']) : null,
        user:
            (json['User'] as Iterable?)?.map((json) => _i1.User.fromJson(json)),
        $count: json['_count'] is Map
            ? _i2.CarCountOutputType.fromJson(json['_count'])
            : null,
      );

  final int? id;

  final String? vin;

  final int? modelId;

  final double? range;

  final DateTime? createdDate;

  final int? voluem;

  final int? engineId;

  final _i1.Model? model;

  final _i1.Engine? engine;

  final Iterable<_i1.User>? user;

  final _i2.CarCountOutputType? $count;

  Map<String, dynamic> toJson() => {
        'id': id,
        'vin': vin,
        'model_id': modelId,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'engine_id': engineId,
        'model': model?.toJson(),
        'engine': engine?.toJson(),
        'User': user?.map((e) => e.toJson()),
        '_count': $count?.toJson(),
      };
}

class Model {
  const Model({
    this.id,
    this.name,
    this.markId,
    this.countryId,
    this.mark,
    this.country,
    this.car,
    this.$count,
  });

  factory Model.fromJson(Map json) => Model(
        id: json['id'],
        name: json['name'],
        markId: json['mark_id'],
        countryId: json['country_id'],
        mark: json['mark'] is Map ? _i1.Mark.fromJson(json['mark']) : null,
        country: json['country'] is Map
            ? _i1.Country.fromJson(json['country'])
            : null,
        car: (json['Car'] as Iterable?)?.map((json) => _i1.Car.fromJson(json)),
        $count: json['_count'] is Map
            ? _i2.ModelCountOutputType.fromJson(json['_count'])
            : null,
      );

  final int? id;

  final String? name;

  final int? markId;

  final int? countryId;

  final _i1.Mark? mark;

  final _i1.Country? country;

  final Iterable<_i1.Car>? car;

  final _i2.ModelCountOutputType? $count;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'mark_id': markId,
        'country_id': countryId,
        'mark': mark?.toJson(),
        'country': country?.toJson(),
        'Car': car?.map((e) => e.toJson()),
        '_count': $count?.toJson(),
      };
}

class Mark {
  const Mark({
    this.id,
    this.name,
    this.model,
    this.$count,
  });

  factory Mark.fromJson(Map json) => Mark(
        id: json['id'],
        name: json['name'],
        model: (json['Model'] as Iterable?)
            ?.map((json) => _i1.Model.fromJson(json)),
        $count: json['_count'] is Map
            ? _i2.MarkCountOutputType.fromJson(json['_count'])
            : null,
      );

  final int? id;

  final String? name;

  final Iterable<_i1.Model>? model;

  final _i2.MarkCountOutputType? $count;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'Model': model?.map((e) => e.toJson()),
        '_count': $count?.toJson(),
      };
}

// ignore_for_file: non_constant_identifier_names

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:orm/orm.dart' as _i1;

import 'prisma.dart' as _i2;

class CountryCountOutputType {
  const CountryCountOutputType({this.model});

  factory CountryCountOutputType.fromJson(Map json) =>
      CountryCountOutputType(model: json['Model']);

  final int? model;

  Map<String, dynamic> toJson() => {'Model': model};
}

class EngineCountOutputType {
  const EngineCountOutputType({this.car});

  factory EngineCountOutputType.fromJson(Map json) =>
      EngineCountOutputType(car: json['Car']);

  final int? car;

  Map<String, dynamic> toJson() => {'Car': car};
}

class CategoryCountOutputType {
  const CategoryCountOutputType({this.expensive});

  factory CategoryCountOutputType.fromJson(Map json) =>
      CategoryCountOutputType(expensive: json['Expensive']);

  final int? expensive;

  Map<String, dynamic> toJson() => {'Expensive': expensive};
}

class FuelCountOutputType {
  const FuelCountOutputType({this.expensive});

  factory FuelCountOutputType.fromJson(Map json) =>
      FuelCountOutputType(expensive: json['Expensive']);

  final int? expensive;

  Map<String, dynamic> toJson() => {'Expensive': expensive};
}

class UserCountOutputType {
  const UserCountOutputType({this.expensive});

  factory UserCountOutputType.fromJson(Map json) =>
      UserCountOutputType(expensive: json['Expensive']);

  final int? expensive;

  Map<String, dynamic> toJson() => {'Expensive': expensive};
}

class CarCountOutputType {
  const CarCountOutputType({this.user});

  factory CarCountOutputType.fromJson(Map json) =>
      CarCountOutputType(user: json['User']);

  final int? user;

  Map<String, dynamic> toJson() => {'User': user};
}

class ModelCountOutputType {
  const ModelCountOutputType({this.car});

  factory ModelCountOutputType.fromJson(Map json) =>
      ModelCountOutputType(car: json['Car']);

  final int? car;

  Map<String, dynamic> toJson() => {'Car': car};
}

class MarkCountOutputType {
  const MarkCountOutputType({this.model});

  factory MarkCountOutputType.fromJson(Map json) =>
      MarkCountOutputType(model: json['Model']);

  final int? model;

  Map<String, dynamic> toJson() => {'Model': model};
}

class MarkWhereUniqueInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkWhereUniqueInput({this.id});

  final int? id;

  @override
  Map<String, dynamic> toJson() => {'id': id};
}

class ModelMarkArgs implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelMarkArgs({
    this.select,
    this.include,
  });

  final _i2.MarkSelect? select;

  final _i2.MarkInclude? include;

  @override
  Map<String, dynamic> toJson() => {
        'select': select,
        'include': include,
      };
}

class NestedIntFilter implements _i1.JsonConvertible<Map<String, dynamic>> {
  const NestedIntFilter({
    this.equals,
    this.$in,
    this.notIn,
    this.lt,
    this.lte,
    this.gt,
    this.gte,
    this.not,
  });

  final int? equals;

  final _i1.PrismaUnion<Iterable<int>, int>? $in;

  final _i1.PrismaUnion<Iterable<int>, int>? notIn;

  final int? lt;

  final int? lte;

  final int? gt;

  final int? gte;

  final _i1.PrismaUnion<int, _i2.NestedIntFilter>? not;

  @override
  Map<String, dynamic> toJson() => {
        'equals': equals,
        'in': $in,
        'notIn': notIn,
        'lt': lt,
        'lte': lte,
        'gt': gt,
        'gte': gte,
        'not': not,
      };
}

class IntFilter implements _i1.JsonConvertible<Map<String, dynamic>> {
  const IntFilter({
    this.equals,
    this.$in,
    this.notIn,
    this.lt,
    this.lte,
    this.gt,
    this.gte,
    this.not,
  });

  final int? equals;

  final _i1.PrismaUnion<Iterable<int>, int>? $in;

  final _i1.PrismaUnion<Iterable<int>, int>? notIn;

  final int? lt;

  final int? lte;

  final int? gt;

  final int? gte;

  final _i1.PrismaUnion<int, _i2.NestedIntFilter>? not;

  @override
  Map<String, dynamic> toJson() => {
        'equals': equals,
        'in': $in,
        'notIn': notIn,
        'lt': lt,
        'lte': lte,
        'gt': gt,
        'gte': gte,
        'not': not,
      };
}

enum QueryMode implements _i1.PrismaEnum {
  $default._('default'),
  insensitive._('insensitive');

  const QueryMode._(this.name);

  @override
  final String name;
}

class NestedStringFilter implements _i1.JsonConvertible<Map<String, dynamic>> {
  const NestedStringFilter({
    this.equals,
    this.$in,
    this.notIn,
    this.lt,
    this.lte,
    this.gt,
    this.gte,
    this.contains,
    this.startsWith,
    this.endsWith,
    this.not,
  });

  final String? equals;

  final _i1.PrismaUnion<Iterable<String>, String>? $in;

  final _i1.PrismaUnion<Iterable<String>, String>? notIn;

  final String? lt;

  final String? lte;

  final String? gt;

  final String? gte;

  final String? contains;

  final String? startsWith;

  final String? endsWith;

  final _i1.PrismaUnion<String, _i2.NestedStringFilter>? not;

  @override
  Map<String, dynamic> toJson() => {
        'equals': equals,
        'in': $in,
        'notIn': notIn,
        'lt': lt,
        'lte': lte,
        'gt': gt,
        'gte': gte,
        'contains': contains,
        'startsWith': startsWith,
        'endsWith': endsWith,
        'not': not,
      };
}

class StringFilter implements _i1.JsonConvertible<Map<String, dynamic>> {
  const StringFilter({
    this.equals,
    this.$in,
    this.notIn,
    this.lt,
    this.lte,
    this.gt,
    this.gte,
    this.contains,
    this.startsWith,
    this.endsWith,
    this.mode,
    this.not,
  });

  final String? equals;

  final _i1.PrismaUnion<Iterable<String>, String>? $in;

  final _i1.PrismaUnion<Iterable<String>, String>? notIn;

  final String? lt;

  final String? lte;

  final String? gt;

  final String? gte;

  final String? contains;

  final String? startsWith;

  final String? endsWith;

  final _i2.QueryMode? mode;

  final _i1.PrismaUnion<String, _i2.NestedStringFilter>? not;

  @override
  Map<String, dynamic> toJson() => {
        'equals': equals,
        'in': $in,
        'notIn': notIn,
        'lt': lt,
        'lte': lte,
        'gt': gt,
        'gte': gte,
        'contains': contains,
        'startsWith': startsWith,
        'endsWith': endsWith,
        'mode': mode,
        'not': not,
      };
}

class ModelListRelationFilter
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelListRelationFilter({
    this.every,
    this.some,
    this.none,
  });

  final _i2.ModelWhereInput? every;

  final _i2.ModelWhereInput? some;

  final _i2.ModelWhereInput? none;

  @override
  Map<String, dynamic> toJson() => {
        'every': every,
        'some': some,
        'none': none,
      };
}

class MarkWhereInput implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkWhereInput({
    this.AND,
    this.OR,
    this.NOT,
    this.id,
    this.name,
    this.model,
  });

  final _i1.PrismaUnion<_i2.MarkWhereInput, Iterable<_i2.MarkWhereInput>>? AND;

  final Iterable<_i2.MarkWhereInput>? OR;

  final _i1.PrismaUnion<_i2.MarkWhereInput, Iterable<_i2.MarkWhereInput>>? NOT;

  final _i1.PrismaUnion<_i2.IntFilter, int>? id;

  final _i1.PrismaUnion<_i2.StringFilter, String>? name;

  final _i2.ModelListRelationFilter? model;

  @override
  Map<String, dynamic> toJson() => {
        'AND': AND,
        'OR': OR,
        'NOT': NOT,
        'id': id,
        'name': name,
        'Model': model,
      };
}

class MarkRelationFilter implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkRelationFilter({
    this.$is,
    this.isNot,
  });

  final _i1.PrismaUnion<_i2.MarkWhereInput, _i1.PrismaNull>? $is;

  final _i1.PrismaUnion<_i2.MarkWhereInput, _i1.PrismaNull>? isNot;

  @override
  Map<String, dynamic> toJson() => {
        'is': $is,
        'isNot': isNot,
      };
}

class CountryWhereInput implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryWhereInput({
    this.AND,
    this.OR,
    this.NOT,
    this.id,
    this.name,
    this.code,
    this.model,
  });

  final _i1.PrismaUnion<_i2.CountryWhereInput, Iterable<_i2.CountryWhereInput>>?
      AND;

  final Iterable<_i2.CountryWhereInput>? OR;

  final _i1.PrismaUnion<_i2.CountryWhereInput, Iterable<_i2.CountryWhereInput>>?
      NOT;

  final _i1.PrismaUnion<_i2.IntFilter, int>? id;

  final _i1.PrismaUnion<_i2.StringFilter, String>? name;

  final _i1.PrismaUnion<_i2.StringFilter, String>? code;

  final _i2.ModelListRelationFilter? model;

  @override
  Map<String, dynamic> toJson() => {
        'AND': AND,
        'OR': OR,
        'NOT': NOT,
        'id': id,
        'name': name,
        'code': code,
        'Model': model,
      };
}

class CountryRelationFilter
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryRelationFilter({
    this.$is,
    this.isNot,
  });

  final _i1.PrismaUnion<_i2.CountryWhereInput, _i1.PrismaNull>? $is;

  final _i1.PrismaUnion<_i2.CountryWhereInput, _i1.PrismaNull>? isNot;

  @override
  Map<String, dynamic> toJson() => {
        'is': $is,
        'isNot': isNot,
      };
}

class NestedFloatFilter implements _i1.JsonConvertible<Map<String, dynamic>> {
  const NestedFloatFilter({
    this.equals,
    this.$in,
    this.notIn,
    this.lt,
    this.lte,
    this.gt,
    this.gte,
    this.not,
  });

  final double? equals;

  final _i1.PrismaUnion<Iterable<double>, double>? $in;

  final _i1.PrismaUnion<Iterable<double>, double>? notIn;

  final double? lt;

  final double? lte;

  final double? gt;

  final double? gte;

  final _i1.PrismaUnion<double, _i2.NestedFloatFilter>? not;

  @override
  Map<String, dynamic> toJson() => {
        'equals': equals,
        'in': $in,
        'notIn': notIn,
        'lt': lt,
        'lte': lte,
        'gt': gt,
        'gte': gte,
        'not': not,
      };
}

class FloatFilter implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FloatFilter({
    this.equals,
    this.$in,
    this.notIn,
    this.lt,
    this.lte,
    this.gt,
    this.gte,
    this.not,
  });

  final double? equals;

  final _i1.PrismaUnion<Iterable<double>, double>? $in;

  final _i1.PrismaUnion<Iterable<double>, double>? notIn;

  final double? lt;

  final double? lte;

  final double? gt;

  final double? gte;

  final _i1.PrismaUnion<double, _i2.NestedFloatFilter>? not;

  @override
  Map<String, dynamic> toJson() => {
        'equals': equals,
        'in': $in,
        'notIn': notIn,
        'lt': lt,
        'lte': lte,
        'gt': gt,
        'gte': gte,
        'not': not,
      };
}

class NestedDateTimeFilter
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const NestedDateTimeFilter({
    this.equals,
    this.$in,
    this.notIn,
    this.lt,
    this.lte,
    this.gt,
    this.gte,
    this.not,
  });

  final DateTime? equals;

  final _i1.PrismaUnion<Iterable<DateTime>, DateTime>? $in;

  final _i1.PrismaUnion<Iterable<DateTime>, DateTime>? notIn;

  final DateTime? lt;

  final DateTime? lte;

  final DateTime? gt;

  final DateTime? gte;

  final _i1.PrismaUnion<DateTime, _i2.NestedDateTimeFilter>? not;

  @override
  Map<String, dynamic> toJson() => {
        'equals': equals,
        'in': $in,
        'notIn': notIn,
        'lt': lt,
        'lte': lte,
        'gt': gt,
        'gte': gte,
        'not': not,
      };
}

class DateTimeFilter implements _i1.JsonConvertible<Map<String, dynamic>> {
  const DateTimeFilter({
    this.equals,
    this.$in,
    this.notIn,
    this.lt,
    this.lte,
    this.gt,
    this.gte,
    this.not,
  });

  final DateTime? equals;

  final _i1.PrismaUnion<Iterable<DateTime>, DateTime>? $in;

  final _i1.PrismaUnion<Iterable<DateTime>, DateTime>? notIn;

  final DateTime? lt;

  final DateTime? lte;

  final DateTime? gt;

  final DateTime? gte;

  final _i1.PrismaUnion<DateTime, _i2.NestedDateTimeFilter>? not;

  @override
  Map<String, dynamic> toJson() => {
        'equals': equals,
        'in': $in,
        'notIn': notIn,
        'lt': lt,
        'lte': lte,
        'gt': gt,
        'gte': gte,
        'not': not,
      };
}

class ModelRelationFilter implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelRelationFilter({
    this.$is,
    this.isNot,
  });

  final _i1.PrismaUnion<_i2.ModelWhereInput, _i1.PrismaNull>? $is;

  final _i1.PrismaUnion<_i2.ModelWhereInput, _i1.PrismaNull>? isNot;

  @override
  Map<String, dynamic> toJson() => {
        'is': $is,
        'isNot': isNot,
      };
}

class EngineWhereInput implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineWhereInput({
    this.AND,
    this.OR,
    this.NOT,
    this.id,
    this.name,
    this.power,
    this.car,
  });

  final _i1.PrismaUnion<_i2.EngineWhereInput, Iterable<_i2.EngineWhereInput>>?
      AND;

  final Iterable<_i2.EngineWhereInput>? OR;

  final _i1.PrismaUnion<_i2.EngineWhereInput, Iterable<_i2.EngineWhereInput>>?
      NOT;

  final _i1.PrismaUnion<_i2.IntFilter, int>? id;

  final _i1.PrismaUnion<_i2.StringFilter, String>? name;

  final _i1.PrismaUnion<_i2.IntFilter, int>? power;

  final _i2.CarListRelationFilter? car;

  @override
  Map<String, dynamic> toJson() => {
        'AND': AND,
        'OR': OR,
        'NOT': NOT,
        'id': id,
        'name': name,
        'power': power,
        'Car': car,
      };
}

class EngineRelationFilter
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineRelationFilter({
    this.$is,
    this.isNot,
  });

  final _i1.PrismaUnion<_i2.EngineWhereInput, _i1.PrismaNull>? $is;

  final _i1.PrismaUnion<_i2.EngineWhereInput, _i1.PrismaNull>? isNot;

  @override
  Map<String, dynamic> toJson() => {
        'is': $is,
        'isNot': isNot,
      };
}

class CarRelationFilter implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarRelationFilter({
    this.$is,
    this.isNot,
  });

  final _i1.PrismaUnion<_i2.CarWhereInput, _i1.PrismaNull>? $is;

  final _i1.PrismaUnion<_i2.CarWhereInput, _i1.PrismaNull>? isNot;

  @override
  Map<String, dynamic> toJson() => {
        'is': $is,
        'isNot': isNot,
      };
}

class NestedIntNullableFilter
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const NestedIntNullableFilter({
    this.equals,
    this.$in,
    this.notIn,
    this.lt,
    this.lte,
    this.gt,
    this.gte,
    this.not,
  });

  final _i1.PrismaUnion<int, _i1.PrismaNull>? equals;

  final _i1.PrismaUnion<Iterable<int>, _i1.PrismaUnion<int, _i1.PrismaNull>>?
      $in;

  final _i1.PrismaUnion<Iterable<int>, _i1.PrismaUnion<int, _i1.PrismaNull>>?
      notIn;

  final int? lt;

  final int? lte;

  final int? gt;

  final int? gte;

  final _i1.PrismaUnion<int,
      _i1.PrismaUnion<_i2.NestedIntNullableFilter, _i1.PrismaNull>>? not;

  @override
  Map<String, dynamic> toJson() => {
        'equals': equals,
        'in': $in,
        'notIn': notIn,
        'lt': lt,
        'lte': lte,
        'gt': gt,
        'gte': gte,
        'not': not,
      };
}

class IntNullableFilter implements _i1.JsonConvertible<Map<String, dynamic>> {
  const IntNullableFilter({
    this.equals,
    this.$in,
    this.notIn,
    this.lt,
    this.lte,
    this.gt,
    this.gte,
    this.not,
  });

  final _i1.PrismaUnion<int, _i1.PrismaNull>? equals;

  final _i1.PrismaUnion<Iterable<int>, _i1.PrismaUnion<int, _i1.PrismaNull>>?
      $in;

  final _i1.PrismaUnion<Iterable<int>, _i1.PrismaUnion<int, _i1.PrismaNull>>?
      notIn;

  final int? lt;

  final int? lte;

  final int? gt;

  final int? gte;

  final _i1.PrismaUnion<int,
      _i1.PrismaUnion<_i2.NestedIntNullableFilter, _i1.PrismaNull>>? not;

  @override
  Map<String, dynamic> toJson() => {
        'equals': equals,
        'in': $in,
        'notIn': notIn,
        'lt': lt,
        'lte': lte,
        'gt': gt,
        'gte': gte,
        'not': not,
      };
}

class NestedFloatNullableFilter
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const NestedFloatNullableFilter({
    this.equals,
    this.$in,
    this.notIn,
    this.lt,
    this.lte,
    this.gt,
    this.gte,
    this.not,
  });

  final _i1.PrismaUnion<double, _i1.PrismaNull>? equals;

  final _i1
      .PrismaUnion<Iterable<double>, _i1.PrismaUnion<double, _i1.PrismaNull>>?
      $in;

  final _i1
      .PrismaUnion<Iterable<double>, _i1.PrismaUnion<double, _i1.PrismaNull>>?
      notIn;

  final double? lt;

  final double? lte;

  final double? gt;

  final double? gte;

  final _i1.PrismaUnion<double,
      _i1.PrismaUnion<_i2.NestedFloatNullableFilter, _i1.PrismaNull>>? not;

  @override
  Map<String, dynamic> toJson() => {
        'equals': equals,
        'in': $in,
        'notIn': notIn,
        'lt': lt,
        'lte': lte,
        'gt': gt,
        'gte': gte,
        'not': not,
      };
}

class FloatNullableFilter implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FloatNullableFilter({
    this.equals,
    this.$in,
    this.notIn,
    this.lt,
    this.lte,
    this.gt,
    this.gte,
    this.not,
  });

  final _i1.PrismaUnion<double, _i1.PrismaNull>? equals;

  final _i1
      .PrismaUnion<Iterable<double>, _i1.PrismaUnion<double, _i1.PrismaNull>>?
      $in;

  final _i1
      .PrismaUnion<Iterable<double>, _i1.PrismaUnion<double, _i1.PrismaNull>>?
      notIn;

  final double? lt;

  final double? lte;

  final double? gt;

  final double? gte;

  final _i1.PrismaUnion<double,
      _i1.PrismaUnion<_i2.NestedFloatNullableFilter, _i1.PrismaNull>>? not;

  @override
  Map<String, dynamic> toJson() => {
        'equals': equals,
        'in': $in,
        'notIn': notIn,
        'lt': lt,
        'lte': lte,
        'gt': gt,
        'gte': gte,
        'not': not,
      };
}

class CategoryWhereInput implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryWhereInput({
    this.AND,
    this.OR,
    this.NOT,
    this.id,
    this.name,
    this.expensive,
  });

  final _i1
      .PrismaUnion<_i2.CategoryWhereInput, Iterable<_i2.CategoryWhereInput>>?
      AND;

  final Iterable<_i2.CategoryWhereInput>? OR;

  final _i1
      .PrismaUnion<_i2.CategoryWhereInput, Iterable<_i2.CategoryWhereInput>>?
      NOT;

  final _i1.PrismaUnion<_i2.IntFilter, int>? id;

  final _i1.PrismaUnion<_i2.StringFilter, String>? name;

  final _i2.ExpensiveListRelationFilter? expensive;

  @override
  Map<String, dynamic> toJson() => {
        'AND': AND,
        'OR': OR,
        'NOT': NOT,
        'id': id,
        'name': name,
        'Expensive': expensive,
      };
}

class CategoryRelationFilter
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryRelationFilter({
    this.$is,
    this.isNot,
  });

  final _i1.PrismaUnion<_i2.CategoryWhereInput, _i1.PrismaNull>? $is;

  final _i1.PrismaUnion<_i2.CategoryWhereInput, _i1.PrismaNull>? isNot;

  @override
  Map<String, dynamic> toJson() => {
        'is': $is,
        'isNot': isNot,
      };
}

class UserRelationFilter implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserRelationFilter({
    this.$is,
    this.isNot,
  });

  final _i1.PrismaUnion<_i2.UserWhereInput, _i1.PrismaNull>? $is;

  final _i1.PrismaUnion<_i2.UserWhereInput, _i1.PrismaNull>? isNot;

  @override
  Map<String, dynamic> toJson() => {
        'is': $is,
        'isNot': isNot,
      };
}

class FuelWhereInput implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelWhereInput({
    this.AND,
    this.OR,
    this.NOT,
    this.id,
    this.name,
    this.expensive,
  });

  final _i1.PrismaUnion<_i2.FuelWhereInput, Iterable<_i2.FuelWhereInput>>? AND;

  final Iterable<_i2.FuelWhereInput>? OR;

  final _i1.PrismaUnion<_i2.FuelWhereInput, Iterable<_i2.FuelWhereInput>>? NOT;

  final _i1.PrismaUnion<_i2.IntFilter, int>? id;

  final _i1.PrismaUnion<_i2.StringFilter, String>? name;

  final _i2.ExpensiveListRelationFilter? expensive;

  @override
  Map<String, dynamic> toJson() => {
        'AND': AND,
        'OR': OR,
        'NOT': NOT,
        'id': id,
        'name': name,
        'Expensive': expensive,
      };
}

class FuelRelationFilter implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelRelationFilter({
    this.$is,
    this.isNot,
  });

  final _i1.PrismaUnion<_i2.FuelWhereInput, _i1.PrismaNull>? $is;

  final _i1.PrismaUnion<_i2.FuelWhereInput, _i1.PrismaNull>? isNot;

  @override
  Map<String, dynamic> toJson() => {
        'is': $is,
        'isNot': isNot,
      };
}

class ExpensiveWhereInput implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveWhereInput({
    this.AND,
    this.OR,
    this.NOT,
    this.id,
    this.name,
    this.categoryId,
    this.description,
    this.createdDate,
    this.range,
    this.price,
    this.userId,
    this.fuelTypeId,
    this.liters,
    this.category,
    this.user,
    this.fuel,
  });

  final _i1
      .PrismaUnion<_i2.ExpensiveWhereInput, Iterable<_i2.ExpensiveWhereInput>>?
      AND;

  final Iterable<_i2.ExpensiveWhereInput>? OR;

  final _i1
      .PrismaUnion<_i2.ExpensiveWhereInput, Iterable<_i2.ExpensiveWhereInput>>?
      NOT;

  final _i1.PrismaUnion<_i2.IntFilter, int>? id;

  final _i1.PrismaUnion<_i2.StringFilter, String>? name;

  final _i1.PrismaUnion<_i2.IntFilter, int>? categoryId;

  final _i1.PrismaUnion<_i2.StringFilter, String>? description;

  final _i1.PrismaUnion<_i2.DateTimeFilter, DateTime>? createdDate;

  final _i1.PrismaUnion<_i2.FloatFilter, double>? range;

  final _i1.PrismaUnion<_i2.FloatFilter, double>? price;

  final _i1.PrismaUnion<_i2.IntFilter, int>? userId;

  final _i1
      .PrismaUnion<_i2.IntNullableFilter, _i1.PrismaUnion<int, _i1.PrismaNull>>?
      fuelTypeId;

  final _i1.PrismaUnion<_i2.FloatNullableFilter,
      _i1.PrismaUnion<double, _i1.PrismaNull>>? liters;

  final _i1.PrismaUnion<_i2.CategoryRelationFilter, _i2.CategoryWhereInput>?
      category;

  final _i1.PrismaUnion<_i2.UserRelationFilter, _i2.UserWhereInput>? user;

  final _i1.PrismaUnion<_i2.FuelRelationFilter,
      _i1.PrismaUnion<_i2.FuelWhereInput, _i1.PrismaNull>>? fuel;

  @override
  Map<String, dynamic> toJson() => {
        'AND': AND,
        'OR': OR,
        'NOT': NOT,
        'id': id,
        'name': name,
        'category_id': categoryId,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'user_id': userId,
        'fuel_type_id': fuelTypeId,
        'liters': liters,
        'category': category,
        'User': user,
        'Fuel': fuel,
      };
}

class ExpensiveListRelationFilter
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveListRelationFilter({
    this.every,
    this.some,
    this.none,
  });

  final _i2.ExpensiveWhereInput? every;

  final _i2.ExpensiveWhereInput? some;

  final _i2.ExpensiveWhereInput? none;

  @override
  Map<String, dynamic> toJson() => {
        'every': every,
        'some': some,
        'none': none,
      };
}

class UserWhereInput implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserWhereInput({
    this.AND,
    this.OR,
    this.NOT,
    this.id,
    this.name,
    this.phone,
    this.birthday,
    this.carId,
    this.car,
    this.expensive,
  });

  final _i1.PrismaUnion<_i2.UserWhereInput, Iterable<_i2.UserWhereInput>>? AND;

  final Iterable<_i2.UserWhereInput>? OR;

  final _i1.PrismaUnion<_i2.UserWhereInput, Iterable<_i2.UserWhereInput>>? NOT;

  final _i1.PrismaUnion<_i2.IntFilter, int>? id;

  final _i1.PrismaUnion<_i2.StringFilter, String>? name;

  final _i1.PrismaUnion<_i2.StringFilter, String>? phone;

  final _i1.PrismaUnion<_i2.DateTimeFilter, DateTime>? birthday;

  final _i1.PrismaUnion<_i2.IntFilter, int>? carId;

  final _i1.PrismaUnion<_i2.CarRelationFilter, _i2.CarWhereInput>? car;

  final _i2.ExpensiveListRelationFilter? expensive;

  @override
  Map<String, dynamic> toJson() => {
        'AND': AND,
        'OR': OR,
        'NOT': NOT,
        'id': id,
        'name': name,
        'phone': phone,
        'birthday': birthday,
        'car_id': carId,
        'car': car,
        'Expensive': expensive,
      };
}

class UserListRelationFilter
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserListRelationFilter({
    this.every,
    this.some,
    this.none,
  });

  final _i2.UserWhereInput? every;

  final _i2.UserWhereInput? some;

  final _i2.UserWhereInput? none;

  @override
  Map<String, dynamic> toJson() => {
        'every': every,
        'some': some,
        'none': none,
      };
}

class CarWhereInput implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarWhereInput({
    this.AND,
    this.OR,
    this.NOT,
    this.id,
    this.vin,
    this.modelId,
    this.range,
    this.createdDate,
    this.voluem,
    this.engineId,
    this.model,
    this.engine,
    this.user,
  });

  final _i1.PrismaUnion<_i2.CarWhereInput, Iterable<_i2.CarWhereInput>>? AND;

  final Iterable<_i2.CarWhereInput>? OR;

  final _i1.PrismaUnion<_i2.CarWhereInput, Iterable<_i2.CarWhereInput>>? NOT;

  final _i1.PrismaUnion<_i2.IntFilter, int>? id;

  final _i1.PrismaUnion<_i2.StringFilter, String>? vin;

  final _i1.PrismaUnion<_i2.IntFilter, int>? modelId;

  final _i1.PrismaUnion<_i2.FloatFilter, double>? range;

  final _i1.PrismaUnion<_i2.DateTimeFilter, DateTime>? createdDate;

  final _i1.PrismaUnion<_i2.IntFilter, int>? voluem;

  final _i1.PrismaUnion<_i2.IntFilter, int>? engineId;

  final _i1.PrismaUnion<_i2.ModelRelationFilter, _i2.ModelWhereInput>? model;

  final _i1.PrismaUnion<_i2.EngineRelationFilter, _i2.EngineWhereInput>? engine;

  final _i2.UserListRelationFilter? user;

  @override
  Map<String, dynamic> toJson() => {
        'AND': AND,
        'OR': OR,
        'NOT': NOT,
        'id': id,
        'vin': vin,
        'model_id': modelId,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'engine_id': engineId,
        'model': model,
        'engine': engine,
        'User': user,
      };
}

class CarListRelationFilter
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarListRelationFilter({
    this.every,
    this.some,
    this.none,
  });

  final _i2.CarWhereInput? every;

  final _i2.CarWhereInput? some;

  final _i2.CarWhereInput? none;

  @override
  Map<String, dynamic> toJson() => {
        'every': every,
        'some': some,
        'none': none,
      };
}

class ModelWhereInput implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelWhereInput({
    this.AND,
    this.OR,
    this.NOT,
    this.id,
    this.name,
    this.markId,
    this.countryId,
    this.mark,
    this.country,
    this.car,
  });

  final _i1.PrismaUnion<_i2.ModelWhereInput, Iterable<_i2.ModelWhereInput>>?
      AND;

  final Iterable<_i2.ModelWhereInput>? OR;

  final _i1.PrismaUnion<_i2.ModelWhereInput, Iterable<_i2.ModelWhereInput>>?
      NOT;

  final _i1.PrismaUnion<_i2.IntFilter, int>? id;

  final _i1.PrismaUnion<_i2.StringFilter, String>? name;

  final _i1.PrismaUnion<_i2.IntFilter, int>? markId;

  final _i1.PrismaUnion<_i2.IntFilter, int>? countryId;

  final _i1.PrismaUnion<_i2.MarkRelationFilter, _i2.MarkWhereInput>? mark;

  final _i1.PrismaUnion<_i2.CountryRelationFilter, _i2.CountryWhereInput>?
      country;

  final _i2.CarListRelationFilter? car;

  @override
  Map<String, dynamic> toJson() => {
        'AND': AND,
        'OR': OR,
        'NOT': NOT,
        'id': id,
        'name': name,
        'mark_id': markId,
        'country_id': countryId,
        'mark': mark,
        'country': country,
        'Car': car,
      };
}

enum SortOrder implements _i1.PrismaEnum {
  asc._('asc'),
  desc._('desc');

  const SortOrder._(this.name);

  @override
  final String name;
}

class ModelOrderByRelationAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelOrderByRelationAggregateInput({this.$count});

  final _i2.SortOrder? $count;

  @override
  Map<String, dynamic> toJson() => {'_count': $count};
}

class MarkOrderByWithRelationInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkOrderByWithRelationInput({
    this.id,
    this.name,
    this.model,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  final _i2.ModelOrderByRelationAggregateInput? model;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'Model': model,
      };
}

class CountryOrderByWithRelationInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryOrderByWithRelationInput({
    this.id,
    this.name,
    this.code,
    this.model,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  final _i2.SortOrder? code;

  final _i2.ModelOrderByRelationAggregateInput? model;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'code': code,
        'Model': model,
      };
}

class CarOrderByRelationAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarOrderByRelationAggregateInput({this.$count});

  final _i2.SortOrder? $count;

  @override
  Map<String, dynamic> toJson() => {'_count': $count};
}

class ModelOrderByWithRelationInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelOrderByWithRelationInput({
    this.id,
    this.name,
    this.markId,
    this.countryId,
    this.mark,
    this.country,
    this.car,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  final _i2.SortOrder? markId;

  final _i2.SortOrder? countryId;

  final _i2.MarkOrderByWithRelationInput? mark;

  final _i2.CountryOrderByWithRelationInput? country;

  final _i2.CarOrderByRelationAggregateInput? car;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'mark_id': markId,
        'country_id': countryId,
        'mark': mark,
        'country': country,
        'Car': car,
      };
}

class ModelWhereUniqueInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelWhereUniqueInput({this.id});

  final int? id;

  @override
  Map<String, dynamic> toJson() => {'id': id};
}

enum ModelScalar<T> implements _i1.PrismaEnum, _i1.Reference<T> {
  id<int>('id', 'Model'),
  name$<String>('name', 'Model'),
  markId<int>('mark_id', 'Model'),
  countryId<int>('country_id', 'Model');

  const ModelScalar(
    this.name,
    this.model,
  );

  @override
  final String name;

  @override
  final String model;
}

class CountryModelArgs implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryModelArgs({
    this.where,
    this.orderBy,
    this.cursor,
    this.take,
    this.skip,
    this.distinct,
    this.select,
    this.include,
  });

  final _i2.ModelWhereInput? where;

  final _i1.PrismaUnion<Iterable<_i2.ModelOrderByWithRelationInput>,
      _i2.ModelOrderByWithRelationInput>? orderBy;

  final _i2.ModelWhereUniqueInput? cursor;

  final int? take;

  final int? skip;

  final _i1.PrismaUnion<_i2.ModelScalar, Iterable<_i2.ModelScalar>>? distinct;

  final _i2.ModelSelect? select;

  final _i2.ModelInclude? include;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'orderBy': orderBy,
        'cursor': cursor,
        'take': take,
        'skip': skip,
        'distinct': distinct,
        'select': select,
        'include': include,
      };
}

class CountryCountOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryCountOutputTypeSelect({this.model});

  final bool? model;

  @override
  Map<String, dynamic> toJson() => {'Model': model};
}

class CountryCountArgs implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryCountArgs({this.select});

  final _i2.CountryCountOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class CountrySelect implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountrySelect({
    this.id,
    this.name,
    this.code,
    this.model,
    this.$count,
  });

  final bool? id;

  final bool? name;

  final bool? code;

  final _i1.PrismaUnion<bool, _i2.CountryModelArgs>? model;

  final _i1.PrismaUnion<bool, _i2.CountryCountArgs>? $count;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'code': code,
        'Model': model,
        '_count': $count,
      };
}

class CountryInclude implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryInclude({
    this.model,
    this.$count,
  });

  final _i1.PrismaUnion<bool, _i2.CountryModelArgs>? model;

  final _i1.PrismaUnion<bool, _i2.CountryCountArgs>? $count;

  @override
  Map<String, dynamic> toJson() => {
        'Model': model,
        '_count': $count,
      };
}

class ModelCountryArgs implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelCountryArgs({
    this.select,
    this.include,
  });

  final _i2.CountrySelect? select;

  final _i2.CountryInclude? include;

  @override
  Map<String, dynamic> toJson() => {
        'select': select,
        'include': include,
      };
}

class CarModelArgs implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarModelArgs({
    this.select,
    this.include,
  });

  final _i2.ModelSelect? select;

  final _i2.ModelInclude? include;

  @override
  Map<String, dynamic> toJson() => {
        'select': select,
        'include': include,
      };
}

class EngineOrderByWithRelationInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineOrderByWithRelationInput({
    this.id,
    this.name,
    this.power,
    this.car,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  final _i2.SortOrder? power;

  final _i2.CarOrderByRelationAggregateInput? car;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'power': power,
        'Car': car,
      };
}

class UserOrderByRelationAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserOrderByRelationAggregateInput({this.$count});

  final _i2.SortOrder? $count;

  @override
  Map<String, dynamic> toJson() => {'_count': $count};
}

class CarOrderByWithRelationInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarOrderByWithRelationInput({
    this.id,
    this.vin,
    this.modelId,
    this.range,
    this.createdDate,
    this.voluem,
    this.engineId,
    this.model,
    this.engine,
    this.user,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? vin;

  final _i2.SortOrder? modelId;

  final _i2.SortOrder? range;

  final _i2.SortOrder? createdDate;

  final _i2.SortOrder? voluem;

  final _i2.SortOrder? engineId;

  final _i2.ModelOrderByWithRelationInput? model;

  final _i2.EngineOrderByWithRelationInput? engine;

  final _i2.UserOrderByRelationAggregateInput? user;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'vin': vin,
        'model_id': modelId,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'engine_id': engineId,
        'model': model,
        'engine': engine,
        'User': user,
      };
}

class CarWhereUniqueInput implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarWhereUniqueInput({this.id});

  final int? id;

  @override
  Map<String, dynamic> toJson() => {'id': id};
}

enum CarScalar<T> implements _i1.PrismaEnum, _i1.Reference<T> {
  id<int>('id', 'Car'),
  vin<String>('vin', 'Car'),
  modelId<int>('model_id', 'Car'),
  range<double>('range', 'Car'),
  createdDate<DateTime>('created_date', 'Car'),
  voluem<int>('voluem', 'Car'),
  engineId<int>('engine_id', 'Car');

  const CarScalar(
    this.name,
    this.model,
  );

  @override
  final String name;

  @override
  final String model;
}

class EngineCarArgs implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineCarArgs({
    this.where,
    this.orderBy,
    this.cursor,
    this.take,
    this.skip,
    this.distinct,
    this.select,
    this.include,
  });

  final _i2.CarWhereInput? where;

  final _i1.PrismaUnion<Iterable<_i2.CarOrderByWithRelationInput>,
      _i2.CarOrderByWithRelationInput>? orderBy;

  final _i2.CarWhereUniqueInput? cursor;

  final int? take;

  final int? skip;

  final _i1.PrismaUnion<_i2.CarScalar, Iterable<_i2.CarScalar>>? distinct;

  final _i2.CarSelect? select;

  final _i2.CarInclude? include;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'orderBy': orderBy,
        'cursor': cursor,
        'take': take,
        'skip': skip,
        'distinct': distinct,
        'select': select,
        'include': include,
      };
}

class EngineCountOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineCountOutputTypeSelect({this.car});

  final bool? car;

  @override
  Map<String, dynamic> toJson() => {'Car': car};
}

class EngineCountArgs implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineCountArgs({this.select});

  final _i2.EngineCountOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class EngineInclude implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineInclude({
    this.car,
    this.$count,
  });

  final _i1.PrismaUnion<bool, _i2.EngineCarArgs>? car;

  final _i1.PrismaUnion<bool, _i2.EngineCountArgs>? $count;

  @override
  Map<String, dynamic> toJson() => {
        'Car': car,
        '_count': $count,
      };
}

class CarEngineArgs implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarEngineArgs({
    this.select,
    this.include,
  });

  final _i2.EngineSelect? select;

  final _i2.EngineInclude? include;

  @override
  Map<String, dynamic> toJson() => {
        'select': select,
        'include': include,
      };
}

class UserCarArgs implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserCarArgs({
    this.select,
    this.include,
  });

  final _i2.CarSelect? select;

  final _i2.CarInclude? include;

  @override
  Map<String, dynamic> toJson() => {
        'select': select,
        'include': include,
      };
}

enum NullsOrder implements _i1.PrismaEnum {
  first._('first'),
  last._('last');

  const NullsOrder._(this.name);

  @override
  final String name;
}

class SortOrderInput implements _i1.JsonConvertible<Map<String, dynamic>> {
  const SortOrderInput({
    required this.sort,
    this.nulls,
  });

  final _i2.SortOrder sort;

  final _i2.NullsOrder? nulls;

  @override
  Map<String, dynamic> toJson() => {
        'sort': sort,
        'nulls': nulls,
      };
}

class ExpensiveOrderByRelationAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveOrderByRelationAggregateInput({this.$count});

  final _i2.SortOrder? $count;

  @override
  Map<String, dynamic> toJson() => {'_count': $count};
}

class CategoryOrderByWithRelationInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryOrderByWithRelationInput({
    this.id,
    this.name,
    this.expensive,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  final _i2.ExpensiveOrderByRelationAggregateInput? expensive;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'Expensive': expensive,
      };
}

class UserOrderByWithRelationInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserOrderByWithRelationInput({
    this.id,
    this.name,
    this.phone,
    this.birthday,
    this.carId,
    this.car,
    this.expensive,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  final _i2.SortOrder? phone;

  final _i2.SortOrder? birthday;

  final _i2.SortOrder? carId;

  final _i2.CarOrderByWithRelationInput? car;

  final _i2.ExpensiveOrderByRelationAggregateInput? expensive;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'phone': phone,
        'birthday': birthday,
        'car_id': carId,
        'car': car,
        'Expensive': expensive,
      };
}

class FuelOrderByWithRelationInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelOrderByWithRelationInput({
    this.id,
    this.name,
    this.expensive,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  final _i2.ExpensiveOrderByRelationAggregateInput? expensive;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'Expensive': expensive,
      };
}

class ExpensiveOrderByWithRelationInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveOrderByWithRelationInput({
    this.id,
    this.name,
    this.categoryId,
    this.description,
    this.createdDate,
    this.range,
    this.price,
    this.userId,
    this.fuelTypeId,
    this.liters,
    this.category,
    this.user,
    this.fuel,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  final _i2.SortOrder? categoryId;

  final _i2.SortOrder? description;

  final _i2.SortOrder? createdDate;

  final _i2.SortOrder? range;

  final _i2.SortOrder? price;

  final _i2.SortOrder? userId;

  final _i1.PrismaUnion<_i2.SortOrder, _i2.SortOrderInput>? fuelTypeId;

  final _i1.PrismaUnion<_i2.SortOrder, _i2.SortOrderInput>? liters;

  final _i2.CategoryOrderByWithRelationInput? category;

  final _i2.UserOrderByWithRelationInput? user;

  final _i2.FuelOrderByWithRelationInput? fuel;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'category_id': categoryId,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'user_id': userId,
        'fuel_type_id': fuelTypeId,
        'liters': liters,
        'category': category,
        'User': user,
        'Fuel': fuel,
      };
}

class ExpensiveWhereUniqueInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveWhereUniqueInput({this.id});

  final int? id;

  @override
  Map<String, dynamic> toJson() => {'id': id};
}

enum ExpensiveScalar<T> implements _i1.PrismaEnum, _i1.Reference<T> {
  id<int>('id', 'Expensive'),
  name$<String>('name', 'Expensive'),
  categoryId<int>('category_id', 'Expensive'),
  description<String>('description', 'Expensive'),
  createdDate<DateTime>('created_date', 'Expensive'),
  range<double>('range', 'Expensive'),
  price<double>('price', 'Expensive'),
  userId<int>('user_id', 'Expensive'),
  fuelTypeId<int>('fuel_type_id', 'Expensive'),
  liters<double>('liters', 'Expensive');

  const ExpensiveScalar(
    this.name,
    this.model,
  );

  @override
  final String name;

  @override
  final String model;
}

class CategoryExpensiveArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryExpensiveArgs({
    this.where,
    this.orderBy,
    this.cursor,
    this.take,
    this.skip,
    this.distinct,
    this.select,
    this.include,
  });

  final _i2.ExpensiveWhereInput? where;

  final _i1.PrismaUnion<Iterable<_i2.ExpensiveOrderByWithRelationInput>,
      _i2.ExpensiveOrderByWithRelationInput>? orderBy;

  final _i2.ExpensiveWhereUniqueInput? cursor;

  final int? take;

  final int? skip;

  final _i1.PrismaUnion<_i2.ExpensiveScalar, Iterable<_i2.ExpensiveScalar>>?
      distinct;

  final _i2.ExpensiveSelect? select;

  final _i2.ExpensiveInclude? include;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'orderBy': orderBy,
        'cursor': cursor,
        'take': take,
        'skip': skip,
        'distinct': distinct,
        'select': select,
        'include': include,
      };
}

class CategoryCountOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryCountOutputTypeSelect({this.expensive});

  final bool? expensive;

  @override
  Map<String, dynamic> toJson() => {'Expensive': expensive};
}

class CategoryCountArgs implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryCountArgs({this.select});

  final _i2.CategoryCountOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class CategoryInclude implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryInclude({
    this.expensive,
    this.$count,
  });

  final _i1.PrismaUnion<bool, _i2.CategoryExpensiveArgs>? expensive;

  final _i1.PrismaUnion<bool, _i2.CategoryCountArgs>? $count;

  @override
  Map<String, dynamic> toJson() => {
        'Expensive': expensive,
        '_count': $count,
      };
}

class ExpensiveCategoryArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveCategoryArgs({
    this.select,
    this.include,
  });

  final _i2.CategorySelect? select;

  final _i2.CategoryInclude? include;

  @override
  Map<String, dynamic> toJson() => {
        'select': select,
        'include': include,
      };
}

class UserExpensiveArgs implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserExpensiveArgs({
    this.where,
    this.orderBy,
    this.cursor,
    this.take,
    this.skip,
    this.distinct,
    this.select,
    this.include,
  });

  final _i2.ExpensiveWhereInput? where;

  final _i1.PrismaUnion<Iterable<_i2.ExpensiveOrderByWithRelationInput>,
      _i2.ExpensiveOrderByWithRelationInput>? orderBy;

  final _i2.ExpensiveWhereUniqueInput? cursor;

  final int? take;

  final int? skip;

  final _i1.PrismaUnion<_i2.ExpensiveScalar, Iterable<_i2.ExpensiveScalar>>?
      distinct;

  final _i2.ExpensiveSelect? select;

  final _i2.ExpensiveInclude? include;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'orderBy': orderBy,
        'cursor': cursor,
        'take': take,
        'skip': skip,
        'distinct': distinct,
        'select': select,
        'include': include,
      };
}

class UserCountOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserCountOutputTypeSelect({this.expensive});

  final bool? expensive;

  @override
  Map<String, dynamic> toJson() => {'Expensive': expensive};
}

class UserCountArgs implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserCountArgs({this.select});

  final _i2.UserCountOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class UserInclude implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserInclude({
    this.car,
    this.expensive,
    this.$count,
  });

  final _i1.PrismaUnion<bool, _i2.UserCarArgs>? car;

  final _i1.PrismaUnion<bool, _i2.UserExpensiveArgs>? expensive;

  final _i1.PrismaUnion<bool, _i2.UserCountArgs>? $count;

  @override
  Map<String, dynamic> toJson() => {
        'car': car,
        'Expensive': expensive,
        '_count': $count,
      };
}

class ExpensiveUserArgs implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveUserArgs({
    this.select,
    this.include,
  });

  final _i2.UserSelect? select;

  final _i2.UserInclude? include;

  @override
  Map<String, dynamic> toJson() => {
        'select': select,
        'include': include,
      };
}

class FuelExpensiveArgs implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelExpensiveArgs({
    this.where,
    this.orderBy,
    this.cursor,
    this.take,
    this.skip,
    this.distinct,
    this.select,
    this.include,
  });

  final _i2.ExpensiveWhereInput? where;

  final _i1.PrismaUnion<Iterable<_i2.ExpensiveOrderByWithRelationInput>,
      _i2.ExpensiveOrderByWithRelationInput>? orderBy;

  final _i2.ExpensiveWhereUniqueInput? cursor;

  final int? take;

  final int? skip;

  final _i1.PrismaUnion<_i2.ExpensiveScalar, Iterable<_i2.ExpensiveScalar>>?
      distinct;

  final _i2.ExpensiveSelect? select;

  final _i2.ExpensiveInclude? include;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'orderBy': orderBy,
        'cursor': cursor,
        'take': take,
        'skip': skip,
        'distinct': distinct,
        'select': select,
        'include': include,
      };
}

class FuelCountOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelCountOutputTypeSelect({this.expensive});

  final bool? expensive;

  @override
  Map<String, dynamic> toJson() => {'Expensive': expensive};
}

class FuelCountArgs implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelCountArgs({this.select});

  final _i2.FuelCountOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class FuelSelect implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelSelect({
    this.id,
    this.name,
    this.expensive,
    this.$count,
  });

  final bool? id;

  final bool? name;

  final _i1.PrismaUnion<bool, _i2.FuelExpensiveArgs>? expensive;

  final _i1.PrismaUnion<bool, _i2.FuelCountArgs>? $count;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'Expensive': expensive,
        '_count': $count,
      };
}

class FuelInclude implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelInclude({
    this.expensive,
    this.$count,
  });

  final _i1.PrismaUnion<bool, _i2.FuelExpensiveArgs>? expensive;

  final _i1.PrismaUnion<bool, _i2.FuelCountArgs>? $count;

  @override
  Map<String, dynamic> toJson() => {
        'Expensive': expensive,
        '_count': $count,
      };
}

class ExpensiveFuelArgs implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveFuelArgs({
    this.select,
    this.include,
  });

  final _i2.FuelSelect? select;

  final _i2.FuelInclude? include;

  @override
  Map<String, dynamic> toJson() => {
        'select': select,
        'include': include,
      };
}

class ExpensiveInclude implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveInclude({
    this.category,
    this.user,
    this.fuel,
  });

  final _i1.PrismaUnion<bool, _i2.ExpensiveCategoryArgs>? category;

  final _i1.PrismaUnion<bool, _i2.ExpensiveUserArgs>? user;

  final _i1.PrismaUnion<bool, _i2.ExpensiveFuelArgs>? fuel;

  @override
  Map<String, dynamic> toJson() => {
        'category': category,
        'User': user,
        'Fuel': fuel,
      };
}

class CategorySelect implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategorySelect({
    this.id,
    this.name,
    this.expensive,
    this.$count,
  });

  final bool? id;

  final bool? name;

  final _i1.PrismaUnion<bool, _i2.CategoryExpensiveArgs>? expensive;

  final _i1.PrismaUnion<bool, _i2.CategoryCountArgs>? $count;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'Expensive': expensive,
        '_count': $count,
      };
}

class ExpensiveSelect implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveSelect({
    this.id,
    this.name,
    this.categoryId,
    this.description,
    this.createdDate,
    this.range,
    this.price,
    this.userId,
    this.fuelTypeId,
    this.liters,
    this.category,
    this.user,
    this.fuel,
  });

  final bool? id;

  final bool? name;

  final bool? categoryId;

  final bool? description;

  final bool? createdDate;

  final bool? range;

  final bool? price;

  final bool? userId;

  final bool? fuelTypeId;

  final bool? liters;

  final _i1.PrismaUnion<bool, _i2.ExpensiveCategoryArgs>? category;

  final _i1.PrismaUnion<bool, _i2.ExpensiveUserArgs>? user;

  final _i1.PrismaUnion<bool, _i2.ExpensiveFuelArgs>? fuel;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'category_id': categoryId,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'user_id': userId,
        'fuel_type_id': fuelTypeId,
        'liters': liters,
        'category': category,
        'User': user,
        'Fuel': fuel,
      };
}

class UserSelect implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserSelect({
    this.id,
    this.name,
    this.phone,
    this.birthday,
    this.carId,
    this.car,
    this.expensive,
    this.$count,
  });

  final bool? id;

  final bool? name;

  final bool? phone;

  final bool? birthday;

  final bool? carId;

  final _i1.PrismaUnion<bool, _i2.UserCarArgs>? car;

  final _i1.PrismaUnion<bool, _i2.UserExpensiveArgs>? expensive;

  final _i1.PrismaUnion<bool, _i2.UserCountArgs>? $count;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'phone': phone,
        'birthday': birthday,
        'car_id': carId,
        'car': car,
        'Expensive': expensive,
        '_count': $count,
      };
}

class UserWhereUniqueInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserWhereUniqueInput({this.id});

  final int? id;

  @override
  Map<String, dynamic> toJson() => {'id': id};
}

enum UserScalar<T> implements _i1.PrismaEnum, _i1.Reference<T> {
  id<int>('id', 'User'),
  name$<String>('name', 'User'),
  phone<String>('phone', 'User'),
  birthday<DateTime>('birthday', 'User'),
  carId<int>('car_id', 'User');

  const UserScalar(
    this.name,
    this.model,
  );

  @override
  final String name;

  @override
  final String model;
}

class CarUserArgs implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarUserArgs({
    this.where,
    this.orderBy,
    this.cursor,
    this.take,
    this.skip,
    this.distinct,
    this.select,
    this.include,
  });

  final _i2.UserWhereInput? where;

  final _i1.PrismaUnion<Iterable<_i2.UserOrderByWithRelationInput>,
      _i2.UserOrderByWithRelationInput>? orderBy;

  final _i2.UserWhereUniqueInput? cursor;

  final int? take;

  final int? skip;

  final _i1.PrismaUnion<_i2.UserScalar, Iterable<_i2.UserScalar>>? distinct;

  final _i2.UserSelect? select;

  final _i2.UserInclude? include;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'orderBy': orderBy,
        'cursor': cursor,
        'take': take,
        'skip': skip,
        'distinct': distinct,
        'select': select,
        'include': include,
      };
}

class CarCountOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarCountOutputTypeSelect({this.user});

  final bool? user;

  @override
  Map<String, dynamic> toJson() => {'User': user};
}

class CarCountArgs implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarCountArgs({this.select});

  final _i2.CarCountOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class CarInclude implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarInclude({
    this.model,
    this.engine,
    this.user,
    this.$count,
  });

  final _i1.PrismaUnion<bool, _i2.CarModelArgs>? model;

  final _i1.PrismaUnion<bool, _i2.CarEngineArgs>? engine;

  final _i1.PrismaUnion<bool, _i2.CarUserArgs>? user;

  final _i1.PrismaUnion<bool, _i2.CarCountArgs>? $count;

  @override
  Map<String, dynamic> toJson() => {
        'model': model,
        'engine': engine,
        'User': user,
        '_count': $count,
      };
}

class EngineSelect implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineSelect({
    this.id,
    this.name,
    this.power,
    this.car,
    this.$count,
  });

  final bool? id;

  final bool? name;

  final bool? power;

  final _i1.PrismaUnion<bool, _i2.EngineCarArgs>? car;

  final _i1.PrismaUnion<bool, _i2.EngineCountArgs>? $count;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'power': power,
        'Car': car,
        '_count': $count,
      };
}

class CarSelect implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarSelect({
    this.id,
    this.vin,
    this.modelId,
    this.range,
    this.createdDate,
    this.voluem,
    this.engineId,
    this.model,
    this.engine,
    this.user,
    this.$count,
  });

  final bool? id;

  final bool? vin;

  final bool? modelId;

  final bool? range;

  final bool? createdDate;

  final bool? voluem;

  final bool? engineId;

  final _i1.PrismaUnion<bool, _i2.CarModelArgs>? model;

  final _i1.PrismaUnion<bool, _i2.CarEngineArgs>? engine;

  final _i1.PrismaUnion<bool, _i2.CarUserArgs>? user;

  final _i1.PrismaUnion<bool, _i2.CarCountArgs>? $count;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'vin': vin,
        'model_id': modelId,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'engine_id': engineId,
        'model': model,
        'engine': engine,
        'User': user,
        '_count': $count,
      };
}

class ModelCarArgs implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelCarArgs({
    this.where,
    this.orderBy,
    this.cursor,
    this.take,
    this.skip,
    this.distinct,
    this.select,
    this.include,
  });

  final _i2.CarWhereInput? where;

  final _i1.PrismaUnion<Iterable<_i2.CarOrderByWithRelationInput>,
      _i2.CarOrderByWithRelationInput>? orderBy;

  final _i2.CarWhereUniqueInput? cursor;

  final int? take;

  final int? skip;

  final _i1.PrismaUnion<_i2.CarScalar, Iterable<_i2.CarScalar>>? distinct;

  final _i2.CarSelect? select;

  final _i2.CarInclude? include;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'orderBy': orderBy,
        'cursor': cursor,
        'take': take,
        'skip': skip,
        'distinct': distinct,
        'select': select,
        'include': include,
      };
}

class ModelCountOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelCountOutputTypeSelect({this.car});

  final bool? car;

  @override
  Map<String, dynamic> toJson() => {'Car': car};
}

class ModelCountArgs implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelCountArgs({this.select});

  final _i2.ModelCountOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class ModelInclude implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelInclude({
    this.mark,
    this.country,
    this.car,
    this.$count,
  });

  final _i1.PrismaUnion<bool, _i2.ModelMarkArgs>? mark;

  final _i1.PrismaUnion<bool, _i2.ModelCountryArgs>? country;

  final _i1.PrismaUnion<bool, _i2.ModelCarArgs>? car;

  final _i1.PrismaUnion<bool, _i2.ModelCountArgs>? $count;

  @override
  Map<String, dynamic> toJson() => {
        'mark': mark,
        'country': country,
        'Car': car,
        '_count': $count,
      };
}

class MarkModelArgs implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkModelArgs({
    this.where,
    this.orderBy,
    this.cursor,
    this.take,
    this.skip,
    this.distinct,
    this.select,
    this.include,
  });

  final _i2.ModelWhereInput? where;

  final _i1.PrismaUnion<Iterable<_i2.ModelOrderByWithRelationInput>,
      _i2.ModelOrderByWithRelationInput>? orderBy;

  final _i2.ModelWhereUniqueInput? cursor;

  final int? take;

  final int? skip;

  final _i1.PrismaUnion<_i2.ModelScalar, Iterable<_i2.ModelScalar>>? distinct;

  final _i2.ModelSelect? select;

  final _i2.ModelInclude? include;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'orderBy': orderBy,
        'cursor': cursor,
        'take': take,
        'skip': skip,
        'distinct': distinct,
        'select': select,
        'include': include,
      };
}

class MarkCountOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkCountOutputTypeSelect({this.model});

  final bool? model;

  @override
  Map<String, dynamic> toJson() => {'Model': model};
}

class MarkCountArgs implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkCountArgs({this.select});

  final _i2.MarkCountOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class MarkInclude implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkInclude({
    this.model,
    this.$count,
  });

  final _i1.PrismaUnion<bool, _i2.MarkModelArgs>? model;

  final _i1.PrismaUnion<bool, _i2.MarkCountArgs>? $count;

  @override
  Map<String, dynamic> toJson() => {
        'Model': model,
        '_count': $count,
      };
}

class ModelSelect implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelSelect({
    this.id,
    this.name,
    this.markId,
    this.countryId,
    this.mark,
    this.country,
    this.car,
    this.$count,
  });

  final bool? id;

  final bool? name;

  final bool? markId;

  final bool? countryId;

  final _i1.PrismaUnion<bool, _i2.ModelMarkArgs>? mark;

  final _i1.PrismaUnion<bool, _i2.ModelCountryArgs>? country;

  final _i1.PrismaUnion<bool, _i2.ModelCarArgs>? car;

  final _i1.PrismaUnion<bool, _i2.ModelCountArgs>? $count;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'mark_id': markId,
        'country_id': countryId,
        'mark': mark,
        'country': country,
        'Car': car,
        '_count': $count,
      };
}

class MarkSelect implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkSelect({
    this.id,
    this.name,
    this.model,
    this.$count,
  });

  final bool? id;

  final bool? name;

  final _i1.PrismaUnion<bool, _i2.MarkModelArgs>? model;

  final _i1.PrismaUnion<bool, _i2.MarkCountArgs>? $count;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'Model': model,
        '_count': $count,
      };
}

enum MarkScalar<T> implements _i1.PrismaEnum, _i1.Reference<T> {
  id<int>('id', 'Mark'),
  name$<String>('name', 'Mark');

  const MarkScalar(
    this.name,
    this.model,
  );

  @override
  final String name;

  @override
  final String model;
}

class CountryCreateWithoutModelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryCreateWithoutModelInput({
    required this.name,
    required this.code,
  });

  final String name;

  final String code;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'code': code,
      };
}

class CountryUncheckedCreateWithoutModelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryUncheckedCreateWithoutModelInput({
    this.id,
    required this.name,
    required this.code,
  });

  final int? id;

  final String name;

  final String code;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'code': code,
      };
}

class CountryWhereUniqueInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryWhereUniqueInput({this.id});

  final int? id;

  @override
  Map<String, dynamic> toJson() => {'id': id};
}

class CountryCreateOrConnectWithoutModelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryCreateOrConnectWithoutModelInput({
    required this.where,
    required this.create,
  });

  final _i2.CountryWhereUniqueInput where;

  final _i1.PrismaUnion<_i2.CountryCreateWithoutModelInput,
      _i2.CountryUncheckedCreateWithoutModelInput> create;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'create': create,
      };
}

class CountryCreateNestedOneWithoutModelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryCreateNestedOneWithoutModelInput({
    this.create,
    this.connectOrCreate,
    this.connect,
  });

  final _i1.PrismaUnion<_i2.CountryCreateWithoutModelInput,
      _i2.CountryUncheckedCreateWithoutModelInput>? create;

  final _i2.CountryCreateOrConnectWithoutModelInput? connectOrCreate;

  final _i2.CountryWhereUniqueInput? connect;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'connect': connect,
      };
}

class EngineCreateWithoutCarInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineCreateWithoutCarInput({
    required this.name,
    required this.power,
  });

  final String name;

  final int power;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'power': power,
      };
}

class EngineUncheckedCreateWithoutCarInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineUncheckedCreateWithoutCarInput({
    this.id,
    required this.name,
    required this.power,
  });

  final int? id;

  final String name;

  final int power;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'power': power,
      };
}

class EngineWhereUniqueInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineWhereUniqueInput({this.id});

  final int? id;

  @override
  Map<String, dynamic> toJson() => {'id': id};
}

class EngineCreateOrConnectWithoutCarInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineCreateOrConnectWithoutCarInput({
    required this.where,
    required this.create,
  });

  final _i2.EngineWhereUniqueInput where;

  final _i1.PrismaUnion<_i2.EngineCreateWithoutCarInput,
      _i2.EngineUncheckedCreateWithoutCarInput> create;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'create': create,
      };
}

class EngineCreateNestedOneWithoutCarInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineCreateNestedOneWithoutCarInput({
    this.create,
    this.connectOrCreate,
    this.connect,
  });

  final _i1.PrismaUnion<_i2.EngineCreateWithoutCarInput,
      _i2.EngineUncheckedCreateWithoutCarInput>? create;

  final _i2.EngineCreateOrConnectWithoutCarInput? connectOrCreate;

  final _i2.EngineWhereUniqueInput? connect;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'connect': connect,
      };
}

class CategoryCreateWithoutExpensiveInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryCreateWithoutExpensiveInput({required this.name});

  final String name;

  @override
  Map<String, dynamic> toJson() => {'name': name};
}

class CategoryUncheckedCreateWithoutExpensiveInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryUncheckedCreateWithoutExpensiveInput({
    this.id,
    required this.name,
  });

  final int? id;

  final String name;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
      };
}

class CategoryWhereUniqueInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryWhereUniqueInput({this.id});

  final int? id;

  @override
  Map<String, dynamic> toJson() => {'id': id};
}

class CategoryCreateOrConnectWithoutExpensiveInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryCreateOrConnectWithoutExpensiveInput({
    required this.where,
    required this.create,
  });

  final _i2.CategoryWhereUniqueInput where;

  final _i1.PrismaUnion<_i2.CategoryCreateWithoutExpensiveInput,
      _i2.CategoryUncheckedCreateWithoutExpensiveInput> create;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'create': create,
      };
}

class CategoryCreateNestedOneWithoutExpensiveInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryCreateNestedOneWithoutExpensiveInput({
    this.create,
    this.connectOrCreate,
    this.connect,
  });

  final _i1.PrismaUnion<_i2.CategoryCreateWithoutExpensiveInput,
      _i2.CategoryUncheckedCreateWithoutExpensiveInput>? create;

  final _i2.CategoryCreateOrConnectWithoutExpensiveInput? connectOrCreate;

  final _i2.CategoryWhereUniqueInput? connect;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'connect': connect,
      };
}

class FuelCreateWithoutExpensiveInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelCreateWithoutExpensiveInput({required this.name});

  final String name;

  @override
  Map<String, dynamic> toJson() => {'name': name};
}

class FuelUncheckedCreateWithoutExpensiveInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelUncheckedCreateWithoutExpensiveInput({
    this.id,
    required this.name,
  });

  final int? id;

  final String name;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
      };
}

class FuelWhereUniqueInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelWhereUniqueInput({this.id});

  final int? id;

  @override
  Map<String, dynamic> toJson() => {'id': id};
}

class FuelCreateOrConnectWithoutExpensiveInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelCreateOrConnectWithoutExpensiveInput({
    required this.where,
    required this.create,
  });

  final _i2.FuelWhereUniqueInput where;

  final _i1.PrismaUnion<_i2.FuelCreateWithoutExpensiveInput,
      _i2.FuelUncheckedCreateWithoutExpensiveInput> create;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'create': create,
      };
}

class FuelCreateNestedOneWithoutExpensiveInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelCreateNestedOneWithoutExpensiveInput({
    this.create,
    this.connectOrCreate,
    this.connect,
  });

  final _i1.PrismaUnion<_i2.FuelCreateWithoutExpensiveInput,
      _i2.FuelUncheckedCreateWithoutExpensiveInput>? create;

  final _i2.FuelCreateOrConnectWithoutExpensiveInput? connectOrCreate;

  final _i2.FuelWhereUniqueInput? connect;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'connect': connect,
      };
}

class ExpensiveCreateWithoutUserInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveCreateWithoutUserInput({
    required this.name,
    required this.description,
    required this.createdDate,
    required this.range,
    required this.price,
    this.liters,
    required this.category,
    this.fuel,
  });

  final String name;

  final String description;

  final DateTime createdDate;

  final double range;

  final double price;

  final _i1.PrismaUnion<double, _i1.PrismaNull>? liters;

  final _i2.CategoryCreateNestedOneWithoutExpensiveInput category;

  final _i2.FuelCreateNestedOneWithoutExpensiveInput? fuel;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'liters': liters,
        'category': category,
        'Fuel': fuel,
      };
}

class ExpensiveUncheckedCreateWithoutUserInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveUncheckedCreateWithoutUserInput({
    this.id,
    required this.name,
    required this.categoryId,
    required this.description,
    required this.createdDate,
    required this.range,
    required this.price,
    this.fuelTypeId,
    this.liters,
  });

  final int? id;

  final String name;

  final int categoryId;

  final String description;

  final DateTime createdDate;

  final double range;

  final double price;

  final _i1.PrismaUnion<int, _i1.PrismaNull>? fuelTypeId;

  final _i1.PrismaUnion<double, _i1.PrismaNull>? liters;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'category_id': categoryId,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'fuel_type_id': fuelTypeId,
        'liters': liters,
      };
}

class ExpensiveCreateOrConnectWithoutUserInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveCreateOrConnectWithoutUserInput({
    required this.where,
    required this.create,
  });

  final _i2.ExpensiveWhereUniqueInput where;

  final _i1.PrismaUnion<_i2.ExpensiveCreateWithoutUserInput,
      _i2.ExpensiveUncheckedCreateWithoutUserInput> create;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'create': create,
      };
}

class ExpensiveCreateManyUserInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveCreateManyUserInput({
    this.id,
    required this.name,
    required this.categoryId,
    required this.description,
    required this.createdDate,
    required this.range,
    required this.price,
    this.fuelTypeId,
    this.liters,
  });

  final int? id;

  final String name;

  final int categoryId;

  final String description;

  final DateTime createdDate;

  final double range;

  final double price;

  final _i1.PrismaUnion<int, _i1.PrismaNull>? fuelTypeId;

  final _i1.PrismaUnion<double, _i1.PrismaNull>? liters;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'category_id': categoryId,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'fuel_type_id': fuelTypeId,
        'liters': liters,
      };
}

class ExpensiveCreateManyUserInputEnvelope
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveCreateManyUserInputEnvelope({
    required this.data,
    this.skipDuplicates,
  });

  final _i1.PrismaUnion<_i2.ExpensiveCreateManyUserInput,
      Iterable<_i2.ExpensiveCreateManyUserInput>> data;

  final bool? skipDuplicates;

  @override
  Map<String, dynamic> toJson() => {
        'data': data,
        'skipDuplicates': skipDuplicates,
      };
}

class ExpensiveCreateNestedManyWithoutUserInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveCreateNestedManyWithoutUserInput({
    this.create,
    this.connectOrCreate,
    this.createMany,
    this.connect,
  });

  final _i1.PrismaUnion<
      _i2.ExpensiveCreateWithoutUserInput,
      _i1.PrismaUnion<
          Iterable<_i2.ExpensiveCreateWithoutUserInput>,
          _i1.PrismaUnion<_i2.ExpensiveUncheckedCreateWithoutUserInput,
              Iterable<_i2.ExpensiveUncheckedCreateWithoutUserInput>>>>? create;

  final _i1.PrismaUnion<_i2.ExpensiveCreateOrConnectWithoutUserInput,
      Iterable<_i2.ExpensiveCreateOrConnectWithoutUserInput>>? connectOrCreate;

  final _i2.ExpensiveCreateManyUserInputEnvelope? createMany;

  final _i1.PrismaUnion<_i2.ExpensiveWhereUniqueInput,
      Iterable<_i2.ExpensiveWhereUniqueInput>>? connect;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'createMany': createMany,
        'connect': connect,
      };
}

class UserCreateWithoutCarInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserCreateWithoutCarInput({
    required this.name,
    required this.phone,
    required this.birthday,
    this.expensive,
  });

  final String name;

  final String phone;

  final DateTime birthday;

  final _i2.ExpensiveCreateNestedManyWithoutUserInput? expensive;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'phone': phone,
        'birthday': birthday,
        'Expensive': expensive,
      };
}

class ExpensiveUncheckedCreateNestedManyWithoutUserInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveUncheckedCreateNestedManyWithoutUserInput({
    this.create,
    this.connectOrCreate,
    this.createMany,
    this.connect,
  });

  final _i1.PrismaUnion<
      _i2.ExpensiveCreateWithoutUserInput,
      _i1.PrismaUnion<
          Iterable<_i2.ExpensiveCreateWithoutUserInput>,
          _i1.PrismaUnion<_i2.ExpensiveUncheckedCreateWithoutUserInput,
              Iterable<_i2.ExpensiveUncheckedCreateWithoutUserInput>>>>? create;

  final _i1.PrismaUnion<_i2.ExpensiveCreateOrConnectWithoutUserInput,
      Iterable<_i2.ExpensiveCreateOrConnectWithoutUserInput>>? connectOrCreate;

  final _i2.ExpensiveCreateManyUserInputEnvelope? createMany;

  final _i1.PrismaUnion<_i2.ExpensiveWhereUniqueInput,
      Iterable<_i2.ExpensiveWhereUniqueInput>>? connect;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'createMany': createMany,
        'connect': connect,
      };
}

class UserUncheckedCreateWithoutCarInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserUncheckedCreateWithoutCarInput({
    this.id,
    required this.name,
    required this.phone,
    required this.birthday,
    this.expensive,
  });

  final int? id;

  final String name;

  final String phone;

  final DateTime birthday;

  final _i2.ExpensiveUncheckedCreateNestedManyWithoutUserInput? expensive;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'phone': phone,
        'birthday': birthday,
        'Expensive': expensive,
      };
}

class UserCreateOrConnectWithoutCarInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserCreateOrConnectWithoutCarInput({
    required this.where,
    required this.create,
  });

  final _i2.UserWhereUniqueInput where;

  final _i1.PrismaUnion<_i2.UserCreateWithoutCarInput,
      _i2.UserUncheckedCreateWithoutCarInput> create;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'create': create,
      };
}

class UserCreateManyCarInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserCreateManyCarInput({
    this.id,
    required this.name,
    required this.phone,
    required this.birthday,
  });

  final int? id;

  final String name;

  final String phone;

  final DateTime birthday;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'phone': phone,
        'birthday': birthday,
      };
}

class UserCreateManyCarInputEnvelope
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserCreateManyCarInputEnvelope({
    required this.data,
    this.skipDuplicates,
  });

  final _i1.PrismaUnion<_i2.UserCreateManyCarInput,
      Iterable<_i2.UserCreateManyCarInput>> data;

  final bool? skipDuplicates;

  @override
  Map<String, dynamic> toJson() => {
        'data': data,
        'skipDuplicates': skipDuplicates,
      };
}

class UserCreateNestedManyWithoutCarInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserCreateNestedManyWithoutCarInput({
    this.create,
    this.connectOrCreate,
    this.createMany,
    this.connect,
  });

  final _i1.PrismaUnion<
      _i2.UserCreateWithoutCarInput,
      _i1.PrismaUnion<
          Iterable<_i2.UserCreateWithoutCarInput>,
          _i1.PrismaUnion<_i2.UserUncheckedCreateWithoutCarInput,
              Iterable<_i2.UserUncheckedCreateWithoutCarInput>>>>? create;

  final _i1.PrismaUnion<_i2.UserCreateOrConnectWithoutCarInput,
      Iterable<_i2.UserCreateOrConnectWithoutCarInput>>? connectOrCreate;

  final _i2.UserCreateManyCarInputEnvelope? createMany;

  final _i1.PrismaUnion<_i2.UserWhereUniqueInput,
      Iterable<_i2.UserWhereUniqueInput>>? connect;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'createMany': createMany,
        'connect': connect,
      };
}

class CarCreateWithoutModelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarCreateWithoutModelInput({
    required this.vin,
    required this.range,
    required this.createdDate,
    required this.voluem,
    required this.engine,
    this.user,
  });

  final String vin;

  final double range;

  final DateTime createdDate;

  final int voluem;

  final _i2.EngineCreateNestedOneWithoutCarInput engine;

  final _i2.UserCreateNestedManyWithoutCarInput? user;

  @override
  Map<String, dynamic> toJson() => {
        'vin': vin,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'engine': engine,
        'User': user,
      };
}

class UserUncheckedCreateNestedManyWithoutCarInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserUncheckedCreateNestedManyWithoutCarInput({
    this.create,
    this.connectOrCreate,
    this.createMany,
    this.connect,
  });

  final _i1.PrismaUnion<
      _i2.UserCreateWithoutCarInput,
      _i1.PrismaUnion<
          Iterable<_i2.UserCreateWithoutCarInput>,
          _i1.PrismaUnion<_i2.UserUncheckedCreateWithoutCarInput,
              Iterable<_i2.UserUncheckedCreateWithoutCarInput>>>>? create;

  final _i1.PrismaUnion<_i2.UserCreateOrConnectWithoutCarInput,
      Iterable<_i2.UserCreateOrConnectWithoutCarInput>>? connectOrCreate;

  final _i2.UserCreateManyCarInputEnvelope? createMany;

  final _i1.PrismaUnion<_i2.UserWhereUniqueInput,
      Iterable<_i2.UserWhereUniqueInput>>? connect;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'createMany': createMany,
        'connect': connect,
      };
}

class CarUncheckedCreateWithoutModelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarUncheckedCreateWithoutModelInput({
    this.id,
    required this.vin,
    required this.range,
    required this.createdDate,
    required this.voluem,
    required this.engineId,
    this.user,
  });

  final int? id;

  final String vin;

  final double range;

  final DateTime createdDate;

  final int voluem;

  final int engineId;

  final _i2.UserUncheckedCreateNestedManyWithoutCarInput? user;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'vin': vin,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'engine_id': engineId,
        'User': user,
      };
}

class CarCreateOrConnectWithoutModelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarCreateOrConnectWithoutModelInput({
    required this.where,
    required this.create,
  });

  final _i2.CarWhereUniqueInput where;

  final _i1.PrismaUnion<_i2.CarCreateWithoutModelInput,
      _i2.CarUncheckedCreateWithoutModelInput> create;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'create': create,
      };
}

class CarCreateManyModelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarCreateManyModelInput({
    this.id,
    required this.vin,
    required this.range,
    required this.createdDate,
    required this.voluem,
    required this.engineId,
  });

  final int? id;

  final String vin;

  final double range;

  final DateTime createdDate;

  final int voluem;

  final int engineId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'vin': vin,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'engine_id': engineId,
      };
}

class CarCreateManyModelInputEnvelope
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarCreateManyModelInputEnvelope({
    required this.data,
    this.skipDuplicates,
  });

  final _i1.PrismaUnion<_i2.CarCreateManyModelInput,
      Iterable<_i2.CarCreateManyModelInput>> data;

  final bool? skipDuplicates;

  @override
  Map<String, dynamic> toJson() => {
        'data': data,
        'skipDuplicates': skipDuplicates,
      };
}

class CarCreateNestedManyWithoutModelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarCreateNestedManyWithoutModelInput({
    this.create,
    this.connectOrCreate,
    this.createMany,
    this.connect,
  });

  final _i1.PrismaUnion<
      _i2.CarCreateWithoutModelInput,
      _i1.PrismaUnion<
          Iterable<_i2.CarCreateWithoutModelInput>,
          _i1.PrismaUnion<_i2.CarUncheckedCreateWithoutModelInput,
              Iterable<_i2.CarUncheckedCreateWithoutModelInput>>>>? create;

  final _i1.PrismaUnion<_i2.CarCreateOrConnectWithoutModelInput,
      Iterable<_i2.CarCreateOrConnectWithoutModelInput>>? connectOrCreate;

  final _i2.CarCreateManyModelInputEnvelope? createMany;

  final _i1
      .PrismaUnion<_i2.CarWhereUniqueInput, Iterable<_i2.CarWhereUniqueInput>>?
      connect;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'createMany': createMany,
        'connect': connect,
      };
}

class ModelCreateWithoutMarkInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelCreateWithoutMarkInput({
    required this.name,
    required this.country,
    this.car,
  });

  final String name;

  final _i2.CountryCreateNestedOneWithoutModelInput country;

  final _i2.CarCreateNestedManyWithoutModelInput? car;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'country': country,
        'Car': car,
      };
}

class CarUncheckedCreateNestedManyWithoutModelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarUncheckedCreateNestedManyWithoutModelInput({
    this.create,
    this.connectOrCreate,
    this.createMany,
    this.connect,
  });

  final _i1.PrismaUnion<
      _i2.CarCreateWithoutModelInput,
      _i1.PrismaUnion<
          Iterable<_i2.CarCreateWithoutModelInput>,
          _i1.PrismaUnion<_i2.CarUncheckedCreateWithoutModelInput,
              Iterable<_i2.CarUncheckedCreateWithoutModelInput>>>>? create;

  final _i1.PrismaUnion<_i2.CarCreateOrConnectWithoutModelInput,
      Iterable<_i2.CarCreateOrConnectWithoutModelInput>>? connectOrCreate;

  final _i2.CarCreateManyModelInputEnvelope? createMany;

  final _i1
      .PrismaUnion<_i2.CarWhereUniqueInput, Iterable<_i2.CarWhereUniqueInput>>?
      connect;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'createMany': createMany,
        'connect': connect,
      };
}

class ModelUncheckedCreateWithoutMarkInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelUncheckedCreateWithoutMarkInput({
    this.id,
    required this.name,
    required this.countryId,
    this.car,
  });

  final int? id;

  final String name;

  final int countryId;

  final _i2.CarUncheckedCreateNestedManyWithoutModelInput? car;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'country_id': countryId,
        'Car': car,
      };
}

class ModelCreateOrConnectWithoutMarkInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelCreateOrConnectWithoutMarkInput({
    required this.where,
    required this.create,
  });

  final _i2.ModelWhereUniqueInput where;

  final _i1.PrismaUnion<_i2.ModelCreateWithoutMarkInput,
      _i2.ModelUncheckedCreateWithoutMarkInput> create;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'create': create,
      };
}

class ModelCreateManyMarkInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelCreateManyMarkInput({
    this.id,
    required this.name,
    required this.countryId,
  });

  final int? id;

  final String name;

  final int countryId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'country_id': countryId,
      };
}

class ModelCreateManyMarkInputEnvelope
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelCreateManyMarkInputEnvelope({
    required this.data,
    this.skipDuplicates,
  });

  final _i1.PrismaUnion<_i2.ModelCreateManyMarkInput,
      Iterable<_i2.ModelCreateManyMarkInput>> data;

  final bool? skipDuplicates;

  @override
  Map<String, dynamic> toJson() => {
        'data': data,
        'skipDuplicates': skipDuplicates,
      };
}

class ModelCreateNestedManyWithoutMarkInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelCreateNestedManyWithoutMarkInput({
    this.create,
    this.connectOrCreate,
    this.createMany,
    this.connect,
  });

  final _i1.PrismaUnion<
      _i2.ModelCreateWithoutMarkInput,
      _i1.PrismaUnion<
          Iterable<_i2.ModelCreateWithoutMarkInput>,
          _i1.PrismaUnion<_i2.ModelUncheckedCreateWithoutMarkInput,
              Iterable<_i2.ModelUncheckedCreateWithoutMarkInput>>>>? create;

  final _i1.PrismaUnion<_i2.ModelCreateOrConnectWithoutMarkInput,
      Iterable<_i2.ModelCreateOrConnectWithoutMarkInput>>? connectOrCreate;

  final _i2.ModelCreateManyMarkInputEnvelope? createMany;

  final _i1.PrismaUnion<_i2.ModelWhereUniqueInput,
      Iterable<_i2.ModelWhereUniqueInput>>? connect;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'createMany': createMany,
        'connect': connect,
      };
}

class MarkCreateInput implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkCreateInput({
    required this.name,
    this.model,
  });

  final String name;

  final _i2.ModelCreateNestedManyWithoutMarkInput? model;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'Model': model,
      };
}

class ModelUncheckedCreateNestedManyWithoutMarkInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelUncheckedCreateNestedManyWithoutMarkInput({
    this.create,
    this.connectOrCreate,
    this.createMany,
    this.connect,
  });

  final _i1.PrismaUnion<
      _i2.ModelCreateWithoutMarkInput,
      _i1.PrismaUnion<
          Iterable<_i2.ModelCreateWithoutMarkInput>,
          _i1.PrismaUnion<_i2.ModelUncheckedCreateWithoutMarkInput,
              Iterable<_i2.ModelUncheckedCreateWithoutMarkInput>>>>? create;

  final _i1.PrismaUnion<_i2.ModelCreateOrConnectWithoutMarkInput,
      Iterable<_i2.ModelCreateOrConnectWithoutMarkInput>>? connectOrCreate;

  final _i2.ModelCreateManyMarkInputEnvelope? createMany;

  final _i1.PrismaUnion<_i2.ModelWhereUniqueInput,
      Iterable<_i2.ModelWhereUniqueInput>>? connect;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'createMany': createMany,
        'connect': connect,
      };
}

class MarkUncheckedCreateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkUncheckedCreateInput({
    this.id,
    required this.name,
    this.model,
  });

  final int? id;

  final String name;

  final _i2.ModelUncheckedCreateNestedManyWithoutMarkInput? model;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'Model': model,
      };
}

class AffectedRowsOutput {
  const AffectedRowsOutput({this.count});

  factory AffectedRowsOutput.fromJson(Map json) =>
      AffectedRowsOutput(count: json['count']);

  final int? count;

  Map<String, dynamic> toJson() => {'count': count};
}

class MarkCreateManyInput implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkCreateManyInput({
    this.id,
    required this.name,
  });

  final int? id;

  final String name;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
      };
}

class StringFieldUpdateOperationsInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const StringFieldUpdateOperationsInput({this.set});

  final String? set;

  @override
  Map<String, dynamic> toJson() => {'set': set};
}

class CountryUpdateWithoutModelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryUpdateWithoutModelInput({
    this.name,
    this.code,
  });

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? code;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'code': code,
      };
}

class IntFieldUpdateOperationsInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const IntFieldUpdateOperationsInput({
    this.set,
    this.increment,
    this.decrement,
    this.multiply,
    this.divide,
  });

  final int? set;

  final int? increment;

  final int? decrement;

  final int? multiply;

  final int? divide;

  @override
  Map<String, dynamic> toJson() => {
        'set': set,
        'increment': increment,
        'decrement': decrement,
        'multiply': multiply,
        'divide': divide,
      };
}

class CountryUncheckedUpdateWithoutModelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryUncheckedUpdateWithoutModelInput({
    this.id,
    this.name,
    this.code,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? code;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'code': code,
      };
}

class CountryUpsertWithoutModelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryUpsertWithoutModelInput({
    required this.update,
    required this.create,
  });

  final _i1.PrismaUnion<_i2.CountryUpdateWithoutModelInput,
      _i2.CountryUncheckedUpdateWithoutModelInput> update;

  final _i1.PrismaUnion<_i2.CountryCreateWithoutModelInput,
      _i2.CountryUncheckedCreateWithoutModelInput> create;

  @override
  Map<String, dynamic> toJson() => {
        'update': update,
        'create': create,
      };
}

class CountryUpdateOneRequiredWithoutModelNestedInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryUpdateOneRequiredWithoutModelNestedInput({
    this.create,
    this.connectOrCreate,
    this.upsert,
    this.connect,
    this.update,
  });

  final _i1.PrismaUnion<_i2.CountryCreateWithoutModelInput,
      _i2.CountryUncheckedCreateWithoutModelInput>? create;

  final _i2.CountryCreateOrConnectWithoutModelInput? connectOrCreate;

  final _i2.CountryUpsertWithoutModelInput? upsert;

  final _i2.CountryWhereUniqueInput? connect;

  final _i1.PrismaUnion<_i2.CountryUpdateWithoutModelInput,
      _i2.CountryUncheckedUpdateWithoutModelInput>? update;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'upsert': upsert,
        'connect': connect,
        'update': update,
      };
}

class FloatFieldUpdateOperationsInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FloatFieldUpdateOperationsInput({
    this.set,
    this.increment,
    this.decrement,
    this.multiply,
    this.divide,
  });

  final double? set;

  final double? increment;

  final double? decrement;

  final double? multiply;

  final double? divide;

  @override
  Map<String, dynamic> toJson() => {
        'set': set,
        'increment': increment,
        'decrement': decrement,
        'multiply': multiply,
        'divide': divide,
      };
}

class DateTimeFieldUpdateOperationsInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const DateTimeFieldUpdateOperationsInput({this.set});

  final DateTime? set;

  @override
  Map<String, dynamic> toJson() => {'set': set};
}

class EngineUpdateWithoutCarInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineUpdateWithoutCarInput({
    this.name,
    this.power,
  });

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? power;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'power': power,
      };
}

class EngineUncheckedUpdateWithoutCarInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineUncheckedUpdateWithoutCarInput({
    this.id,
    this.name,
    this.power,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? power;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'power': power,
      };
}

class EngineUpsertWithoutCarInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineUpsertWithoutCarInput({
    required this.update,
    required this.create,
  });

  final _i1.PrismaUnion<_i2.EngineUpdateWithoutCarInput,
      _i2.EngineUncheckedUpdateWithoutCarInput> update;

  final _i1.PrismaUnion<_i2.EngineCreateWithoutCarInput,
      _i2.EngineUncheckedCreateWithoutCarInput> create;

  @override
  Map<String, dynamic> toJson() => {
        'update': update,
        'create': create,
      };
}

class EngineUpdateOneRequiredWithoutCarNestedInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineUpdateOneRequiredWithoutCarNestedInput({
    this.create,
    this.connectOrCreate,
    this.upsert,
    this.connect,
    this.update,
  });

  final _i1.PrismaUnion<_i2.EngineCreateWithoutCarInput,
      _i2.EngineUncheckedCreateWithoutCarInput>? create;

  final _i2.EngineCreateOrConnectWithoutCarInput? connectOrCreate;

  final _i2.EngineUpsertWithoutCarInput? upsert;

  final _i2.EngineWhereUniqueInput? connect;

  final _i1.PrismaUnion<_i2.EngineUpdateWithoutCarInput,
      _i2.EngineUncheckedUpdateWithoutCarInput>? update;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'upsert': upsert,
        'connect': connect,
        'update': update,
      };
}

class NullableFloatFieldUpdateOperationsInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const NullableFloatFieldUpdateOperationsInput({
    this.set,
    this.increment,
    this.decrement,
    this.multiply,
    this.divide,
  });

  final _i1.PrismaUnion<double, _i1.PrismaNull>? set;

  final double? increment;

  final double? decrement;

  final double? multiply;

  final double? divide;

  @override
  Map<String, dynamic> toJson() => {
        'set': set,
        'increment': increment,
        'decrement': decrement,
        'multiply': multiply,
        'divide': divide,
      };
}

class CategoryUpdateWithoutExpensiveInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryUpdateWithoutExpensiveInput({this.name});

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  @override
  Map<String, dynamic> toJson() => {'name': name};
}

class CategoryUncheckedUpdateWithoutExpensiveInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryUncheckedUpdateWithoutExpensiveInput({
    this.id,
    this.name,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
      };
}

class CategoryUpsertWithoutExpensiveInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryUpsertWithoutExpensiveInput({
    required this.update,
    required this.create,
  });

  final _i1.PrismaUnion<_i2.CategoryUpdateWithoutExpensiveInput,
      _i2.CategoryUncheckedUpdateWithoutExpensiveInput> update;

  final _i1.PrismaUnion<_i2.CategoryCreateWithoutExpensiveInput,
      _i2.CategoryUncheckedCreateWithoutExpensiveInput> create;

  @override
  Map<String, dynamic> toJson() => {
        'update': update,
        'create': create,
      };
}

class CategoryUpdateOneRequiredWithoutExpensiveNestedInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryUpdateOneRequiredWithoutExpensiveNestedInput({
    this.create,
    this.connectOrCreate,
    this.upsert,
    this.connect,
    this.update,
  });

  final _i1.PrismaUnion<_i2.CategoryCreateWithoutExpensiveInput,
      _i2.CategoryUncheckedCreateWithoutExpensiveInput>? create;

  final _i2.CategoryCreateOrConnectWithoutExpensiveInput? connectOrCreate;

  final _i2.CategoryUpsertWithoutExpensiveInput? upsert;

  final _i2.CategoryWhereUniqueInput? connect;

  final _i1.PrismaUnion<_i2.CategoryUpdateWithoutExpensiveInput,
      _i2.CategoryUncheckedUpdateWithoutExpensiveInput>? update;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'upsert': upsert,
        'connect': connect,
        'update': update,
      };
}

class FuelUpdateWithoutExpensiveInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelUpdateWithoutExpensiveInput({this.name});

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  @override
  Map<String, dynamic> toJson() => {'name': name};
}

class FuelUncheckedUpdateWithoutExpensiveInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelUncheckedUpdateWithoutExpensiveInput({
    this.id,
    this.name,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
      };
}

class FuelUpsertWithoutExpensiveInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelUpsertWithoutExpensiveInput({
    required this.update,
    required this.create,
  });

  final _i1.PrismaUnion<_i2.FuelUpdateWithoutExpensiveInput,
      _i2.FuelUncheckedUpdateWithoutExpensiveInput> update;

  final _i1.PrismaUnion<_i2.FuelCreateWithoutExpensiveInput,
      _i2.FuelUncheckedCreateWithoutExpensiveInput> create;

  @override
  Map<String, dynamic> toJson() => {
        'update': update,
        'create': create,
      };
}

class FuelUpdateOneWithoutExpensiveNestedInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelUpdateOneWithoutExpensiveNestedInput({
    this.create,
    this.connectOrCreate,
    this.upsert,
    this.disconnect,
    this.delete,
    this.connect,
    this.update,
  });

  final _i1.PrismaUnion<_i2.FuelCreateWithoutExpensiveInput,
      _i2.FuelUncheckedCreateWithoutExpensiveInput>? create;

  final _i2.FuelCreateOrConnectWithoutExpensiveInput? connectOrCreate;

  final _i2.FuelUpsertWithoutExpensiveInput? upsert;

  final bool? disconnect;

  final bool? delete;

  final _i2.FuelWhereUniqueInput? connect;

  final _i1.PrismaUnion<_i2.FuelUpdateWithoutExpensiveInput,
      _i2.FuelUncheckedUpdateWithoutExpensiveInput>? update;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'upsert': upsert,
        'disconnect': disconnect,
        'delete': delete,
        'connect': connect,
        'update': update,
      };
}

class ExpensiveUpdateWithoutUserInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveUpdateWithoutUserInput({
    this.name,
    this.description,
    this.createdDate,
    this.range,
    this.price,
    this.liters,
    this.category,
    this.fuel,
  });

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>?
      description;

  final _i1.PrismaUnion<DateTime, _i2.DateTimeFieldUpdateOperationsInput>?
      createdDate;

  final _i1.PrismaUnion<double, _i2.FloatFieldUpdateOperationsInput>? range;

  final _i1.PrismaUnion<double, _i2.FloatFieldUpdateOperationsInput>? price;

  final _i1.PrismaUnion<
      double,
      _i1.PrismaUnion<_i2.NullableFloatFieldUpdateOperationsInput,
          _i1.PrismaNull>>? liters;

  final _i2.CategoryUpdateOneRequiredWithoutExpensiveNestedInput? category;

  final _i2.FuelUpdateOneWithoutExpensiveNestedInput? fuel;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'liters': liters,
        'category': category,
        'Fuel': fuel,
      };
}

class NullableIntFieldUpdateOperationsInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const NullableIntFieldUpdateOperationsInput({
    this.set,
    this.increment,
    this.decrement,
    this.multiply,
    this.divide,
  });

  final _i1.PrismaUnion<int, _i1.PrismaNull>? set;

  final int? increment;

  final int? decrement;

  final int? multiply;

  final int? divide;

  @override
  Map<String, dynamic> toJson() => {
        'set': set,
        'increment': increment,
        'decrement': decrement,
        'multiply': multiply,
        'divide': divide,
      };
}

class ExpensiveUncheckedUpdateWithoutUserInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveUncheckedUpdateWithoutUserInput({
    this.id,
    this.name,
    this.categoryId,
    this.description,
    this.createdDate,
    this.range,
    this.price,
    this.fuelTypeId,
    this.liters,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? categoryId;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>?
      description;

  final _i1.PrismaUnion<DateTime, _i2.DateTimeFieldUpdateOperationsInput>?
      createdDate;

  final _i1.PrismaUnion<double, _i2.FloatFieldUpdateOperationsInput>? range;

  final _i1.PrismaUnion<double, _i2.FloatFieldUpdateOperationsInput>? price;

  final _i1.PrismaUnion<
      int,
      _i1.PrismaUnion<_i2.NullableIntFieldUpdateOperationsInput,
          _i1.PrismaNull>>? fuelTypeId;

  final _i1.PrismaUnion<
      double,
      _i1.PrismaUnion<_i2.NullableFloatFieldUpdateOperationsInput,
          _i1.PrismaNull>>? liters;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'category_id': categoryId,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'fuel_type_id': fuelTypeId,
        'liters': liters,
      };
}

class ExpensiveUpsertWithWhereUniqueWithoutUserInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveUpsertWithWhereUniqueWithoutUserInput({
    required this.where,
    required this.update,
    required this.create,
  });

  final _i2.ExpensiveWhereUniqueInput where;

  final _i1.PrismaUnion<_i2.ExpensiveUpdateWithoutUserInput,
      _i2.ExpensiveUncheckedUpdateWithoutUserInput> update;

  final _i1.PrismaUnion<_i2.ExpensiveCreateWithoutUserInput,
      _i2.ExpensiveUncheckedCreateWithoutUserInput> create;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'update': update,
        'create': create,
      };
}

class ExpensiveUpdateWithWhereUniqueWithoutUserInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveUpdateWithWhereUniqueWithoutUserInput({
    required this.where,
    required this.data,
  });

  final _i2.ExpensiveWhereUniqueInput where;

  final _i1.PrismaUnion<_i2.ExpensiveUpdateWithoutUserInput,
      _i2.ExpensiveUncheckedUpdateWithoutUserInput> data;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'data': data,
      };
}

class ExpensiveScalarWhereInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveScalarWhereInput({
    this.AND,
    this.OR,
    this.NOT,
    this.id,
    this.name,
    this.categoryId,
    this.description,
    this.createdDate,
    this.range,
    this.price,
    this.userId,
    this.fuelTypeId,
    this.liters,
  });

  final _i1.PrismaUnion<_i2.ExpensiveScalarWhereInput,
      Iterable<_i2.ExpensiveScalarWhereInput>>? AND;

  final Iterable<_i2.ExpensiveScalarWhereInput>? OR;

  final _i1.PrismaUnion<_i2.ExpensiveScalarWhereInput,
      Iterable<_i2.ExpensiveScalarWhereInput>>? NOT;

  final _i1.PrismaUnion<_i2.IntFilter, int>? id;

  final _i1.PrismaUnion<_i2.StringFilter, String>? name;

  final _i1.PrismaUnion<_i2.IntFilter, int>? categoryId;

  final _i1.PrismaUnion<_i2.StringFilter, String>? description;

  final _i1.PrismaUnion<_i2.DateTimeFilter, DateTime>? createdDate;

  final _i1.PrismaUnion<_i2.FloatFilter, double>? range;

  final _i1.PrismaUnion<_i2.FloatFilter, double>? price;

  final _i1.PrismaUnion<_i2.IntFilter, int>? userId;

  final _i1
      .PrismaUnion<_i2.IntNullableFilter, _i1.PrismaUnion<int, _i1.PrismaNull>>?
      fuelTypeId;

  final _i1.PrismaUnion<_i2.FloatNullableFilter,
      _i1.PrismaUnion<double, _i1.PrismaNull>>? liters;

  @override
  Map<String, dynamic> toJson() => {
        'AND': AND,
        'OR': OR,
        'NOT': NOT,
        'id': id,
        'name': name,
        'category_id': categoryId,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'user_id': userId,
        'fuel_type_id': fuelTypeId,
        'liters': liters,
      };
}

class ExpensiveUpdateManyMutationInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveUpdateManyMutationInput({
    this.name,
    this.description,
    this.createdDate,
    this.range,
    this.price,
    this.liters,
  });

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>?
      description;

  final _i1.PrismaUnion<DateTime, _i2.DateTimeFieldUpdateOperationsInput>?
      createdDate;

  final _i1.PrismaUnion<double, _i2.FloatFieldUpdateOperationsInput>? range;

  final _i1.PrismaUnion<double, _i2.FloatFieldUpdateOperationsInput>? price;

  final _i1.PrismaUnion<
      double,
      _i1.PrismaUnion<_i2.NullableFloatFieldUpdateOperationsInput,
          _i1.PrismaNull>>? liters;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'liters': liters,
      };
}

class ExpensiveUncheckedUpdateManyWithoutExpensiveInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveUncheckedUpdateManyWithoutExpensiveInput({
    this.id,
    this.name,
    this.categoryId,
    this.description,
    this.createdDate,
    this.range,
    this.price,
    this.fuelTypeId,
    this.liters,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? categoryId;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>?
      description;

  final _i1.PrismaUnion<DateTime, _i2.DateTimeFieldUpdateOperationsInput>?
      createdDate;

  final _i1.PrismaUnion<double, _i2.FloatFieldUpdateOperationsInput>? range;

  final _i1.PrismaUnion<double, _i2.FloatFieldUpdateOperationsInput>? price;

  final _i1.PrismaUnion<
      int,
      _i1.PrismaUnion<_i2.NullableIntFieldUpdateOperationsInput,
          _i1.PrismaNull>>? fuelTypeId;

  final _i1.PrismaUnion<
      double,
      _i1.PrismaUnion<_i2.NullableFloatFieldUpdateOperationsInput,
          _i1.PrismaNull>>? liters;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'category_id': categoryId,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'fuel_type_id': fuelTypeId,
        'liters': liters,
      };
}

class ExpensiveUpdateManyWithWhereWithoutUserInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveUpdateManyWithWhereWithoutUserInput({
    required this.where,
    required this.data,
  });

  final _i2.ExpensiveScalarWhereInput where;

  final _i1.PrismaUnion<_i2.ExpensiveUpdateManyMutationInput,
      _i2.ExpensiveUncheckedUpdateManyWithoutExpensiveInput> data;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'data': data,
      };
}

class ExpensiveUpdateManyWithoutUserNestedInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveUpdateManyWithoutUserNestedInput({
    this.create,
    this.connectOrCreate,
    this.upsert,
    this.createMany,
    this.set,
    this.disconnect,
    this.delete,
    this.connect,
    this.update,
    this.updateMany,
    this.deleteMany,
  });

  final _i1.PrismaUnion<
      _i2.ExpensiveCreateWithoutUserInput,
      _i1.PrismaUnion<
          Iterable<_i2.ExpensiveCreateWithoutUserInput>,
          _i1.PrismaUnion<_i2.ExpensiveUncheckedCreateWithoutUserInput,
              Iterable<_i2.ExpensiveUncheckedCreateWithoutUserInput>>>>? create;

  final _i1.PrismaUnion<_i2.ExpensiveCreateOrConnectWithoutUserInput,
      Iterable<_i2.ExpensiveCreateOrConnectWithoutUserInput>>? connectOrCreate;

  final _i1.PrismaUnion<_i2.ExpensiveUpsertWithWhereUniqueWithoutUserInput,
      Iterable<_i2.ExpensiveUpsertWithWhereUniqueWithoutUserInput>>? upsert;

  final _i2.ExpensiveCreateManyUserInputEnvelope? createMany;

  final _i1.PrismaUnion<_i2.ExpensiveWhereUniqueInput,
      Iterable<_i2.ExpensiveWhereUniqueInput>>? set;

  final _i1.PrismaUnion<_i2.ExpensiveWhereUniqueInput,
      Iterable<_i2.ExpensiveWhereUniqueInput>>? disconnect;

  final _i1.PrismaUnion<_i2.ExpensiveWhereUniqueInput,
      Iterable<_i2.ExpensiveWhereUniqueInput>>? delete;

  final _i1.PrismaUnion<_i2.ExpensiveWhereUniqueInput,
      Iterable<_i2.ExpensiveWhereUniqueInput>>? connect;

  final _i1.PrismaUnion<_i2.ExpensiveUpdateWithWhereUniqueWithoutUserInput,
      Iterable<_i2.ExpensiveUpdateWithWhereUniqueWithoutUserInput>>? update;

  final _i1.PrismaUnion<_i2.ExpensiveUpdateManyWithWhereWithoutUserInput,
      Iterable<_i2.ExpensiveUpdateManyWithWhereWithoutUserInput>>? updateMany;

  final _i1.PrismaUnion<_i2.ExpensiveScalarWhereInput,
      Iterable<_i2.ExpensiveScalarWhereInput>>? deleteMany;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'upsert': upsert,
        'createMany': createMany,
        'set': set,
        'disconnect': disconnect,
        'delete': delete,
        'connect': connect,
        'update': update,
        'updateMany': updateMany,
        'deleteMany': deleteMany,
      };
}

class UserUpdateWithoutCarInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserUpdateWithoutCarInput({
    this.name,
    this.phone,
    this.birthday,
    this.expensive,
  });

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? phone;

  final _i1.PrismaUnion<DateTime, _i2.DateTimeFieldUpdateOperationsInput>?
      birthday;

  final _i2.ExpensiveUpdateManyWithoutUserNestedInput? expensive;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'phone': phone,
        'birthday': birthday,
        'Expensive': expensive,
      };
}

class ExpensiveUncheckedUpdateManyWithoutUserNestedInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveUncheckedUpdateManyWithoutUserNestedInput({
    this.create,
    this.connectOrCreate,
    this.upsert,
    this.createMany,
    this.set,
    this.disconnect,
    this.delete,
    this.connect,
    this.update,
    this.updateMany,
    this.deleteMany,
  });

  final _i1.PrismaUnion<
      _i2.ExpensiveCreateWithoutUserInput,
      _i1.PrismaUnion<
          Iterable<_i2.ExpensiveCreateWithoutUserInput>,
          _i1.PrismaUnion<_i2.ExpensiveUncheckedCreateWithoutUserInput,
              Iterable<_i2.ExpensiveUncheckedCreateWithoutUserInput>>>>? create;

  final _i1.PrismaUnion<_i2.ExpensiveCreateOrConnectWithoutUserInput,
      Iterable<_i2.ExpensiveCreateOrConnectWithoutUserInput>>? connectOrCreate;

  final _i1.PrismaUnion<_i2.ExpensiveUpsertWithWhereUniqueWithoutUserInput,
      Iterable<_i2.ExpensiveUpsertWithWhereUniqueWithoutUserInput>>? upsert;

  final _i2.ExpensiveCreateManyUserInputEnvelope? createMany;

  final _i1.PrismaUnion<_i2.ExpensiveWhereUniqueInput,
      Iterable<_i2.ExpensiveWhereUniqueInput>>? set;

  final _i1.PrismaUnion<_i2.ExpensiveWhereUniqueInput,
      Iterable<_i2.ExpensiveWhereUniqueInput>>? disconnect;

  final _i1.PrismaUnion<_i2.ExpensiveWhereUniqueInput,
      Iterable<_i2.ExpensiveWhereUniqueInput>>? delete;

  final _i1.PrismaUnion<_i2.ExpensiveWhereUniqueInput,
      Iterable<_i2.ExpensiveWhereUniqueInput>>? connect;

  final _i1.PrismaUnion<_i2.ExpensiveUpdateWithWhereUniqueWithoutUserInput,
      Iterable<_i2.ExpensiveUpdateWithWhereUniqueWithoutUserInput>>? update;

  final _i1.PrismaUnion<_i2.ExpensiveUpdateManyWithWhereWithoutUserInput,
      Iterable<_i2.ExpensiveUpdateManyWithWhereWithoutUserInput>>? updateMany;

  final _i1.PrismaUnion<_i2.ExpensiveScalarWhereInput,
      Iterable<_i2.ExpensiveScalarWhereInput>>? deleteMany;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'upsert': upsert,
        'createMany': createMany,
        'set': set,
        'disconnect': disconnect,
        'delete': delete,
        'connect': connect,
        'update': update,
        'updateMany': updateMany,
        'deleteMany': deleteMany,
      };
}

class UserUncheckedUpdateWithoutCarInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserUncheckedUpdateWithoutCarInput({
    this.id,
    this.name,
    this.phone,
    this.birthday,
    this.expensive,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? phone;

  final _i1.PrismaUnion<DateTime, _i2.DateTimeFieldUpdateOperationsInput>?
      birthday;

  final _i2.ExpensiveUncheckedUpdateManyWithoutUserNestedInput? expensive;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'phone': phone,
        'birthday': birthday,
        'Expensive': expensive,
      };
}

class UserUpsertWithWhereUniqueWithoutCarInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserUpsertWithWhereUniqueWithoutCarInput({
    required this.where,
    required this.update,
    required this.create,
  });

  final _i2.UserWhereUniqueInput where;

  final _i1.PrismaUnion<_i2.UserUpdateWithoutCarInput,
      _i2.UserUncheckedUpdateWithoutCarInput> update;

  final _i1.PrismaUnion<_i2.UserCreateWithoutCarInput,
      _i2.UserUncheckedCreateWithoutCarInput> create;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'update': update,
        'create': create,
      };
}

class UserUpdateWithWhereUniqueWithoutCarInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserUpdateWithWhereUniqueWithoutCarInput({
    required this.where,
    required this.data,
  });

  final _i2.UserWhereUniqueInput where;

  final _i1.PrismaUnion<_i2.UserUpdateWithoutCarInput,
      _i2.UserUncheckedUpdateWithoutCarInput> data;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'data': data,
      };
}

class UserScalarWhereInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserScalarWhereInput({
    this.AND,
    this.OR,
    this.NOT,
    this.id,
    this.name,
    this.phone,
    this.birthday,
    this.carId,
  });

  final _i1.PrismaUnion<_i2.UserScalarWhereInput,
      Iterable<_i2.UserScalarWhereInput>>? AND;

  final Iterable<_i2.UserScalarWhereInput>? OR;

  final _i1.PrismaUnion<_i2.UserScalarWhereInput,
      Iterable<_i2.UserScalarWhereInput>>? NOT;

  final _i1.PrismaUnion<_i2.IntFilter, int>? id;

  final _i1.PrismaUnion<_i2.StringFilter, String>? name;

  final _i1.PrismaUnion<_i2.StringFilter, String>? phone;

  final _i1.PrismaUnion<_i2.DateTimeFilter, DateTime>? birthday;

  final _i1.PrismaUnion<_i2.IntFilter, int>? carId;

  @override
  Map<String, dynamic> toJson() => {
        'AND': AND,
        'OR': OR,
        'NOT': NOT,
        'id': id,
        'name': name,
        'phone': phone,
        'birthday': birthday,
        'car_id': carId,
      };
}

class UserUpdateManyMutationInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserUpdateManyMutationInput({
    this.name,
    this.phone,
    this.birthday,
  });

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? phone;

  final _i1.PrismaUnion<DateTime, _i2.DateTimeFieldUpdateOperationsInput>?
      birthday;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'phone': phone,
        'birthday': birthday,
      };
}

class UserUncheckedUpdateManyWithoutUserInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserUncheckedUpdateManyWithoutUserInput({
    this.id,
    this.name,
    this.phone,
    this.birthday,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? phone;

  final _i1.PrismaUnion<DateTime, _i2.DateTimeFieldUpdateOperationsInput>?
      birthday;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'phone': phone,
        'birthday': birthday,
      };
}

class UserUpdateManyWithWhereWithoutCarInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserUpdateManyWithWhereWithoutCarInput({
    required this.where,
    required this.data,
  });

  final _i2.UserScalarWhereInput where;

  final _i1.PrismaUnion<_i2.UserUpdateManyMutationInput,
      _i2.UserUncheckedUpdateManyWithoutUserInput> data;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'data': data,
      };
}

class UserUpdateManyWithoutCarNestedInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserUpdateManyWithoutCarNestedInput({
    this.create,
    this.connectOrCreate,
    this.upsert,
    this.createMany,
    this.set,
    this.disconnect,
    this.delete,
    this.connect,
    this.update,
    this.updateMany,
    this.deleteMany,
  });

  final _i1.PrismaUnion<
      _i2.UserCreateWithoutCarInput,
      _i1.PrismaUnion<
          Iterable<_i2.UserCreateWithoutCarInput>,
          _i1.PrismaUnion<_i2.UserUncheckedCreateWithoutCarInput,
              Iterable<_i2.UserUncheckedCreateWithoutCarInput>>>>? create;

  final _i1.PrismaUnion<_i2.UserCreateOrConnectWithoutCarInput,
      Iterable<_i2.UserCreateOrConnectWithoutCarInput>>? connectOrCreate;

  final _i1.PrismaUnion<_i2.UserUpsertWithWhereUniqueWithoutCarInput,
      Iterable<_i2.UserUpsertWithWhereUniqueWithoutCarInput>>? upsert;

  final _i2.UserCreateManyCarInputEnvelope? createMany;

  final _i1.PrismaUnion<_i2.UserWhereUniqueInput,
      Iterable<_i2.UserWhereUniqueInput>>? set;

  final _i1.PrismaUnion<_i2.UserWhereUniqueInput,
      Iterable<_i2.UserWhereUniqueInput>>? disconnect;

  final _i1.PrismaUnion<_i2.UserWhereUniqueInput,
      Iterable<_i2.UserWhereUniqueInput>>? delete;

  final _i1.PrismaUnion<_i2.UserWhereUniqueInput,
      Iterable<_i2.UserWhereUniqueInput>>? connect;

  final _i1.PrismaUnion<_i2.UserUpdateWithWhereUniqueWithoutCarInput,
      Iterable<_i2.UserUpdateWithWhereUniqueWithoutCarInput>>? update;

  final _i1.PrismaUnion<_i2.UserUpdateManyWithWhereWithoutCarInput,
      Iterable<_i2.UserUpdateManyWithWhereWithoutCarInput>>? updateMany;

  final _i1.PrismaUnion<_i2.UserScalarWhereInput,
      Iterable<_i2.UserScalarWhereInput>>? deleteMany;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'upsert': upsert,
        'createMany': createMany,
        'set': set,
        'disconnect': disconnect,
        'delete': delete,
        'connect': connect,
        'update': update,
        'updateMany': updateMany,
        'deleteMany': deleteMany,
      };
}

class CarUpdateWithoutModelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarUpdateWithoutModelInput({
    this.vin,
    this.range,
    this.createdDate,
    this.voluem,
    this.engine,
    this.user,
  });

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? vin;

  final _i1.PrismaUnion<double, _i2.FloatFieldUpdateOperationsInput>? range;

  final _i1.PrismaUnion<DateTime, _i2.DateTimeFieldUpdateOperationsInput>?
      createdDate;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? voluem;

  final _i2.EngineUpdateOneRequiredWithoutCarNestedInput? engine;

  final _i2.UserUpdateManyWithoutCarNestedInput? user;

  @override
  Map<String, dynamic> toJson() => {
        'vin': vin,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'engine': engine,
        'User': user,
      };
}

class UserUncheckedUpdateManyWithoutCarNestedInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserUncheckedUpdateManyWithoutCarNestedInput({
    this.create,
    this.connectOrCreate,
    this.upsert,
    this.createMany,
    this.set,
    this.disconnect,
    this.delete,
    this.connect,
    this.update,
    this.updateMany,
    this.deleteMany,
  });

  final _i1.PrismaUnion<
      _i2.UserCreateWithoutCarInput,
      _i1.PrismaUnion<
          Iterable<_i2.UserCreateWithoutCarInput>,
          _i1.PrismaUnion<_i2.UserUncheckedCreateWithoutCarInput,
              Iterable<_i2.UserUncheckedCreateWithoutCarInput>>>>? create;

  final _i1.PrismaUnion<_i2.UserCreateOrConnectWithoutCarInput,
      Iterable<_i2.UserCreateOrConnectWithoutCarInput>>? connectOrCreate;

  final _i1.PrismaUnion<_i2.UserUpsertWithWhereUniqueWithoutCarInput,
      Iterable<_i2.UserUpsertWithWhereUniqueWithoutCarInput>>? upsert;

  final _i2.UserCreateManyCarInputEnvelope? createMany;

  final _i1.PrismaUnion<_i2.UserWhereUniqueInput,
      Iterable<_i2.UserWhereUniqueInput>>? set;

  final _i1.PrismaUnion<_i2.UserWhereUniqueInput,
      Iterable<_i2.UserWhereUniqueInput>>? disconnect;

  final _i1.PrismaUnion<_i2.UserWhereUniqueInput,
      Iterable<_i2.UserWhereUniqueInput>>? delete;

  final _i1.PrismaUnion<_i2.UserWhereUniqueInput,
      Iterable<_i2.UserWhereUniqueInput>>? connect;

  final _i1.PrismaUnion<_i2.UserUpdateWithWhereUniqueWithoutCarInput,
      Iterable<_i2.UserUpdateWithWhereUniqueWithoutCarInput>>? update;

  final _i1.PrismaUnion<_i2.UserUpdateManyWithWhereWithoutCarInput,
      Iterable<_i2.UserUpdateManyWithWhereWithoutCarInput>>? updateMany;

  final _i1.PrismaUnion<_i2.UserScalarWhereInput,
      Iterable<_i2.UserScalarWhereInput>>? deleteMany;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'upsert': upsert,
        'createMany': createMany,
        'set': set,
        'disconnect': disconnect,
        'delete': delete,
        'connect': connect,
        'update': update,
        'updateMany': updateMany,
        'deleteMany': deleteMany,
      };
}

class CarUncheckedUpdateWithoutModelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarUncheckedUpdateWithoutModelInput({
    this.id,
    this.vin,
    this.range,
    this.createdDate,
    this.voluem,
    this.engineId,
    this.user,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? vin;

  final _i1.PrismaUnion<double, _i2.FloatFieldUpdateOperationsInput>? range;

  final _i1.PrismaUnion<DateTime, _i2.DateTimeFieldUpdateOperationsInput>?
      createdDate;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? voluem;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? engineId;

  final _i2.UserUncheckedUpdateManyWithoutCarNestedInput? user;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'vin': vin,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'engine_id': engineId,
        'User': user,
      };
}

class CarUpsertWithWhereUniqueWithoutModelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarUpsertWithWhereUniqueWithoutModelInput({
    required this.where,
    required this.update,
    required this.create,
  });

  final _i2.CarWhereUniqueInput where;

  final _i1.PrismaUnion<_i2.CarUpdateWithoutModelInput,
      _i2.CarUncheckedUpdateWithoutModelInput> update;

  final _i1.PrismaUnion<_i2.CarCreateWithoutModelInput,
      _i2.CarUncheckedCreateWithoutModelInput> create;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'update': update,
        'create': create,
      };
}

class CarUpdateWithWhereUniqueWithoutModelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarUpdateWithWhereUniqueWithoutModelInput({
    required this.where,
    required this.data,
  });

  final _i2.CarWhereUniqueInput where;

  final _i1.PrismaUnion<_i2.CarUpdateWithoutModelInput,
      _i2.CarUncheckedUpdateWithoutModelInput> data;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'data': data,
      };
}

class CarScalarWhereInput implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarScalarWhereInput({
    this.AND,
    this.OR,
    this.NOT,
    this.id,
    this.vin,
    this.modelId,
    this.range,
    this.createdDate,
    this.voluem,
    this.engineId,
  });

  final _i1
      .PrismaUnion<_i2.CarScalarWhereInput, Iterable<_i2.CarScalarWhereInput>>?
      AND;

  final Iterable<_i2.CarScalarWhereInput>? OR;

  final _i1
      .PrismaUnion<_i2.CarScalarWhereInput, Iterable<_i2.CarScalarWhereInput>>?
      NOT;

  final _i1.PrismaUnion<_i2.IntFilter, int>? id;

  final _i1.PrismaUnion<_i2.StringFilter, String>? vin;

  final _i1.PrismaUnion<_i2.IntFilter, int>? modelId;

  final _i1.PrismaUnion<_i2.FloatFilter, double>? range;

  final _i1.PrismaUnion<_i2.DateTimeFilter, DateTime>? createdDate;

  final _i1.PrismaUnion<_i2.IntFilter, int>? voluem;

  final _i1.PrismaUnion<_i2.IntFilter, int>? engineId;

  @override
  Map<String, dynamic> toJson() => {
        'AND': AND,
        'OR': OR,
        'NOT': NOT,
        'id': id,
        'vin': vin,
        'model_id': modelId,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'engine_id': engineId,
      };
}

class CarUpdateManyMutationInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarUpdateManyMutationInput({
    this.vin,
    this.range,
    this.createdDate,
    this.voluem,
  });

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? vin;

  final _i1.PrismaUnion<double, _i2.FloatFieldUpdateOperationsInput>? range;

  final _i1.PrismaUnion<DateTime, _i2.DateTimeFieldUpdateOperationsInput>?
      createdDate;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? voluem;

  @override
  Map<String, dynamic> toJson() => {
        'vin': vin,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
      };
}

class CarUncheckedUpdateManyWithoutCarInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarUncheckedUpdateManyWithoutCarInput({
    this.id,
    this.vin,
    this.range,
    this.createdDate,
    this.voluem,
    this.engineId,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? vin;

  final _i1.PrismaUnion<double, _i2.FloatFieldUpdateOperationsInput>? range;

  final _i1.PrismaUnion<DateTime, _i2.DateTimeFieldUpdateOperationsInput>?
      createdDate;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? voluem;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? engineId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'vin': vin,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'engine_id': engineId,
      };
}

class CarUpdateManyWithWhereWithoutModelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarUpdateManyWithWhereWithoutModelInput({
    required this.where,
    required this.data,
  });

  final _i2.CarScalarWhereInput where;

  final _i1.PrismaUnion<_i2.CarUpdateManyMutationInput,
      _i2.CarUncheckedUpdateManyWithoutCarInput> data;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'data': data,
      };
}

class CarUpdateManyWithoutModelNestedInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarUpdateManyWithoutModelNestedInput({
    this.create,
    this.connectOrCreate,
    this.upsert,
    this.createMany,
    this.set,
    this.disconnect,
    this.delete,
    this.connect,
    this.update,
    this.updateMany,
    this.deleteMany,
  });

  final _i1.PrismaUnion<
      _i2.CarCreateWithoutModelInput,
      _i1.PrismaUnion<
          Iterable<_i2.CarCreateWithoutModelInput>,
          _i1.PrismaUnion<_i2.CarUncheckedCreateWithoutModelInput,
              Iterable<_i2.CarUncheckedCreateWithoutModelInput>>>>? create;

  final _i1.PrismaUnion<_i2.CarCreateOrConnectWithoutModelInput,
      Iterable<_i2.CarCreateOrConnectWithoutModelInput>>? connectOrCreate;

  final _i1.PrismaUnion<_i2.CarUpsertWithWhereUniqueWithoutModelInput,
      Iterable<_i2.CarUpsertWithWhereUniqueWithoutModelInput>>? upsert;

  final _i2.CarCreateManyModelInputEnvelope? createMany;

  final _i1
      .PrismaUnion<_i2.CarWhereUniqueInput, Iterable<_i2.CarWhereUniqueInput>>?
      set;

  final _i1
      .PrismaUnion<_i2.CarWhereUniqueInput, Iterable<_i2.CarWhereUniqueInput>>?
      disconnect;

  final _i1
      .PrismaUnion<_i2.CarWhereUniqueInput, Iterable<_i2.CarWhereUniqueInput>>?
      delete;

  final _i1
      .PrismaUnion<_i2.CarWhereUniqueInput, Iterable<_i2.CarWhereUniqueInput>>?
      connect;

  final _i1.PrismaUnion<_i2.CarUpdateWithWhereUniqueWithoutModelInput,
      Iterable<_i2.CarUpdateWithWhereUniqueWithoutModelInput>>? update;

  final _i1.PrismaUnion<_i2.CarUpdateManyWithWhereWithoutModelInput,
      Iterable<_i2.CarUpdateManyWithWhereWithoutModelInput>>? updateMany;

  final _i1
      .PrismaUnion<_i2.CarScalarWhereInput, Iterable<_i2.CarScalarWhereInput>>?
      deleteMany;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'upsert': upsert,
        'createMany': createMany,
        'set': set,
        'disconnect': disconnect,
        'delete': delete,
        'connect': connect,
        'update': update,
        'updateMany': updateMany,
        'deleteMany': deleteMany,
      };
}

class ModelUpdateWithoutMarkInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelUpdateWithoutMarkInput({
    this.name,
    this.country,
    this.car,
  });

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i2.CountryUpdateOneRequiredWithoutModelNestedInput? country;

  final _i2.CarUpdateManyWithoutModelNestedInput? car;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'country': country,
        'Car': car,
      };
}

class CarUncheckedUpdateManyWithoutModelNestedInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarUncheckedUpdateManyWithoutModelNestedInput({
    this.create,
    this.connectOrCreate,
    this.upsert,
    this.createMany,
    this.set,
    this.disconnect,
    this.delete,
    this.connect,
    this.update,
    this.updateMany,
    this.deleteMany,
  });

  final _i1.PrismaUnion<
      _i2.CarCreateWithoutModelInput,
      _i1.PrismaUnion<
          Iterable<_i2.CarCreateWithoutModelInput>,
          _i1.PrismaUnion<_i2.CarUncheckedCreateWithoutModelInput,
              Iterable<_i2.CarUncheckedCreateWithoutModelInput>>>>? create;

  final _i1.PrismaUnion<_i2.CarCreateOrConnectWithoutModelInput,
      Iterable<_i2.CarCreateOrConnectWithoutModelInput>>? connectOrCreate;

  final _i1.PrismaUnion<_i2.CarUpsertWithWhereUniqueWithoutModelInput,
      Iterable<_i2.CarUpsertWithWhereUniqueWithoutModelInput>>? upsert;

  final _i2.CarCreateManyModelInputEnvelope? createMany;

  final _i1
      .PrismaUnion<_i2.CarWhereUniqueInput, Iterable<_i2.CarWhereUniqueInput>>?
      set;

  final _i1
      .PrismaUnion<_i2.CarWhereUniqueInput, Iterable<_i2.CarWhereUniqueInput>>?
      disconnect;

  final _i1
      .PrismaUnion<_i2.CarWhereUniqueInput, Iterable<_i2.CarWhereUniqueInput>>?
      delete;

  final _i1
      .PrismaUnion<_i2.CarWhereUniqueInput, Iterable<_i2.CarWhereUniqueInput>>?
      connect;

  final _i1.PrismaUnion<_i2.CarUpdateWithWhereUniqueWithoutModelInput,
      Iterable<_i2.CarUpdateWithWhereUniqueWithoutModelInput>>? update;

  final _i1.PrismaUnion<_i2.CarUpdateManyWithWhereWithoutModelInput,
      Iterable<_i2.CarUpdateManyWithWhereWithoutModelInput>>? updateMany;

  final _i1
      .PrismaUnion<_i2.CarScalarWhereInput, Iterable<_i2.CarScalarWhereInput>>?
      deleteMany;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'upsert': upsert,
        'createMany': createMany,
        'set': set,
        'disconnect': disconnect,
        'delete': delete,
        'connect': connect,
        'update': update,
        'updateMany': updateMany,
        'deleteMany': deleteMany,
      };
}

class ModelUncheckedUpdateWithoutMarkInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelUncheckedUpdateWithoutMarkInput({
    this.id,
    this.name,
    this.countryId,
    this.car,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? countryId;

  final _i2.CarUncheckedUpdateManyWithoutModelNestedInput? car;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'country_id': countryId,
        'Car': car,
      };
}

class ModelUpsertWithWhereUniqueWithoutMarkInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelUpsertWithWhereUniqueWithoutMarkInput({
    required this.where,
    required this.update,
    required this.create,
  });

  final _i2.ModelWhereUniqueInput where;

  final _i1.PrismaUnion<_i2.ModelUpdateWithoutMarkInput,
      _i2.ModelUncheckedUpdateWithoutMarkInput> update;

  final _i1.PrismaUnion<_i2.ModelCreateWithoutMarkInput,
      _i2.ModelUncheckedCreateWithoutMarkInput> create;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'update': update,
        'create': create,
      };
}

class ModelUpdateWithWhereUniqueWithoutMarkInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelUpdateWithWhereUniqueWithoutMarkInput({
    required this.where,
    required this.data,
  });

  final _i2.ModelWhereUniqueInput where;

  final _i1.PrismaUnion<_i2.ModelUpdateWithoutMarkInput,
      _i2.ModelUncheckedUpdateWithoutMarkInput> data;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'data': data,
      };
}

class ModelScalarWhereInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelScalarWhereInput({
    this.AND,
    this.OR,
    this.NOT,
    this.id,
    this.name,
    this.markId,
    this.countryId,
  });

  final _i1.PrismaUnion<_i2.ModelScalarWhereInput,
      Iterable<_i2.ModelScalarWhereInput>>? AND;

  final Iterable<_i2.ModelScalarWhereInput>? OR;

  final _i1.PrismaUnion<_i2.ModelScalarWhereInput,
      Iterable<_i2.ModelScalarWhereInput>>? NOT;

  final _i1.PrismaUnion<_i2.IntFilter, int>? id;

  final _i1.PrismaUnion<_i2.StringFilter, String>? name;

  final _i1.PrismaUnion<_i2.IntFilter, int>? markId;

  final _i1.PrismaUnion<_i2.IntFilter, int>? countryId;

  @override
  Map<String, dynamic> toJson() => {
        'AND': AND,
        'OR': OR,
        'NOT': NOT,
        'id': id,
        'name': name,
        'mark_id': markId,
        'country_id': countryId,
      };
}

class ModelUpdateManyMutationInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelUpdateManyMutationInput({this.name});

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  @override
  Map<String, dynamic> toJson() => {'name': name};
}

class ModelUncheckedUpdateManyWithoutModelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelUncheckedUpdateManyWithoutModelInput({
    this.id,
    this.name,
    this.countryId,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? countryId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'country_id': countryId,
      };
}

class ModelUpdateManyWithWhereWithoutMarkInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelUpdateManyWithWhereWithoutMarkInput({
    required this.where,
    required this.data,
  });

  final _i2.ModelScalarWhereInput where;

  final _i1.PrismaUnion<_i2.ModelUpdateManyMutationInput,
      _i2.ModelUncheckedUpdateManyWithoutModelInput> data;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'data': data,
      };
}

class ModelUpdateManyWithoutMarkNestedInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelUpdateManyWithoutMarkNestedInput({
    this.create,
    this.connectOrCreate,
    this.upsert,
    this.createMany,
    this.set,
    this.disconnect,
    this.delete,
    this.connect,
    this.update,
    this.updateMany,
    this.deleteMany,
  });

  final _i1.PrismaUnion<
      _i2.ModelCreateWithoutMarkInput,
      _i1.PrismaUnion<
          Iterable<_i2.ModelCreateWithoutMarkInput>,
          _i1.PrismaUnion<_i2.ModelUncheckedCreateWithoutMarkInput,
              Iterable<_i2.ModelUncheckedCreateWithoutMarkInput>>>>? create;

  final _i1.PrismaUnion<_i2.ModelCreateOrConnectWithoutMarkInput,
      Iterable<_i2.ModelCreateOrConnectWithoutMarkInput>>? connectOrCreate;

  final _i1.PrismaUnion<_i2.ModelUpsertWithWhereUniqueWithoutMarkInput,
      Iterable<_i2.ModelUpsertWithWhereUniqueWithoutMarkInput>>? upsert;

  final _i2.ModelCreateManyMarkInputEnvelope? createMany;

  final _i1.PrismaUnion<_i2.ModelWhereUniqueInput,
      Iterable<_i2.ModelWhereUniqueInput>>? set;

  final _i1.PrismaUnion<_i2.ModelWhereUniqueInput,
      Iterable<_i2.ModelWhereUniqueInput>>? disconnect;

  final _i1.PrismaUnion<_i2.ModelWhereUniqueInput,
      Iterable<_i2.ModelWhereUniqueInput>>? delete;

  final _i1.PrismaUnion<_i2.ModelWhereUniqueInput,
      Iterable<_i2.ModelWhereUniqueInput>>? connect;

  final _i1.PrismaUnion<_i2.ModelUpdateWithWhereUniqueWithoutMarkInput,
      Iterable<_i2.ModelUpdateWithWhereUniqueWithoutMarkInput>>? update;

  final _i1.PrismaUnion<_i2.ModelUpdateManyWithWhereWithoutMarkInput,
      Iterable<_i2.ModelUpdateManyWithWhereWithoutMarkInput>>? updateMany;

  final _i1.PrismaUnion<_i2.ModelScalarWhereInput,
      Iterable<_i2.ModelScalarWhereInput>>? deleteMany;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'upsert': upsert,
        'createMany': createMany,
        'set': set,
        'disconnect': disconnect,
        'delete': delete,
        'connect': connect,
        'update': update,
        'updateMany': updateMany,
        'deleteMany': deleteMany,
      };
}

class MarkUpdateInput implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkUpdateInput({
    this.name,
    this.model,
  });

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i2.ModelUpdateManyWithoutMarkNestedInput? model;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'Model': model,
      };
}

class ModelUncheckedUpdateManyWithoutMarkNestedInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelUncheckedUpdateManyWithoutMarkNestedInput({
    this.create,
    this.connectOrCreate,
    this.upsert,
    this.createMany,
    this.set,
    this.disconnect,
    this.delete,
    this.connect,
    this.update,
    this.updateMany,
    this.deleteMany,
  });

  final _i1.PrismaUnion<
      _i2.ModelCreateWithoutMarkInput,
      _i1.PrismaUnion<
          Iterable<_i2.ModelCreateWithoutMarkInput>,
          _i1.PrismaUnion<_i2.ModelUncheckedCreateWithoutMarkInput,
              Iterable<_i2.ModelUncheckedCreateWithoutMarkInput>>>>? create;

  final _i1.PrismaUnion<_i2.ModelCreateOrConnectWithoutMarkInput,
      Iterable<_i2.ModelCreateOrConnectWithoutMarkInput>>? connectOrCreate;

  final _i1.PrismaUnion<_i2.ModelUpsertWithWhereUniqueWithoutMarkInput,
      Iterable<_i2.ModelUpsertWithWhereUniqueWithoutMarkInput>>? upsert;

  final _i2.ModelCreateManyMarkInputEnvelope? createMany;

  final _i1.PrismaUnion<_i2.ModelWhereUniqueInput,
      Iterable<_i2.ModelWhereUniqueInput>>? set;

  final _i1.PrismaUnion<_i2.ModelWhereUniqueInput,
      Iterable<_i2.ModelWhereUniqueInput>>? disconnect;

  final _i1.PrismaUnion<_i2.ModelWhereUniqueInput,
      Iterable<_i2.ModelWhereUniqueInput>>? delete;

  final _i1.PrismaUnion<_i2.ModelWhereUniqueInput,
      Iterable<_i2.ModelWhereUniqueInput>>? connect;

  final _i1.PrismaUnion<_i2.ModelUpdateWithWhereUniqueWithoutMarkInput,
      Iterable<_i2.ModelUpdateWithWhereUniqueWithoutMarkInput>>? update;

  final _i1.PrismaUnion<_i2.ModelUpdateManyWithWhereWithoutMarkInput,
      Iterable<_i2.ModelUpdateManyWithWhereWithoutMarkInput>>? updateMany;

  final _i1.PrismaUnion<_i2.ModelScalarWhereInput,
      Iterable<_i2.ModelScalarWhereInput>>? deleteMany;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'upsert': upsert,
        'createMany': createMany,
        'set': set,
        'disconnect': disconnect,
        'delete': delete,
        'connect': connect,
        'update': update,
        'updateMany': updateMany,
        'deleteMany': deleteMany,
      };
}

class MarkUncheckedUpdateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkUncheckedUpdateInput({
    this.id,
    this.name,
    this.model,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i2.ModelUncheckedUpdateManyWithoutMarkNestedInput? model;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'Model': model,
      };
}

class MarkUpdateManyMutationInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkUpdateManyMutationInput({this.name});

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  @override
  Map<String, dynamic> toJson() => {'name': name};
}

class MarkUncheckedUpdateManyInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkUncheckedUpdateManyInput({
    this.id,
    this.name,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
      };
}

class MarkCountAggregateOutputType {
  const MarkCountAggregateOutputType({
    this.id,
    this.name,
    this.$all,
  });

  factory MarkCountAggregateOutputType.fromJson(Map json) =>
      MarkCountAggregateOutputType(
        id: json['id'],
        name: json['name'],
        $all: json['_all'],
      );

  final int? id;

  final int? name;

  final int? $all;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        '_all': $all,
      };
}

class MarkAvgAggregateOutputType {
  const MarkAvgAggregateOutputType({this.id});

  factory MarkAvgAggregateOutputType.fromJson(Map json) =>
      MarkAvgAggregateOutputType(id: json['id']);

  final double? id;

  Map<String, dynamic> toJson() => {'id': id};
}

class MarkSumAggregateOutputType {
  const MarkSumAggregateOutputType({this.id});

  factory MarkSumAggregateOutputType.fromJson(Map json) =>
      MarkSumAggregateOutputType(id: json['id']);

  final int? id;

  Map<String, dynamic> toJson() => {'id': id};
}

class MarkMinAggregateOutputType {
  const MarkMinAggregateOutputType({
    this.id,
    this.name,
  });

  factory MarkMinAggregateOutputType.fromJson(Map json) =>
      MarkMinAggregateOutputType(
        id: json['id'],
        name: json['name'],
      );

  final int? id;

  final String? name;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
      };
}

class MarkMaxAggregateOutputType {
  const MarkMaxAggregateOutputType({
    this.id,
    this.name,
  });

  factory MarkMaxAggregateOutputType.fromJson(Map json) =>
      MarkMaxAggregateOutputType(
        id: json['id'],
        name: json['name'],
      );

  final int? id;

  final String? name;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
      };
}

class MarkGroupByOutputType {
  const MarkGroupByOutputType({
    this.id,
    this.name,
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  factory MarkGroupByOutputType.fromJson(Map json) => MarkGroupByOutputType(
        id: json['id'],
        name: json['name'],
        $count: json['_count'] is Map
            ? _i2.MarkCountAggregateOutputType.fromJson(json['_count'])
            : null,
        $avg: json['_avg'] is Map
            ? _i2.MarkAvgAggregateOutputType.fromJson(json['_avg'])
            : null,
        $sum: json['_sum'] is Map
            ? _i2.MarkSumAggregateOutputType.fromJson(json['_sum'])
            : null,
        $min: json['_min'] is Map
            ? _i2.MarkMinAggregateOutputType.fromJson(json['_min'])
            : null,
        $max: json['_max'] is Map
            ? _i2.MarkMaxAggregateOutputType.fromJson(json['_max'])
            : null,
      );

  final int? id;

  final String? name;

  final _i2.MarkCountAggregateOutputType? $count;

  final _i2.MarkAvgAggregateOutputType? $avg;

  final _i2.MarkSumAggregateOutputType? $sum;

  final _i2.MarkMinAggregateOutputType? $min;

  final _i2.MarkMaxAggregateOutputType? $max;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        '_count': $count?.toJson(),
        '_avg': $avg?.toJson(),
        '_sum': $sum?.toJson(),
        '_min': $min?.toJson(),
        '_max': $max?.toJson(),
      };
}

class MarkCountOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkCountOrderByAggregateInput({
    this.id,
    this.name,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
      };
}

class MarkAvgOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkAvgOrderByAggregateInput({this.id});

  final _i2.SortOrder? id;

  @override
  Map<String, dynamic> toJson() => {'id': id};
}

class MarkMaxOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkMaxOrderByAggregateInput({
    this.id,
    this.name,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
      };
}

class MarkMinOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkMinOrderByAggregateInput({
    this.id,
    this.name,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
      };
}

class MarkSumOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkSumOrderByAggregateInput({this.id});

  final _i2.SortOrder? id;

  @override
  Map<String, dynamic> toJson() => {'id': id};
}

class MarkOrderByWithAggregationInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkOrderByWithAggregationInput({
    this.id,
    this.name,
    this.$count,
    this.$avg,
    this.$max,
    this.$min,
    this.$sum,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  final _i2.MarkCountOrderByAggregateInput? $count;

  final _i2.MarkAvgOrderByAggregateInput? $avg;

  final _i2.MarkMaxOrderByAggregateInput? $max;

  final _i2.MarkMinOrderByAggregateInput? $min;

  final _i2.MarkSumOrderByAggregateInput? $sum;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        '_count': $count,
        '_avg': $avg,
        '_max': $max,
        '_min': $min,
        '_sum': $sum,
      };
}

class NestedIntWithAggregatesFilter
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const NestedIntWithAggregatesFilter({
    this.equals,
    this.$in,
    this.notIn,
    this.lt,
    this.lte,
    this.gt,
    this.gte,
    this.not,
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  final int? equals;

  final _i1.PrismaUnion<Iterable<int>, int>? $in;

  final _i1.PrismaUnion<Iterable<int>, int>? notIn;

  final int? lt;

  final int? lte;

  final int? gt;

  final int? gte;

  final _i1.PrismaUnion<int, _i2.NestedIntWithAggregatesFilter>? not;

  final _i2.NestedIntFilter? $count;

  final _i2.NestedFloatFilter? $avg;

  final _i2.NestedIntFilter? $sum;

  final _i2.NestedIntFilter? $min;

  final _i2.NestedIntFilter? $max;

  @override
  Map<String, dynamic> toJson() => {
        'equals': equals,
        'in': $in,
        'notIn': notIn,
        'lt': lt,
        'lte': lte,
        'gt': gt,
        'gte': gte,
        'not': not,
        '_count': $count,
        '_avg': $avg,
        '_sum': $sum,
        '_min': $min,
        '_max': $max,
      };
}

class IntWithAggregatesFilter
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const IntWithAggregatesFilter({
    this.equals,
    this.$in,
    this.notIn,
    this.lt,
    this.lte,
    this.gt,
    this.gte,
    this.not,
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  final int? equals;

  final _i1.PrismaUnion<Iterable<int>, int>? $in;

  final _i1.PrismaUnion<Iterable<int>, int>? notIn;

  final int? lt;

  final int? lte;

  final int? gt;

  final int? gte;

  final _i1.PrismaUnion<int, _i2.NestedIntWithAggregatesFilter>? not;

  final _i2.NestedIntFilter? $count;

  final _i2.NestedFloatFilter? $avg;

  final _i2.NestedIntFilter? $sum;

  final _i2.NestedIntFilter? $min;

  final _i2.NestedIntFilter? $max;

  @override
  Map<String, dynamic> toJson() => {
        'equals': equals,
        'in': $in,
        'notIn': notIn,
        'lt': lt,
        'lte': lte,
        'gt': gt,
        'gte': gte,
        'not': not,
        '_count': $count,
        '_avg': $avg,
        '_sum': $sum,
        '_min': $min,
        '_max': $max,
      };
}

class NestedStringWithAggregatesFilter
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const NestedStringWithAggregatesFilter({
    this.equals,
    this.$in,
    this.notIn,
    this.lt,
    this.lte,
    this.gt,
    this.gte,
    this.contains,
    this.startsWith,
    this.endsWith,
    this.not,
    this.$count,
    this.$min,
    this.$max,
  });

  final String? equals;

  final _i1.PrismaUnion<Iterable<String>, String>? $in;

  final _i1.PrismaUnion<Iterable<String>, String>? notIn;

  final String? lt;

  final String? lte;

  final String? gt;

  final String? gte;

  final String? contains;

  final String? startsWith;

  final String? endsWith;

  final _i1.PrismaUnion<String, _i2.NestedStringWithAggregatesFilter>? not;

  final _i2.NestedIntFilter? $count;

  final _i2.NestedStringFilter? $min;

  final _i2.NestedStringFilter? $max;

  @override
  Map<String, dynamic> toJson() => {
        'equals': equals,
        'in': $in,
        'notIn': notIn,
        'lt': lt,
        'lte': lte,
        'gt': gt,
        'gte': gte,
        'contains': contains,
        'startsWith': startsWith,
        'endsWith': endsWith,
        'not': not,
        '_count': $count,
        '_min': $min,
        '_max': $max,
      };
}

class StringWithAggregatesFilter
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const StringWithAggregatesFilter({
    this.equals,
    this.$in,
    this.notIn,
    this.lt,
    this.lte,
    this.gt,
    this.gte,
    this.contains,
    this.startsWith,
    this.endsWith,
    this.mode,
    this.not,
    this.$count,
    this.$min,
    this.$max,
  });

  final String? equals;

  final _i1.PrismaUnion<Iterable<String>, String>? $in;

  final _i1.PrismaUnion<Iterable<String>, String>? notIn;

  final String? lt;

  final String? lte;

  final String? gt;

  final String? gte;

  final String? contains;

  final String? startsWith;

  final String? endsWith;

  final _i2.QueryMode? mode;

  final _i1.PrismaUnion<String, _i2.NestedStringWithAggregatesFilter>? not;

  final _i2.NestedIntFilter? $count;

  final _i2.NestedStringFilter? $min;

  final _i2.NestedStringFilter? $max;

  @override
  Map<String, dynamic> toJson() => {
        'equals': equals,
        'in': $in,
        'notIn': notIn,
        'lt': lt,
        'lte': lte,
        'gt': gt,
        'gte': gte,
        'contains': contains,
        'startsWith': startsWith,
        'endsWith': endsWith,
        'mode': mode,
        'not': not,
        '_count': $count,
        '_min': $min,
        '_max': $max,
      };
}

class MarkScalarWhereWithAggregatesInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkScalarWhereWithAggregatesInput({
    this.AND,
    this.OR,
    this.NOT,
    this.id,
    this.name,
  });

  final _i1.PrismaUnion<_i2.MarkScalarWhereWithAggregatesInput,
      Iterable<_i2.MarkScalarWhereWithAggregatesInput>>? AND;

  final Iterable<_i2.MarkScalarWhereWithAggregatesInput>? OR;

  final _i1.PrismaUnion<_i2.MarkScalarWhereWithAggregatesInput,
      Iterable<_i2.MarkScalarWhereWithAggregatesInput>>? NOT;

  final _i1.PrismaUnion<_i2.IntWithAggregatesFilter, int>? id;

  final _i1.PrismaUnion<_i2.StringWithAggregatesFilter, String>? name;

  @override
  Map<String, dynamic> toJson() => {
        'AND': AND,
        'OR': OR,
        'NOT': NOT,
        'id': id,
        'name': name,
      };
}

class MarkCountAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkCountAggregateOutputTypeSelect({
    this.id,
    this.name,
    this.$all,
  });

  final bool? id;

  final bool? name;

  final bool? $all;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        '_all': $all,
      };
}

class MarkGroupByOutputTypeCountArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkGroupByOutputTypeCountArgs({this.select});

  final _i2.MarkCountAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class MarkAvgAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkAvgAggregateOutputTypeSelect({this.id});

  final bool? id;

  @override
  Map<String, dynamic> toJson() => {'id': id};
}

class MarkGroupByOutputTypeAvgArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkGroupByOutputTypeAvgArgs({this.select});

  final _i2.MarkAvgAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class MarkSumAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkSumAggregateOutputTypeSelect({this.id});

  final bool? id;

  @override
  Map<String, dynamic> toJson() => {'id': id};
}

class MarkGroupByOutputTypeSumArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkGroupByOutputTypeSumArgs({this.select});

  final _i2.MarkSumAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class MarkMinAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkMinAggregateOutputTypeSelect({
    this.id,
    this.name,
  });

  final bool? id;

  final bool? name;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
      };
}

class MarkGroupByOutputTypeMinArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkGroupByOutputTypeMinArgs({this.select});

  final _i2.MarkMinAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class MarkMaxAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkMaxAggregateOutputTypeSelect({
    this.id,
    this.name,
  });

  final bool? id;

  final bool? name;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
      };
}

class MarkGroupByOutputTypeMaxArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkGroupByOutputTypeMaxArgs({this.select});

  final _i2.MarkMaxAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class MarkGroupByOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkGroupByOutputTypeSelect({
    this.id,
    this.name,
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  final bool? id;

  final bool? name;

  final _i1.PrismaUnion<bool, _i2.MarkGroupByOutputTypeCountArgs>? $count;

  final _i1.PrismaUnion<bool, _i2.MarkGroupByOutputTypeAvgArgs>? $avg;

  final _i1.PrismaUnion<bool, _i2.MarkGroupByOutputTypeSumArgs>? $sum;

  final _i1.PrismaUnion<bool, _i2.MarkGroupByOutputTypeMinArgs>? $min;

  final _i1.PrismaUnion<bool, _i2.MarkGroupByOutputTypeMaxArgs>? $max;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        '_count': $count,
        '_avg': $avg,
        '_sum': $sum,
        '_min': $min,
        '_max': $max,
      };
}

class AggregateMark {
  const AggregateMark({
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  factory AggregateMark.fromJson(Map json) => AggregateMark(
        $count: json['_count'] is Map
            ? _i2.MarkCountAggregateOutputType.fromJson(json['_count'])
            : null,
        $avg: json['_avg'] is Map
            ? _i2.MarkAvgAggregateOutputType.fromJson(json['_avg'])
            : null,
        $sum: json['_sum'] is Map
            ? _i2.MarkSumAggregateOutputType.fromJson(json['_sum'])
            : null,
        $min: json['_min'] is Map
            ? _i2.MarkMinAggregateOutputType.fromJson(json['_min'])
            : null,
        $max: json['_max'] is Map
            ? _i2.MarkMaxAggregateOutputType.fromJson(json['_max'])
            : null,
      );

  final _i2.MarkCountAggregateOutputType? $count;

  final _i2.MarkAvgAggregateOutputType? $avg;

  final _i2.MarkSumAggregateOutputType? $sum;

  final _i2.MarkMinAggregateOutputType? $min;

  final _i2.MarkMaxAggregateOutputType? $max;

  Map<String, dynamic> toJson() => {
        '_count': $count?.toJson(),
        '_avg': $avg?.toJson(),
        '_sum': $sum?.toJson(),
        '_min': $min?.toJson(),
        '_max': $max?.toJson(),
      };
}

class AggregateMarkCountArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateMarkCountArgs({this.select});

  final _i2.MarkCountAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateMarkAvgArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateMarkAvgArgs({this.select});

  final _i2.MarkAvgAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateMarkSumArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateMarkSumArgs({this.select});

  final _i2.MarkSumAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateMarkMinArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateMarkMinArgs({this.select});

  final _i2.MarkMinAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateMarkMaxArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateMarkMaxArgs({this.select});

  final _i2.MarkMaxAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateMarkSelect implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateMarkSelect({
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  final _i1.PrismaUnion<bool, _i2.AggregateMarkCountArgs>? $count;

  final _i1.PrismaUnion<bool, _i2.AggregateMarkAvgArgs>? $avg;

  final _i1.PrismaUnion<bool, _i2.AggregateMarkSumArgs>? $sum;

  final _i1.PrismaUnion<bool, _i2.AggregateMarkMinArgs>? $min;

  final _i1.PrismaUnion<bool, _i2.AggregateMarkMaxArgs>? $max;

  @override
  Map<String, dynamic> toJson() => {
        '_count': $count,
        '_avg': $avg,
        '_sum': $sum,
        '_min': $min,
        '_max': $max,
      };
}

enum CountryScalar<T> implements _i1.PrismaEnum, _i1.Reference<T> {
  id<int>('id', 'Country'),
  name$<String>('name', 'Country'),
  code<String>('code', 'Country');

  const CountryScalar(
    this.name,
    this.model,
  );

  @override
  final String name;

  @override
  final String model;
}

class MarkCreateWithoutModelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkCreateWithoutModelInput({required this.name});

  final String name;

  @override
  Map<String, dynamic> toJson() => {'name': name};
}

class MarkUncheckedCreateWithoutModelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkUncheckedCreateWithoutModelInput({
    this.id,
    required this.name,
  });

  final int? id;

  final String name;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
      };
}

class MarkCreateOrConnectWithoutModelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkCreateOrConnectWithoutModelInput({
    required this.where,
    required this.create,
  });

  final _i2.MarkWhereUniqueInput where;

  final _i1.PrismaUnion<_i2.MarkCreateWithoutModelInput,
      _i2.MarkUncheckedCreateWithoutModelInput> create;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'create': create,
      };
}

class MarkCreateNestedOneWithoutModelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkCreateNestedOneWithoutModelInput({
    this.create,
    this.connectOrCreate,
    this.connect,
  });

  final _i1.PrismaUnion<_i2.MarkCreateWithoutModelInput,
      _i2.MarkUncheckedCreateWithoutModelInput>? create;

  final _i2.MarkCreateOrConnectWithoutModelInput? connectOrCreate;

  final _i2.MarkWhereUniqueInput? connect;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'connect': connect,
      };
}

class ModelCreateWithoutCountryInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelCreateWithoutCountryInput({
    required this.name,
    required this.mark,
    this.car,
  });

  final String name;

  final _i2.MarkCreateNestedOneWithoutModelInput mark;

  final _i2.CarCreateNestedManyWithoutModelInput? car;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'mark': mark,
        'Car': car,
      };
}

class ModelUncheckedCreateWithoutCountryInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelUncheckedCreateWithoutCountryInput({
    this.id,
    required this.name,
    required this.markId,
    this.car,
  });

  final int? id;

  final String name;

  final int markId;

  final _i2.CarUncheckedCreateNestedManyWithoutModelInput? car;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'mark_id': markId,
        'Car': car,
      };
}

class ModelCreateOrConnectWithoutCountryInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelCreateOrConnectWithoutCountryInput({
    required this.where,
    required this.create,
  });

  final _i2.ModelWhereUniqueInput where;

  final _i1.PrismaUnion<_i2.ModelCreateWithoutCountryInput,
      _i2.ModelUncheckedCreateWithoutCountryInput> create;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'create': create,
      };
}

class ModelCreateManyCountryInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelCreateManyCountryInput({
    this.id,
    required this.name,
    required this.markId,
  });

  final int? id;

  final String name;

  final int markId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'mark_id': markId,
      };
}

class ModelCreateManyCountryInputEnvelope
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelCreateManyCountryInputEnvelope({
    required this.data,
    this.skipDuplicates,
  });

  final _i1.PrismaUnion<_i2.ModelCreateManyCountryInput,
      Iterable<_i2.ModelCreateManyCountryInput>> data;

  final bool? skipDuplicates;

  @override
  Map<String, dynamic> toJson() => {
        'data': data,
        'skipDuplicates': skipDuplicates,
      };
}

class ModelCreateNestedManyWithoutCountryInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelCreateNestedManyWithoutCountryInput({
    this.create,
    this.connectOrCreate,
    this.createMany,
    this.connect,
  });

  final _i1.PrismaUnion<
      _i2.ModelCreateWithoutCountryInput,
      _i1.PrismaUnion<
          Iterable<_i2.ModelCreateWithoutCountryInput>,
          _i1.PrismaUnion<_i2.ModelUncheckedCreateWithoutCountryInput,
              Iterable<_i2.ModelUncheckedCreateWithoutCountryInput>>>>? create;

  final _i1.PrismaUnion<_i2.ModelCreateOrConnectWithoutCountryInput,
      Iterable<_i2.ModelCreateOrConnectWithoutCountryInput>>? connectOrCreate;

  final _i2.ModelCreateManyCountryInputEnvelope? createMany;

  final _i1.PrismaUnion<_i2.ModelWhereUniqueInput,
      Iterable<_i2.ModelWhereUniqueInput>>? connect;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'createMany': createMany,
        'connect': connect,
      };
}

class CountryCreateInput implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryCreateInput({
    required this.name,
    required this.code,
    this.model,
  });

  final String name;

  final String code;

  final _i2.ModelCreateNestedManyWithoutCountryInput? model;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'code': code,
        'Model': model,
      };
}

class ModelUncheckedCreateNestedManyWithoutCountryInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelUncheckedCreateNestedManyWithoutCountryInput({
    this.create,
    this.connectOrCreate,
    this.createMany,
    this.connect,
  });

  final _i1.PrismaUnion<
      _i2.ModelCreateWithoutCountryInput,
      _i1.PrismaUnion<
          Iterable<_i2.ModelCreateWithoutCountryInput>,
          _i1.PrismaUnion<_i2.ModelUncheckedCreateWithoutCountryInput,
              Iterable<_i2.ModelUncheckedCreateWithoutCountryInput>>>>? create;

  final _i1.PrismaUnion<_i2.ModelCreateOrConnectWithoutCountryInput,
      Iterable<_i2.ModelCreateOrConnectWithoutCountryInput>>? connectOrCreate;

  final _i2.ModelCreateManyCountryInputEnvelope? createMany;

  final _i1.PrismaUnion<_i2.ModelWhereUniqueInput,
      Iterable<_i2.ModelWhereUniqueInput>>? connect;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'createMany': createMany,
        'connect': connect,
      };
}

class CountryUncheckedCreateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryUncheckedCreateInput({
    this.id,
    required this.name,
    required this.code,
    this.model,
  });

  final int? id;

  final String name;

  final String code;

  final _i2.ModelUncheckedCreateNestedManyWithoutCountryInput? model;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'code': code,
        'Model': model,
      };
}

class CountryCreateManyInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryCreateManyInput({
    this.id,
    required this.name,
    required this.code,
  });

  final int? id;

  final String name;

  final String code;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'code': code,
      };
}

class MarkUpdateWithoutModelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkUpdateWithoutModelInput({this.name});

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  @override
  Map<String, dynamic> toJson() => {'name': name};
}

class MarkUncheckedUpdateWithoutModelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkUncheckedUpdateWithoutModelInput({
    this.id,
    this.name,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
      };
}

class MarkUpsertWithoutModelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkUpsertWithoutModelInput({
    required this.update,
    required this.create,
  });

  final _i1.PrismaUnion<_i2.MarkUpdateWithoutModelInput,
      _i2.MarkUncheckedUpdateWithoutModelInput> update;

  final _i1.PrismaUnion<_i2.MarkCreateWithoutModelInput,
      _i2.MarkUncheckedCreateWithoutModelInput> create;

  @override
  Map<String, dynamic> toJson() => {
        'update': update,
        'create': create,
      };
}

class MarkUpdateOneRequiredWithoutModelNestedInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const MarkUpdateOneRequiredWithoutModelNestedInput({
    this.create,
    this.connectOrCreate,
    this.upsert,
    this.connect,
    this.update,
  });

  final _i1.PrismaUnion<_i2.MarkCreateWithoutModelInput,
      _i2.MarkUncheckedCreateWithoutModelInput>? create;

  final _i2.MarkCreateOrConnectWithoutModelInput? connectOrCreate;

  final _i2.MarkUpsertWithoutModelInput? upsert;

  final _i2.MarkWhereUniqueInput? connect;

  final _i1.PrismaUnion<_i2.MarkUpdateWithoutModelInput,
      _i2.MarkUncheckedUpdateWithoutModelInput>? update;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'upsert': upsert,
        'connect': connect,
        'update': update,
      };
}

class ModelUpdateWithoutCountryInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelUpdateWithoutCountryInput({
    this.name,
    this.mark,
    this.car,
  });

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i2.MarkUpdateOneRequiredWithoutModelNestedInput? mark;

  final _i2.CarUpdateManyWithoutModelNestedInput? car;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'mark': mark,
        'Car': car,
      };
}

class ModelUncheckedUpdateWithoutCountryInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelUncheckedUpdateWithoutCountryInput({
    this.id,
    this.name,
    this.markId,
    this.car,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? markId;

  final _i2.CarUncheckedUpdateManyWithoutModelNestedInput? car;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'mark_id': markId,
        'Car': car,
      };
}

class ModelUpsertWithWhereUniqueWithoutCountryInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelUpsertWithWhereUniqueWithoutCountryInput({
    required this.where,
    required this.update,
    required this.create,
  });

  final _i2.ModelWhereUniqueInput where;

  final _i1.PrismaUnion<_i2.ModelUpdateWithoutCountryInput,
      _i2.ModelUncheckedUpdateWithoutCountryInput> update;

  final _i1.PrismaUnion<_i2.ModelCreateWithoutCountryInput,
      _i2.ModelUncheckedCreateWithoutCountryInput> create;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'update': update,
        'create': create,
      };
}

class ModelUpdateWithWhereUniqueWithoutCountryInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelUpdateWithWhereUniqueWithoutCountryInput({
    required this.where,
    required this.data,
  });

  final _i2.ModelWhereUniqueInput where;

  final _i1.PrismaUnion<_i2.ModelUpdateWithoutCountryInput,
      _i2.ModelUncheckedUpdateWithoutCountryInput> data;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'data': data,
      };
}

class ModelUpdateManyWithWhereWithoutCountryInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelUpdateManyWithWhereWithoutCountryInput({
    required this.where,
    required this.data,
  });

  final _i2.ModelScalarWhereInput where;

  final _i1.PrismaUnion<_i2.ModelUpdateManyMutationInput,
      _i2.ModelUncheckedUpdateManyWithoutModelInput> data;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'data': data,
      };
}

class ModelUpdateManyWithoutCountryNestedInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelUpdateManyWithoutCountryNestedInput({
    this.create,
    this.connectOrCreate,
    this.upsert,
    this.createMany,
    this.set,
    this.disconnect,
    this.delete,
    this.connect,
    this.update,
    this.updateMany,
    this.deleteMany,
  });

  final _i1.PrismaUnion<
      _i2.ModelCreateWithoutCountryInput,
      _i1.PrismaUnion<
          Iterable<_i2.ModelCreateWithoutCountryInput>,
          _i1.PrismaUnion<_i2.ModelUncheckedCreateWithoutCountryInput,
              Iterable<_i2.ModelUncheckedCreateWithoutCountryInput>>>>? create;

  final _i1.PrismaUnion<_i2.ModelCreateOrConnectWithoutCountryInput,
      Iterable<_i2.ModelCreateOrConnectWithoutCountryInput>>? connectOrCreate;

  final _i1.PrismaUnion<_i2.ModelUpsertWithWhereUniqueWithoutCountryInput,
      Iterable<_i2.ModelUpsertWithWhereUniqueWithoutCountryInput>>? upsert;

  final _i2.ModelCreateManyCountryInputEnvelope? createMany;

  final _i1.PrismaUnion<_i2.ModelWhereUniqueInput,
      Iterable<_i2.ModelWhereUniqueInput>>? set;

  final _i1.PrismaUnion<_i2.ModelWhereUniqueInput,
      Iterable<_i2.ModelWhereUniqueInput>>? disconnect;

  final _i1.PrismaUnion<_i2.ModelWhereUniqueInput,
      Iterable<_i2.ModelWhereUniqueInput>>? delete;

  final _i1.PrismaUnion<_i2.ModelWhereUniqueInput,
      Iterable<_i2.ModelWhereUniqueInput>>? connect;

  final _i1.PrismaUnion<_i2.ModelUpdateWithWhereUniqueWithoutCountryInput,
      Iterable<_i2.ModelUpdateWithWhereUniqueWithoutCountryInput>>? update;

  final _i1.PrismaUnion<_i2.ModelUpdateManyWithWhereWithoutCountryInput,
      Iterable<_i2.ModelUpdateManyWithWhereWithoutCountryInput>>? updateMany;

  final _i1.PrismaUnion<_i2.ModelScalarWhereInput,
      Iterable<_i2.ModelScalarWhereInput>>? deleteMany;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'upsert': upsert,
        'createMany': createMany,
        'set': set,
        'disconnect': disconnect,
        'delete': delete,
        'connect': connect,
        'update': update,
        'updateMany': updateMany,
        'deleteMany': deleteMany,
      };
}

class CountryUpdateInput implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryUpdateInput({
    this.name,
    this.code,
    this.model,
  });

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? code;

  final _i2.ModelUpdateManyWithoutCountryNestedInput? model;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'code': code,
        'Model': model,
      };
}

class ModelUncheckedUpdateManyWithoutCountryNestedInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelUncheckedUpdateManyWithoutCountryNestedInput({
    this.create,
    this.connectOrCreate,
    this.upsert,
    this.createMany,
    this.set,
    this.disconnect,
    this.delete,
    this.connect,
    this.update,
    this.updateMany,
    this.deleteMany,
  });

  final _i1.PrismaUnion<
      _i2.ModelCreateWithoutCountryInput,
      _i1.PrismaUnion<
          Iterable<_i2.ModelCreateWithoutCountryInput>,
          _i1.PrismaUnion<_i2.ModelUncheckedCreateWithoutCountryInput,
              Iterable<_i2.ModelUncheckedCreateWithoutCountryInput>>>>? create;

  final _i1.PrismaUnion<_i2.ModelCreateOrConnectWithoutCountryInput,
      Iterable<_i2.ModelCreateOrConnectWithoutCountryInput>>? connectOrCreate;

  final _i1.PrismaUnion<_i2.ModelUpsertWithWhereUniqueWithoutCountryInput,
      Iterable<_i2.ModelUpsertWithWhereUniqueWithoutCountryInput>>? upsert;

  final _i2.ModelCreateManyCountryInputEnvelope? createMany;

  final _i1.PrismaUnion<_i2.ModelWhereUniqueInput,
      Iterable<_i2.ModelWhereUniqueInput>>? set;

  final _i1.PrismaUnion<_i2.ModelWhereUniqueInput,
      Iterable<_i2.ModelWhereUniqueInput>>? disconnect;

  final _i1.PrismaUnion<_i2.ModelWhereUniqueInput,
      Iterable<_i2.ModelWhereUniqueInput>>? delete;

  final _i1.PrismaUnion<_i2.ModelWhereUniqueInput,
      Iterable<_i2.ModelWhereUniqueInput>>? connect;

  final _i1.PrismaUnion<_i2.ModelUpdateWithWhereUniqueWithoutCountryInput,
      Iterable<_i2.ModelUpdateWithWhereUniqueWithoutCountryInput>>? update;

  final _i1.PrismaUnion<_i2.ModelUpdateManyWithWhereWithoutCountryInput,
      Iterable<_i2.ModelUpdateManyWithWhereWithoutCountryInput>>? updateMany;

  final _i1.PrismaUnion<_i2.ModelScalarWhereInput,
      Iterable<_i2.ModelScalarWhereInput>>? deleteMany;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'upsert': upsert,
        'createMany': createMany,
        'set': set,
        'disconnect': disconnect,
        'delete': delete,
        'connect': connect,
        'update': update,
        'updateMany': updateMany,
        'deleteMany': deleteMany,
      };
}

class CountryUncheckedUpdateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryUncheckedUpdateInput({
    this.id,
    this.name,
    this.code,
    this.model,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? code;

  final _i2.ModelUncheckedUpdateManyWithoutCountryNestedInput? model;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'code': code,
        'Model': model,
      };
}

class CountryUpdateManyMutationInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryUpdateManyMutationInput({
    this.name,
    this.code,
  });

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? code;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'code': code,
      };
}

class CountryUncheckedUpdateManyInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryUncheckedUpdateManyInput({
    this.id,
    this.name,
    this.code,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? code;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'code': code,
      };
}

class CountryCountAggregateOutputType {
  const CountryCountAggregateOutputType({
    this.id,
    this.name,
    this.code,
    this.$all,
  });

  factory CountryCountAggregateOutputType.fromJson(Map json) =>
      CountryCountAggregateOutputType(
        id: json['id'],
        name: json['name'],
        code: json['code'],
        $all: json['_all'],
      );

  final int? id;

  final int? name;

  final int? code;

  final int? $all;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'code': code,
        '_all': $all,
      };
}

class CountryAvgAggregateOutputType {
  const CountryAvgAggregateOutputType({this.id});

  factory CountryAvgAggregateOutputType.fromJson(Map json) =>
      CountryAvgAggregateOutputType(id: json['id']);

  final double? id;

  Map<String, dynamic> toJson() => {'id': id};
}

class CountrySumAggregateOutputType {
  const CountrySumAggregateOutputType({this.id});

  factory CountrySumAggregateOutputType.fromJson(Map json) =>
      CountrySumAggregateOutputType(id: json['id']);

  final int? id;

  Map<String, dynamic> toJson() => {'id': id};
}

class CountryMinAggregateOutputType {
  const CountryMinAggregateOutputType({
    this.id,
    this.name,
    this.code,
  });

  factory CountryMinAggregateOutputType.fromJson(Map json) =>
      CountryMinAggregateOutputType(
        id: json['id'],
        name: json['name'],
        code: json['code'],
      );

  final int? id;

  final String? name;

  final String? code;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'code': code,
      };
}

class CountryMaxAggregateOutputType {
  const CountryMaxAggregateOutputType({
    this.id,
    this.name,
    this.code,
  });

  factory CountryMaxAggregateOutputType.fromJson(Map json) =>
      CountryMaxAggregateOutputType(
        id: json['id'],
        name: json['name'],
        code: json['code'],
      );

  final int? id;

  final String? name;

  final String? code;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'code': code,
      };
}

class CountryGroupByOutputType {
  const CountryGroupByOutputType({
    this.id,
    this.name,
    this.code,
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  factory CountryGroupByOutputType.fromJson(Map json) =>
      CountryGroupByOutputType(
        id: json['id'],
        name: json['name'],
        code: json['code'],
        $count: json['_count'] is Map
            ? _i2.CountryCountAggregateOutputType.fromJson(json['_count'])
            : null,
        $avg: json['_avg'] is Map
            ? _i2.CountryAvgAggregateOutputType.fromJson(json['_avg'])
            : null,
        $sum: json['_sum'] is Map
            ? _i2.CountrySumAggregateOutputType.fromJson(json['_sum'])
            : null,
        $min: json['_min'] is Map
            ? _i2.CountryMinAggregateOutputType.fromJson(json['_min'])
            : null,
        $max: json['_max'] is Map
            ? _i2.CountryMaxAggregateOutputType.fromJson(json['_max'])
            : null,
      );

  final int? id;

  final String? name;

  final String? code;

  final _i2.CountryCountAggregateOutputType? $count;

  final _i2.CountryAvgAggregateOutputType? $avg;

  final _i2.CountrySumAggregateOutputType? $sum;

  final _i2.CountryMinAggregateOutputType? $min;

  final _i2.CountryMaxAggregateOutputType? $max;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'code': code,
        '_count': $count?.toJson(),
        '_avg': $avg?.toJson(),
        '_sum': $sum?.toJson(),
        '_min': $min?.toJson(),
        '_max': $max?.toJson(),
      };
}

class CountryCountOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryCountOrderByAggregateInput({
    this.id,
    this.name,
    this.code,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  final _i2.SortOrder? code;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'code': code,
      };
}

class CountryAvgOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryAvgOrderByAggregateInput({this.id});

  final _i2.SortOrder? id;

  @override
  Map<String, dynamic> toJson() => {'id': id};
}

class CountryMaxOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryMaxOrderByAggregateInput({
    this.id,
    this.name,
    this.code,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  final _i2.SortOrder? code;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'code': code,
      };
}

class CountryMinOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryMinOrderByAggregateInput({
    this.id,
    this.name,
    this.code,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  final _i2.SortOrder? code;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'code': code,
      };
}

class CountrySumOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountrySumOrderByAggregateInput({this.id});

  final _i2.SortOrder? id;

  @override
  Map<String, dynamic> toJson() => {'id': id};
}

class CountryOrderByWithAggregationInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryOrderByWithAggregationInput({
    this.id,
    this.name,
    this.code,
    this.$count,
    this.$avg,
    this.$max,
    this.$min,
    this.$sum,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  final _i2.SortOrder? code;

  final _i2.CountryCountOrderByAggregateInput? $count;

  final _i2.CountryAvgOrderByAggregateInput? $avg;

  final _i2.CountryMaxOrderByAggregateInput? $max;

  final _i2.CountryMinOrderByAggregateInput? $min;

  final _i2.CountrySumOrderByAggregateInput? $sum;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'code': code,
        '_count': $count,
        '_avg': $avg,
        '_max': $max,
        '_min': $min,
        '_sum': $sum,
      };
}

class CountryScalarWhereWithAggregatesInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryScalarWhereWithAggregatesInput({
    this.AND,
    this.OR,
    this.NOT,
    this.id,
    this.name,
    this.code,
  });

  final _i1.PrismaUnion<_i2.CountryScalarWhereWithAggregatesInput,
      Iterable<_i2.CountryScalarWhereWithAggregatesInput>>? AND;

  final Iterable<_i2.CountryScalarWhereWithAggregatesInput>? OR;

  final _i1.PrismaUnion<_i2.CountryScalarWhereWithAggregatesInput,
      Iterable<_i2.CountryScalarWhereWithAggregatesInput>>? NOT;

  final _i1.PrismaUnion<_i2.IntWithAggregatesFilter, int>? id;

  final _i1.PrismaUnion<_i2.StringWithAggregatesFilter, String>? name;

  final _i1.PrismaUnion<_i2.StringWithAggregatesFilter, String>? code;

  @override
  Map<String, dynamic> toJson() => {
        'AND': AND,
        'OR': OR,
        'NOT': NOT,
        'id': id,
        'name': name,
        'code': code,
      };
}

class CountryCountAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryCountAggregateOutputTypeSelect({
    this.id,
    this.name,
    this.code,
    this.$all,
  });

  final bool? id;

  final bool? name;

  final bool? code;

  final bool? $all;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'code': code,
        '_all': $all,
      };
}

class CountryGroupByOutputTypeCountArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryGroupByOutputTypeCountArgs({this.select});

  final _i2.CountryCountAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class CountryAvgAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryAvgAggregateOutputTypeSelect({this.id});

  final bool? id;

  @override
  Map<String, dynamic> toJson() => {'id': id};
}

class CountryGroupByOutputTypeAvgArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryGroupByOutputTypeAvgArgs({this.select});

  final _i2.CountryAvgAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class CountrySumAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountrySumAggregateOutputTypeSelect({this.id});

  final bool? id;

  @override
  Map<String, dynamic> toJson() => {'id': id};
}

class CountryGroupByOutputTypeSumArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryGroupByOutputTypeSumArgs({this.select});

  final _i2.CountrySumAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class CountryMinAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryMinAggregateOutputTypeSelect({
    this.id,
    this.name,
    this.code,
  });

  final bool? id;

  final bool? name;

  final bool? code;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'code': code,
      };
}

class CountryGroupByOutputTypeMinArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryGroupByOutputTypeMinArgs({this.select});

  final _i2.CountryMinAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class CountryMaxAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryMaxAggregateOutputTypeSelect({
    this.id,
    this.name,
    this.code,
  });

  final bool? id;

  final bool? name;

  final bool? code;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'code': code,
      };
}

class CountryGroupByOutputTypeMaxArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryGroupByOutputTypeMaxArgs({this.select});

  final _i2.CountryMaxAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class CountryGroupByOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CountryGroupByOutputTypeSelect({
    this.id,
    this.name,
    this.code,
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  final bool? id;

  final bool? name;

  final bool? code;

  final _i1.PrismaUnion<bool, _i2.CountryGroupByOutputTypeCountArgs>? $count;

  final _i1.PrismaUnion<bool, _i2.CountryGroupByOutputTypeAvgArgs>? $avg;

  final _i1.PrismaUnion<bool, _i2.CountryGroupByOutputTypeSumArgs>? $sum;

  final _i1.PrismaUnion<bool, _i2.CountryGroupByOutputTypeMinArgs>? $min;

  final _i1.PrismaUnion<bool, _i2.CountryGroupByOutputTypeMaxArgs>? $max;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'code': code,
        '_count': $count,
        '_avg': $avg,
        '_sum': $sum,
        '_min': $min,
        '_max': $max,
      };
}

class AggregateCountry {
  const AggregateCountry({
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  factory AggregateCountry.fromJson(Map json) => AggregateCountry(
        $count: json['_count'] is Map
            ? _i2.CountryCountAggregateOutputType.fromJson(json['_count'])
            : null,
        $avg: json['_avg'] is Map
            ? _i2.CountryAvgAggregateOutputType.fromJson(json['_avg'])
            : null,
        $sum: json['_sum'] is Map
            ? _i2.CountrySumAggregateOutputType.fromJson(json['_sum'])
            : null,
        $min: json['_min'] is Map
            ? _i2.CountryMinAggregateOutputType.fromJson(json['_min'])
            : null,
        $max: json['_max'] is Map
            ? _i2.CountryMaxAggregateOutputType.fromJson(json['_max'])
            : null,
      );

  final _i2.CountryCountAggregateOutputType? $count;

  final _i2.CountryAvgAggregateOutputType? $avg;

  final _i2.CountrySumAggregateOutputType? $sum;

  final _i2.CountryMinAggregateOutputType? $min;

  final _i2.CountryMaxAggregateOutputType? $max;

  Map<String, dynamic> toJson() => {
        '_count': $count?.toJson(),
        '_avg': $avg?.toJson(),
        '_sum': $sum?.toJson(),
        '_min': $min?.toJson(),
        '_max': $max?.toJson(),
      };
}

class AggregateCountryCountArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateCountryCountArgs({this.select});

  final _i2.CountryCountAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateCountryAvgArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateCountryAvgArgs({this.select});

  final _i2.CountryAvgAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateCountrySumArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateCountrySumArgs({this.select});

  final _i2.CountrySumAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateCountryMinArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateCountryMinArgs({this.select});

  final _i2.CountryMinAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateCountryMaxArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateCountryMaxArgs({this.select});

  final _i2.CountryMaxAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateCountrySelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateCountrySelect({
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  final _i1.PrismaUnion<bool, _i2.AggregateCountryCountArgs>? $count;

  final _i1.PrismaUnion<bool, _i2.AggregateCountryAvgArgs>? $avg;

  final _i1.PrismaUnion<bool, _i2.AggregateCountrySumArgs>? $sum;

  final _i1.PrismaUnion<bool, _i2.AggregateCountryMinArgs>? $min;

  final _i1.PrismaUnion<bool, _i2.AggregateCountryMaxArgs>? $max;

  @override
  Map<String, dynamic> toJson() => {
        '_count': $count,
        '_avg': $avg,
        '_sum': $sum,
        '_min': $min,
        '_max': $max,
      };
}

class ModelCreateInput implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelCreateInput({
    required this.name,
    required this.mark,
    required this.country,
    this.car,
  });

  final String name;

  final _i2.MarkCreateNestedOneWithoutModelInput mark;

  final _i2.CountryCreateNestedOneWithoutModelInput country;

  final _i2.CarCreateNestedManyWithoutModelInput? car;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'mark': mark,
        'country': country,
        'Car': car,
      };
}

class ModelUncheckedCreateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelUncheckedCreateInput({
    this.id,
    required this.name,
    required this.markId,
    required this.countryId,
    this.car,
  });

  final int? id;

  final String name;

  final int markId;

  final int countryId;

  final _i2.CarUncheckedCreateNestedManyWithoutModelInput? car;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'mark_id': markId,
        'country_id': countryId,
        'Car': car,
      };
}

class ModelCreateManyInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelCreateManyInput({
    this.id,
    required this.name,
    required this.markId,
    required this.countryId,
  });

  final int? id;

  final String name;

  final int markId;

  final int countryId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'mark_id': markId,
        'country_id': countryId,
      };
}

class ModelUpdateInput implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelUpdateInput({
    this.name,
    this.mark,
    this.country,
    this.car,
  });

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i2.MarkUpdateOneRequiredWithoutModelNestedInput? mark;

  final _i2.CountryUpdateOneRequiredWithoutModelNestedInput? country;

  final _i2.CarUpdateManyWithoutModelNestedInput? car;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'mark': mark,
        'country': country,
        'Car': car,
      };
}

class ModelUncheckedUpdateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelUncheckedUpdateInput({
    this.id,
    this.name,
    this.markId,
    this.countryId,
    this.car,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? markId;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? countryId;

  final _i2.CarUncheckedUpdateManyWithoutModelNestedInput? car;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'mark_id': markId,
        'country_id': countryId,
        'Car': car,
      };
}

class ModelUncheckedUpdateManyInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelUncheckedUpdateManyInput({
    this.id,
    this.name,
    this.markId,
    this.countryId,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? markId;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? countryId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'mark_id': markId,
        'country_id': countryId,
      };
}

class ModelCountAggregateOutputType {
  const ModelCountAggregateOutputType({
    this.id,
    this.name,
    this.markId,
    this.countryId,
    this.$all,
  });

  factory ModelCountAggregateOutputType.fromJson(Map json) =>
      ModelCountAggregateOutputType(
        id: json['id'],
        name: json['name'],
        markId: json['mark_id'],
        countryId: json['country_id'],
        $all: json['_all'],
      );

  final int? id;

  final int? name;

  final int? markId;

  final int? countryId;

  final int? $all;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'mark_id': markId,
        'country_id': countryId,
        '_all': $all,
      };
}

class ModelAvgAggregateOutputType {
  const ModelAvgAggregateOutputType({
    this.id,
    this.markId,
    this.countryId,
  });

  factory ModelAvgAggregateOutputType.fromJson(Map json) =>
      ModelAvgAggregateOutputType(
        id: json['id'],
        markId: json['mark_id'],
        countryId: json['country_id'],
      );

  final double? id;

  final double? markId;

  final double? countryId;

  Map<String, dynamic> toJson() => {
        'id': id,
        'mark_id': markId,
        'country_id': countryId,
      };
}

class ModelSumAggregateOutputType {
  const ModelSumAggregateOutputType({
    this.id,
    this.markId,
    this.countryId,
  });

  factory ModelSumAggregateOutputType.fromJson(Map json) =>
      ModelSumAggregateOutputType(
        id: json['id'],
        markId: json['mark_id'],
        countryId: json['country_id'],
      );

  final int? id;

  final int? markId;

  final int? countryId;

  Map<String, dynamic> toJson() => {
        'id': id,
        'mark_id': markId,
        'country_id': countryId,
      };
}

class ModelMinAggregateOutputType {
  const ModelMinAggregateOutputType({
    this.id,
    this.name,
    this.markId,
    this.countryId,
  });

  factory ModelMinAggregateOutputType.fromJson(Map json) =>
      ModelMinAggregateOutputType(
        id: json['id'],
        name: json['name'],
        markId: json['mark_id'],
        countryId: json['country_id'],
      );

  final int? id;

  final String? name;

  final int? markId;

  final int? countryId;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'mark_id': markId,
        'country_id': countryId,
      };
}

class ModelMaxAggregateOutputType {
  const ModelMaxAggregateOutputType({
    this.id,
    this.name,
    this.markId,
    this.countryId,
  });

  factory ModelMaxAggregateOutputType.fromJson(Map json) =>
      ModelMaxAggregateOutputType(
        id: json['id'],
        name: json['name'],
        markId: json['mark_id'],
        countryId: json['country_id'],
      );

  final int? id;

  final String? name;

  final int? markId;

  final int? countryId;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'mark_id': markId,
        'country_id': countryId,
      };
}

class ModelGroupByOutputType {
  const ModelGroupByOutputType({
    this.id,
    this.name,
    this.markId,
    this.countryId,
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  factory ModelGroupByOutputType.fromJson(Map json) => ModelGroupByOutputType(
        id: json['id'],
        name: json['name'],
        markId: json['mark_id'],
        countryId: json['country_id'],
        $count: json['_count'] is Map
            ? _i2.ModelCountAggregateOutputType.fromJson(json['_count'])
            : null,
        $avg: json['_avg'] is Map
            ? _i2.ModelAvgAggregateOutputType.fromJson(json['_avg'])
            : null,
        $sum: json['_sum'] is Map
            ? _i2.ModelSumAggregateOutputType.fromJson(json['_sum'])
            : null,
        $min: json['_min'] is Map
            ? _i2.ModelMinAggregateOutputType.fromJson(json['_min'])
            : null,
        $max: json['_max'] is Map
            ? _i2.ModelMaxAggregateOutputType.fromJson(json['_max'])
            : null,
      );

  final int? id;

  final String? name;

  final int? markId;

  final int? countryId;

  final _i2.ModelCountAggregateOutputType? $count;

  final _i2.ModelAvgAggregateOutputType? $avg;

  final _i2.ModelSumAggregateOutputType? $sum;

  final _i2.ModelMinAggregateOutputType? $min;

  final _i2.ModelMaxAggregateOutputType? $max;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'mark_id': markId,
        'country_id': countryId,
        '_count': $count?.toJson(),
        '_avg': $avg?.toJson(),
        '_sum': $sum?.toJson(),
        '_min': $min?.toJson(),
        '_max': $max?.toJson(),
      };
}

class ModelCountOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelCountOrderByAggregateInput({
    this.id,
    this.name,
    this.markId,
    this.countryId,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  final _i2.SortOrder? markId;

  final _i2.SortOrder? countryId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'mark_id': markId,
        'country_id': countryId,
      };
}

class ModelAvgOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelAvgOrderByAggregateInput({
    this.id,
    this.markId,
    this.countryId,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? markId;

  final _i2.SortOrder? countryId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'mark_id': markId,
        'country_id': countryId,
      };
}

class ModelMaxOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelMaxOrderByAggregateInput({
    this.id,
    this.name,
    this.markId,
    this.countryId,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  final _i2.SortOrder? markId;

  final _i2.SortOrder? countryId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'mark_id': markId,
        'country_id': countryId,
      };
}

class ModelMinOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelMinOrderByAggregateInput({
    this.id,
    this.name,
    this.markId,
    this.countryId,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  final _i2.SortOrder? markId;

  final _i2.SortOrder? countryId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'mark_id': markId,
        'country_id': countryId,
      };
}

class ModelSumOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelSumOrderByAggregateInput({
    this.id,
    this.markId,
    this.countryId,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? markId;

  final _i2.SortOrder? countryId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'mark_id': markId,
        'country_id': countryId,
      };
}

class ModelOrderByWithAggregationInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelOrderByWithAggregationInput({
    this.id,
    this.name,
    this.markId,
    this.countryId,
    this.$count,
    this.$avg,
    this.$max,
    this.$min,
    this.$sum,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  final _i2.SortOrder? markId;

  final _i2.SortOrder? countryId;

  final _i2.ModelCountOrderByAggregateInput? $count;

  final _i2.ModelAvgOrderByAggregateInput? $avg;

  final _i2.ModelMaxOrderByAggregateInput? $max;

  final _i2.ModelMinOrderByAggregateInput? $min;

  final _i2.ModelSumOrderByAggregateInput? $sum;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'mark_id': markId,
        'country_id': countryId,
        '_count': $count,
        '_avg': $avg,
        '_max': $max,
        '_min': $min,
        '_sum': $sum,
      };
}

class ModelScalarWhereWithAggregatesInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelScalarWhereWithAggregatesInput({
    this.AND,
    this.OR,
    this.NOT,
    this.id,
    this.name,
    this.markId,
    this.countryId,
  });

  final _i1.PrismaUnion<_i2.ModelScalarWhereWithAggregatesInput,
      Iterable<_i2.ModelScalarWhereWithAggregatesInput>>? AND;

  final Iterable<_i2.ModelScalarWhereWithAggregatesInput>? OR;

  final _i1.PrismaUnion<_i2.ModelScalarWhereWithAggregatesInput,
      Iterable<_i2.ModelScalarWhereWithAggregatesInput>>? NOT;

  final _i1.PrismaUnion<_i2.IntWithAggregatesFilter, int>? id;

  final _i1.PrismaUnion<_i2.StringWithAggregatesFilter, String>? name;

  final _i1.PrismaUnion<_i2.IntWithAggregatesFilter, int>? markId;

  final _i1.PrismaUnion<_i2.IntWithAggregatesFilter, int>? countryId;

  @override
  Map<String, dynamic> toJson() => {
        'AND': AND,
        'OR': OR,
        'NOT': NOT,
        'id': id,
        'name': name,
        'mark_id': markId,
        'country_id': countryId,
      };
}

class ModelCountAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelCountAggregateOutputTypeSelect({
    this.id,
    this.name,
    this.markId,
    this.countryId,
    this.$all,
  });

  final bool? id;

  final bool? name;

  final bool? markId;

  final bool? countryId;

  final bool? $all;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'mark_id': markId,
        'country_id': countryId,
        '_all': $all,
      };
}

class ModelGroupByOutputTypeCountArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelGroupByOutputTypeCountArgs({this.select});

  final _i2.ModelCountAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class ModelAvgAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelAvgAggregateOutputTypeSelect({
    this.id,
    this.markId,
    this.countryId,
  });

  final bool? id;

  final bool? markId;

  final bool? countryId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'mark_id': markId,
        'country_id': countryId,
      };
}

class ModelGroupByOutputTypeAvgArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelGroupByOutputTypeAvgArgs({this.select});

  final _i2.ModelAvgAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class ModelSumAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelSumAggregateOutputTypeSelect({
    this.id,
    this.markId,
    this.countryId,
  });

  final bool? id;

  final bool? markId;

  final bool? countryId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'mark_id': markId,
        'country_id': countryId,
      };
}

class ModelGroupByOutputTypeSumArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelGroupByOutputTypeSumArgs({this.select});

  final _i2.ModelSumAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class ModelMinAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelMinAggregateOutputTypeSelect({
    this.id,
    this.name,
    this.markId,
    this.countryId,
  });

  final bool? id;

  final bool? name;

  final bool? markId;

  final bool? countryId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'mark_id': markId,
        'country_id': countryId,
      };
}

class ModelGroupByOutputTypeMinArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelGroupByOutputTypeMinArgs({this.select});

  final _i2.ModelMinAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class ModelMaxAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelMaxAggregateOutputTypeSelect({
    this.id,
    this.name,
    this.markId,
    this.countryId,
  });

  final bool? id;

  final bool? name;

  final bool? markId;

  final bool? countryId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'mark_id': markId,
        'country_id': countryId,
      };
}

class ModelGroupByOutputTypeMaxArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelGroupByOutputTypeMaxArgs({this.select});

  final _i2.ModelMaxAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class ModelGroupByOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelGroupByOutputTypeSelect({
    this.id,
    this.name,
    this.markId,
    this.countryId,
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  final bool? id;

  final bool? name;

  final bool? markId;

  final bool? countryId;

  final _i1.PrismaUnion<bool, _i2.ModelGroupByOutputTypeCountArgs>? $count;

  final _i1.PrismaUnion<bool, _i2.ModelGroupByOutputTypeAvgArgs>? $avg;

  final _i1.PrismaUnion<bool, _i2.ModelGroupByOutputTypeSumArgs>? $sum;

  final _i1.PrismaUnion<bool, _i2.ModelGroupByOutputTypeMinArgs>? $min;

  final _i1.PrismaUnion<bool, _i2.ModelGroupByOutputTypeMaxArgs>? $max;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'mark_id': markId,
        'country_id': countryId,
        '_count': $count,
        '_avg': $avg,
        '_sum': $sum,
        '_min': $min,
        '_max': $max,
      };
}

class AggregateModel {
  const AggregateModel({
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  factory AggregateModel.fromJson(Map json) => AggregateModel(
        $count: json['_count'] is Map
            ? _i2.ModelCountAggregateOutputType.fromJson(json['_count'])
            : null,
        $avg: json['_avg'] is Map
            ? _i2.ModelAvgAggregateOutputType.fromJson(json['_avg'])
            : null,
        $sum: json['_sum'] is Map
            ? _i2.ModelSumAggregateOutputType.fromJson(json['_sum'])
            : null,
        $min: json['_min'] is Map
            ? _i2.ModelMinAggregateOutputType.fromJson(json['_min'])
            : null,
        $max: json['_max'] is Map
            ? _i2.ModelMaxAggregateOutputType.fromJson(json['_max'])
            : null,
      );

  final _i2.ModelCountAggregateOutputType? $count;

  final _i2.ModelAvgAggregateOutputType? $avg;

  final _i2.ModelSumAggregateOutputType? $sum;

  final _i2.ModelMinAggregateOutputType? $min;

  final _i2.ModelMaxAggregateOutputType? $max;

  Map<String, dynamic> toJson() => {
        '_count': $count?.toJson(),
        '_avg': $avg?.toJson(),
        '_sum': $sum?.toJson(),
        '_min': $min?.toJson(),
        '_max': $max?.toJson(),
      };
}

class AggregateModelCountArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateModelCountArgs({this.select});

  final _i2.ModelCountAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateModelAvgArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateModelAvgArgs({this.select});

  final _i2.ModelAvgAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateModelSumArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateModelSumArgs({this.select});

  final _i2.ModelSumAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateModelMinArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateModelMinArgs({this.select});

  final _i2.ModelMinAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateModelMaxArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateModelMaxArgs({this.select});

  final _i2.ModelMaxAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateModelSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateModelSelect({
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  final _i1.PrismaUnion<bool, _i2.AggregateModelCountArgs>? $count;

  final _i1.PrismaUnion<bool, _i2.AggregateModelAvgArgs>? $avg;

  final _i1.PrismaUnion<bool, _i2.AggregateModelSumArgs>? $sum;

  final _i1.PrismaUnion<bool, _i2.AggregateModelMinArgs>? $min;

  final _i1.PrismaUnion<bool, _i2.AggregateModelMaxArgs>? $max;

  @override
  Map<String, dynamic> toJson() => {
        '_count': $count,
        '_avg': $avg,
        '_sum': $sum,
        '_min': $min,
        '_max': $max,
      };
}

enum EngineScalar<T> implements _i1.PrismaEnum, _i1.Reference<T> {
  id<int>('id', 'Engine'),
  name$<String>('name', 'Engine'),
  power<int>('power', 'Engine');

  const EngineScalar(
    this.name,
    this.model,
  );

  @override
  final String name;

  @override
  final String model;
}

class ModelCreateWithoutCarInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelCreateWithoutCarInput({
    required this.name,
    required this.mark,
    required this.country,
  });

  final String name;

  final _i2.MarkCreateNestedOneWithoutModelInput mark;

  final _i2.CountryCreateNestedOneWithoutModelInput country;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'mark': mark,
        'country': country,
      };
}

class ModelUncheckedCreateWithoutCarInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelUncheckedCreateWithoutCarInput({
    this.id,
    required this.name,
    required this.markId,
    required this.countryId,
  });

  final int? id;

  final String name;

  final int markId;

  final int countryId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'mark_id': markId,
        'country_id': countryId,
      };
}

class ModelCreateOrConnectWithoutCarInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelCreateOrConnectWithoutCarInput({
    required this.where,
    required this.create,
  });

  final _i2.ModelWhereUniqueInput where;

  final _i1.PrismaUnion<_i2.ModelCreateWithoutCarInput,
      _i2.ModelUncheckedCreateWithoutCarInput> create;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'create': create,
      };
}

class ModelCreateNestedOneWithoutCarInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelCreateNestedOneWithoutCarInput({
    this.create,
    this.connectOrCreate,
    this.connect,
  });

  final _i1.PrismaUnion<_i2.ModelCreateWithoutCarInput,
      _i2.ModelUncheckedCreateWithoutCarInput>? create;

  final _i2.ModelCreateOrConnectWithoutCarInput? connectOrCreate;

  final _i2.ModelWhereUniqueInput? connect;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'connect': connect,
      };
}

class CarCreateWithoutEngineInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarCreateWithoutEngineInput({
    required this.vin,
    required this.range,
    required this.createdDate,
    required this.voluem,
    required this.model,
    this.user,
  });

  final String vin;

  final double range;

  final DateTime createdDate;

  final int voluem;

  final _i2.ModelCreateNestedOneWithoutCarInput model;

  final _i2.UserCreateNestedManyWithoutCarInput? user;

  @override
  Map<String, dynamic> toJson() => {
        'vin': vin,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'model': model,
        'User': user,
      };
}

class CarUncheckedCreateWithoutEngineInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarUncheckedCreateWithoutEngineInput({
    this.id,
    required this.vin,
    required this.modelId,
    required this.range,
    required this.createdDate,
    required this.voluem,
    this.user,
  });

  final int? id;

  final String vin;

  final int modelId;

  final double range;

  final DateTime createdDate;

  final int voluem;

  final _i2.UserUncheckedCreateNestedManyWithoutCarInput? user;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'vin': vin,
        'model_id': modelId,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'User': user,
      };
}

class CarCreateOrConnectWithoutEngineInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarCreateOrConnectWithoutEngineInput({
    required this.where,
    required this.create,
  });

  final _i2.CarWhereUniqueInput where;

  final _i1.PrismaUnion<_i2.CarCreateWithoutEngineInput,
      _i2.CarUncheckedCreateWithoutEngineInput> create;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'create': create,
      };
}

class CarCreateManyEngineInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarCreateManyEngineInput({
    this.id,
    required this.vin,
    required this.modelId,
    required this.range,
    required this.createdDate,
    required this.voluem,
  });

  final int? id;

  final String vin;

  final int modelId;

  final double range;

  final DateTime createdDate;

  final int voluem;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'vin': vin,
        'model_id': modelId,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
      };
}

class CarCreateManyEngineInputEnvelope
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarCreateManyEngineInputEnvelope({
    required this.data,
    this.skipDuplicates,
  });

  final _i1.PrismaUnion<_i2.CarCreateManyEngineInput,
      Iterable<_i2.CarCreateManyEngineInput>> data;

  final bool? skipDuplicates;

  @override
  Map<String, dynamic> toJson() => {
        'data': data,
        'skipDuplicates': skipDuplicates,
      };
}

class CarCreateNestedManyWithoutEngineInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarCreateNestedManyWithoutEngineInput({
    this.create,
    this.connectOrCreate,
    this.createMany,
    this.connect,
  });

  final _i1.PrismaUnion<
      _i2.CarCreateWithoutEngineInput,
      _i1.PrismaUnion<
          Iterable<_i2.CarCreateWithoutEngineInput>,
          _i1.PrismaUnion<_i2.CarUncheckedCreateWithoutEngineInput,
              Iterable<_i2.CarUncheckedCreateWithoutEngineInput>>>>? create;

  final _i1.PrismaUnion<_i2.CarCreateOrConnectWithoutEngineInput,
      Iterable<_i2.CarCreateOrConnectWithoutEngineInput>>? connectOrCreate;

  final _i2.CarCreateManyEngineInputEnvelope? createMany;

  final _i1
      .PrismaUnion<_i2.CarWhereUniqueInput, Iterable<_i2.CarWhereUniqueInput>>?
      connect;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'createMany': createMany,
        'connect': connect,
      };
}

class EngineCreateInput implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineCreateInput({
    required this.name,
    required this.power,
    this.car,
  });

  final String name;

  final int power;

  final _i2.CarCreateNestedManyWithoutEngineInput? car;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'power': power,
        'Car': car,
      };
}

class CarUncheckedCreateNestedManyWithoutEngineInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarUncheckedCreateNestedManyWithoutEngineInput({
    this.create,
    this.connectOrCreate,
    this.createMany,
    this.connect,
  });

  final _i1.PrismaUnion<
      _i2.CarCreateWithoutEngineInput,
      _i1.PrismaUnion<
          Iterable<_i2.CarCreateWithoutEngineInput>,
          _i1.PrismaUnion<_i2.CarUncheckedCreateWithoutEngineInput,
              Iterable<_i2.CarUncheckedCreateWithoutEngineInput>>>>? create;

  final _i1.PrismaUnion<_i2.CarCreateOrConnectWithoutEngineInput,
      Iterable<_i2.CarCreateOrConnectWithoutEngineInput>>? connectOrCreate;

  final _i2.CarCreateManyEngineInputEnvelope? createMany;

  final _i1
      .PrismaUnion<_i2.CarWhereUniqueInput, Iterable<_i2.CarWhereUniqueInput>>?
      connect;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'createMany': createMany,
        'connect': connect,
      };
}

class EngineUncheckedCreateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineUncheckedCreateInput({
    this.id,
    required this.name,
    required this.power,
    this.car,
  });

  final int? id;

  final String name;

  final int power;

  final _i2.CarUncheckedCreateNestedManyWithoutEngineInput? car;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'power': power,
        'Car': car,
      };
}

class EngineCreateManyInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineCreateManyInput({
    this.id,
    required this.name,
    required this.power,
  });

  final int? id;

  final String name;

  final int power;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'power': power,
      };
}

class ModelUpdateWithoutCarInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelUpdateWithoutCarInput({
    this.name,
    this.mark,
    this.country,
  });

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i2.MarkUpdateOneRequiredWithoutModelNestedInput? mark;

  final _i2.CountryUpdateOneRequiredWithoutModelNestedInput? country;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'mark': mark,
        'country': country,
      };
}

class ModelUncheckedUpdateWithoutCarInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelUncheckedUpdateWithoutCarInput({
    this.id,
    this.name,
    this.markId,
    this.countryId,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? markId;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? countryId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'mark_id': markId,
        'country_id': countryId,
      };
}

class ModelUpsertWithoutCarInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelUpsertWithoutCarInput({
    required this.update,
    required this.create,
  });

  final _i1.PrismaUnion<_i2.ModelUpdateWithoutCarInput,
      _i2.ModelUncheckedUpdateWithoutCarInput> update;

  final _i1.PrismaUnion<_i2.ModelCreateWithoutCarInput,
      _i2.ModelUncheckedCreateWithoutCarInput> create;

  @override
  Map<String, dynamic> toJson() => {
        'update': update,
        'create': create,
      };
}

class ModelUpdateOneRequiredWithoutCarNestedInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ModelUpdateOneRequiredWithoutCarNestedInput({
    this.create,
    this.connectOrCreate,
    this.upsert,
    this.connect,
    this.update,
  });

  final _i1.PrismaUnion<_i2.ModelCreateWithoutCarInput,
      _i2.ModelUncheckedCreateWithoutCarInput>? create;

  final _i2.ModelCreateOrConnectWithoutCarInput? connectOrCreate;

  final _i2.ModelUpsertWithoutCarInput? upsert;

  final _i2.ModelWhereUniqueInput? connect;

  final _i1.PrismaUnion<_i2.ModelUpdateWithoutCarInput,
      _i2.ModelUncheckedUpdateWithoutCarInput>? update;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'upsert': upsert,
        'connect': connect,
        'update': update,
      };
}

class CarUpdateWithoutEngineInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarUpdateWithoutEngineInput({
    this.vin,
    this.range,
    this.createdDate,
    this.voluem,
    this.model,
    this.user,
  });

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? vin;

  final _i1.PrismaUnion<double, _i2.FloatFieldUpdateOperationsInput>? range;

  final _i1.PrismaUnion<DateTime, _i2.DateTimeFieldUpdateOperationsInput>?
      createdDate;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? voluem;

  final _i2.ModelUpdateOneRequiredWithoutCarNestedInput? model;

  final _i2.UserUpdateManyWithoutCarNestedInput? user;

  @override
  Map<String, dynamic> toJson() => {
        'vin': vin,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'model': model,
        'User': user,
      };
}

class CarUncheckedUpdateWithoutEngineInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarUncheckedUpdateWithoutEngineInput({
    this.id,
    this.vin,
    this.modelId,
    this.range,
    this.createdDate,
    this.voluem,
    this.user,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? vin;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? modelId;

  final _i1.PrismaUnion<double, _i2.FloatFieldUpdateOperationsInput>? range;

  final _i1.PrismaUnion<DateTime, _i2.DateTimeFieldUpdateOperationsInput>?
      createdDate;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? voluem;

  final _i2.UserUncheckedUpdateManyWithoutCarNestedInput? user;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'vin': vin,
        'model_id': modelId,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'User': user,
      };
}

class CarUpsertWithWhereUniqueWithoutEngineInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarUpsertWithWhereUniqueWithoutEngineInput({
    required this.where,
    required this.update,
    required this.create,
  });

  final _i2.CarWhereUniqueInput where;

  final _i1.PrismaUnion<_i2.CarUpdateWithoutEngineInput,
      _i2.CarUncheckedUpdateWithoutEngineInput> update;

  final _i1.PrismaUnion<_i2.CarCreateWithoutEngineInput,
      _i2.CarUncheckedCreateWithoutEngineInput> create;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'update': update,
        'create': create,
      };
}

class CarUpdateWithWhereUniqueWithoutEngineInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarUpdateWithWhereUniqueWithoutEngineInput({
    required this.where,
    required this.data,
  });

  final _i2.CarWhereUniqueInput where;

  final _i1.PrismaUnion<_i2.CarUpdateWithoutEngineInput,
      _i2.CarUncheckedUpdateWithoutEngineInput> data;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'data': data,
      };
}

class CarUpdateManyWithWhereWithoutEngineInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarUpdateManyWithWhereWithoutEngineInput({
    required this.where,
    required this.data,
  });

  final _i2.CarScalarWhereInput where;

  final _i1.PrismaUnion<_i2.CarUpdateManyMutationInput,
      _i2.CarUncheckedUpdateManyWithoutCarInput> data;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'data': data,
      };
}

class CarUpdateManyWithoutEngineNestedInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarUpdateManyWithoutEngineNestedInput({
    this.create,
    this.connectOrCreate,
    this.upsert,
    this.createMany,
    this.set,
    this.disconnect,
    this.delete,
    this.connect,
    this.update,
    this.updateMany,
    this.deleteMany,
  });

  final _i1.PrismaUnion<
      _i2.CarCreateWithoutEngineInput,
      _i1.PrismaUnion<
          Iterable<_i2.CarCreateWithoutEngineInput>,
          _i1.PrismaUnion<_i2.CarUncheckedCreateWithoutEngineInput,
              Iterable<_i2.CarUncheckedCreateWithoutEngineInput>>>>? create;

  final _i1.PrismaUnion<_i2.CarCreateOrConnectWithoutEngineInput,
      Iterable<_i2.CarCreateOrConnectWithoutEngineInput>>? connectOrCreate;

  final _i1.PrismaUnion<_i2.CarUpsertWithWhereUniqueWithoutEngineInput,
      Iterable<_i2.CarUpsertWithWhereUniqueWithoutEngineInput>>? upsert;

  final _i2.CarCreateManyEngineInputEnvelope? createMany;

  final _i1
      .PrismaUnion<_i2.CarWhereUniqueInput, Iterable<_i2.CarWhereUniqueInput>>?
      set;

  final _i1
      .PrismaUnion<_i2.CarWhereUniqueInput, Iterable<_i2.CarWhereUniqueInput>>?
      disconnect;

  final _i1
      .PrismaUnion<_i2.CarWhereUniqueInput, Iterable<_i2.CarWhereUniqueInput>>?
      delete;

  final _i1
      .PrismaUnion<_i2.CarWhereUniqueInput, Iterable<_i2.CarWhereUniqueInput>>?
      connect;

  final _i1.PrismaUnion<_i2.CarUpdateWithWhereUniqueWithoutEngineInput,
      Iterable<_i2.CarUpdateWithWhereUniqueWithoutEngineInput>>? update;

  final _i1.PrismaUnion<_i2.CarUpdateManyWithWhereWithoutEngineInput,
      Iterable<_i2.CarUpdateManyWithWhereWithoutEngineInput>>? updateMany;

  final _i1
      .PrismaUnion<_i2.CarScalarWhereInput, Iterable<_i2.CarScalarWhereInput>>?
      deleteMany;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'upsert': upsert,
        'createMany': createMany,
        'set': set,
        'disconnect': disconnect,
        'delete': delete,
        'connect': connect,
        'update': update,
        'updateMany': updateMany,
        'deleteMany': deleteMany,
      };
}

class EngineUpdateInput implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineUpdateInput({
    this.name,
    this.power,
    this.car,
  });

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? power;

  final _i2.CarUpdateManyWithoutEngineNestedInput? car;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'power': power,
        'Car': car,
      };
}

class CarUncheckedUpdateManyWithoutEngineNestedInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarUncheckedUpdateManyWithoutEngineNestedInput({
    this.create,
    this.connectOrCreate,
    this.upsert,
    this.createMany,
    this.set,
    this.disconnect,
    this.delete,
    this.connect,
    this.update,
    this.updateMany,
    this.deleteMany,
  });

  final _i1.PrismaUnion<
      _i2.CarCreateWithoutEngineInput,
      _i1.PrismaUnion<
          Iterable<_i2.CarCreateWithoutEngineInput>,
          _i1.PrismaUnion<_i2.CarUncheckedCreateWithoutEngineInput,
              Iterable<_i2.CarUncheckedCreateWithoutEngineInput>>>>? create;

  final _i1.PrismaUnion<_i2.CarCreateOrConnectWithoutEngineInput,
      Iterable<_i2.CarCreateOrConnectWithoutEngineInput>>? connectOrCreate;

  final _i1.PrismaUnion<_i2.CarUpsertWithWhereUniqueWithoutEngineInput,
      Iterable<_i2.CarUpsertWithWhereUniqueWithoutEngineInput>>? upsert;

  final _i2.CarCreateManyEngineInputEnvelope? createMany;

  final _i1
      .PrismaUnion<_i2.CarWhereUniqueInput, Iterable<_i2.CarWhereUniqueInput>>?
      set;

  final _i1
      .PrismaUnion<_i2.CarWhereUniqueInput, Iterable<_i2.CarWhereUniqueInput>>?
      disconnect;

  final _i1
      .PrismaUnion<_i2.CarWhereUniqueInput, Iterable<_i2.CarWhereUniqueInput>>?
      delete;

  final _i1
      .PrismaUnion<_i2.CarWhereUniqueInput, Iterable<_i2.CarWhereUniqueInput>>?
      connect;

  final _i1.PrismaUnion<_i2.CarUpdateWithWhereUniqueWithoutEngineInput,
      Iterable<_i2.CarUpdateWithWhereUniqueWithoutEngineInput>>? update;

  final _i1.PrismaUnion<_i2.CarUpdateManyWithWhereWithoutEngineInput,
      Iterable<_i2.CarUpdateManyWithWhereWithoutEngineInput>>? updateMany;

  final _i1
      .PrismaUnion<_i2.CarScalarWhereInput, Iterable<_i2.CarScalarWhereInput>>?
      deleteMany;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'upsert': upsert,
        'createMany': createMany,
        'set': set,
        'disconnect': disconnect,
        'delete': delete,
        'connect': connect,
        'update': update,
        'updateMany': updateMany,
        'deleteMany': deleteMany,
      };
}

class EngineUncheckedUpdateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineUncheckedUpdateInput({
    this.id,
    this.name,
    this.power,
    this.car,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? power;

  final _i2.CarUncheckedUpdateManyWithoutEngineNestedInput? car;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'power': power,
        'Car': car,
      };
}

class EngineUpdateManyMutationInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineUpdateManyMutationInput({
    this.name,
    this.power,
  });

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? power;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'power': power,
      };
}

class EngineUncheckedUpdateManyInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineUncheckedUpdateManyInput({
    this.id,
    this.name,
    this.power,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? power;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'power': power,
      };
}

class EngineCountAggregateOutputType {
  const EngineCountAggregateOutputType({
    this.id,
    this.name,
    this.power,
    this.$all,
  });

  factory EngineCountAggregateOutputType.fromJson(Map json) =>
      EngineCountAggregateOutputType(
        id: json['id'],
        name: json['name'],
        power: json['power'],
        $all: json['_all'],
      );

  final int? id;

  final int? name;

  final int? power;

  final int? $all;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'power': power,
        '_all': $all,
      };
}

class EngineAvgAggregateOutputType {
  const EngineAvgAggregateOutputType({
    this.id,
    this.power,
  });

  factory EngineAvgAggregateOutputType.fromJson(Map json) =>
      EngineAvgAggregateOutputType(
        id: json['id'],
        power: json['power'],
      );

  final double? id;

  final double? power;

  Map<String, dynamic> toJson() => {
        'id': id,
        'power': power,
      };
}

class EngineSumAggregateOutputType {
  const EngineSumAggregateOutputType({
    this.id,
    this.power,
  });

  factory EngineSumAggregateOutputType.fromJson(Map json) =>
      EngineSumAggregateOutputType(
        id: json['id'],
        power: json['power'],
      );

  final int? id;

  final int? power;

  Map<String, dynamic> toJson() => {
        'id': id,
        'power': power,
      };
}

class EngineMinAggregateOutputType {
  const EngineMinAggregateOutputType({
    this.id,
    this.name,
    this.power,
  });

  factory EngineMinAggregateOutputType.fromJson(Map json) =>
      EngineMinAggregateOutputType(
        id: json['id'],
        name: json['name'],
        power: json['power'],
      );

  final int? id;

  final String? name;

  final int? power;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'power': power,
      };
}

class EngineMaxAggregateOutputType {
  const EngineMaxAggregateOutputType({
    this.id,
    this.name,
    this.power,
  });

  factory EngineMaxAggregateOutputType.fromJson(Map json) =>
      EngineMaxAggregateOutputType(
        id: json['id'],
        name: json['name'],
        power: json['power'],
      );

  final int? id;

  final String? name;

  final int? power;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'power': power,
      };
}

class EngineGroupByOutputType {
  const EngineGroupByOutputType({
    this.id,
    this.name,
    this.power,
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  factory EngineGroupByOutputType.fromJson(Map json) => EngineGroupByOutputType(
        id: json['id'],
        name: json['name'],
        power: json['power'],
        $count: json['_count'] is Map
            ? _i2.EngineCountAggregateOutputType.fromJson(json['_count'])
            : null,
        $avg: json['_avg'] is Map
            ? _i2.EngineAvgAggregateOutputType.fromJson(json['_avg'])
            : null,
        $sum: json['_sum'] is Map
            ? _i2.EngineSumAggregateOutputType.fromJson(json['_sum'])
            : null,
        $min: json['_min'] is Map
            ? _i2.EngineMinAggregateOutputType.fromJson(json['_min'])
            : null,
        $max: json['_max'] is Map
            ? _i2.EngineMaxAggregateOutputType.fromJson(json['_max'])
            : null,
      );

  final int? id;

  final String? name;

  final int? power;

  final _i2.EngineCountAggregateOutputType? $count;

  final _i2.EngineAvgAggregateOutputType? $avg;

  final _i2.EngineSumAggregateOutputType? $sum;

  final _i2.EngineMinAggregateOutputType? $min;

  final _i2.EngineMaxAggregateOutputType? $max;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'power': power,
        '_count': $count?.toJson(),
        '_avg': $avg?.toJson(),
        '_sum': $sum?.toJson(),
        '_min': $min?.toJson(),
        '_max': $max?.toJson(),
      };
}

class EngineCountOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineCountOrderByAggregateInput({
    this.id,
    this.name,
    this.power,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  final _i2.SortOrder? power;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'power': power,
      };
}

class EngineAvgOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineAvgOrderByAggregateInput({
    this.id,
    this.power,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? power;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'power': power,
      };
}

class EngineMaxOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineMaxOrderByAggregateInput({
    this.id,
    this.name,
    this.power,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  final _i2.SortOrder? power;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'power': power,
      };
}

class EngineMinOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineMinOrderByAggregateInput({
    this.id,
    this.name,
    this.power,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  final _i2.SortOrder? power;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'power': power,
      };
}

class EngineSumOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineSumOrderByAggregateInput({
    this.id,
    this.power,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? power;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'power': power,
      };
}

class EngineOrderByWithAggregationInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineOrderByWithAggregationInput({
    this.id,
    this.name,
    this.power,
    this.$count,
    this.$avg,
    this.$max,
    this.$min,
    this.$sum,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  final _i2.SortOrder? power;

  final _i2.EngineCountOrderByAggregateInput? $count;

  final _i2.EngineAvgOrderByAggregateInput? $avg;

  final _i2.EngineMaxOrderByAggregateInput? $max;

  final _i2.EngineMinOrderByAggregateInput? $min;

  final _i2.EngineSumOrderByAggregateInput? $sum;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'power': power,
        '_count': $count,
        '_avg': $avg,
        '_max': $max,
        '_min': $min,
        '_sum': $sum,
      };
}

class EngineScalarWhereWithAggregatesInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineScalarWhereWithAggregatesInput({
    this.AND,
    this.OR,
    this.NOT,
    this.id,
    this.name,
    this.power,
  });

  final _i1.PrismaUnion<_i2.EngineScalarWhereWithAggregatesInput,
      Iterable<_i2.EngineScalarWhereWithAggregatesInput>>? AND;

  final Iterable<_i2.EngineScalarWhereWithAggregatesInput>? OR;

  final _i1.PrismaUnion<_i2.EngineScalarWhereWithAggregatesInput,
      Iterable<_i2.EngineScalarWhereWithAggregatesInput>>? NOT;

  final _i1.PrismaUnion<_i2.IntWithAggregatesFilter, int>? id;

  final _i1.PrismaUnion<_i2.StringWithAggregatesFilter, String>? name;

  final _i1.PrismaUnion<_i2.IntWithAggregatesFilter, int>? power;

  @override
  Map<String, dynamic> toJson() => {
        'AND': AND,
        'OR': OR,
        'NOT': NOT,
        'id': id,
        'name': name,
        'power': power,
      };
}

class EngineCountAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineCountAggregateOutputTypeSelect({
    this.id,
    this.name,
    this.power,
    this.$all,
  });

  final bool? id;

  final bool? name;

  final bool? power;

  final bool? $all;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'power': power,
        '_all': $all,
      };
}

class EngineGroupByOutputTypeCountArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineGroupByOutputTypeCountArgs({this.select});

  final _i2.EngineCountAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class EngineAvgAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineAvgAggregateOutputTypeSelect({
    this.id,
    this.power,
  });

  final bool? id;

  final bool? power;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'power': power,
      };
}

class EngineGroupByOutputTypeAvgArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineGroupByOutputTypeAvgArgs({this.select});

  final _i2.EngineAvgAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class EngineSumAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineSumAggregateOutputTypeSelect({
    this.id,
    this.power,
  });

  final bool? id;

  final bool? power;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'power': power,
      };
}

class EngineGroupByOutputTypeSumArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineGroupByOutputTypeSumArgs({this.select});

  final _i2.EngineSumAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class EngineMinAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineMinAggregateOutputTypeSelect({
    this.id,
    this.name,
    this.power,
  });

  final bool? id;

  final bool? name;

  final bool? power;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'power': power,
      };
}

class EngineGroupByOutputTypeMinArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineGroupByOutputTypeMinArgs({this.select});

  final _i2.EngineMinAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class EngineMaxAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineMaxAggregateOutputTypeSelect({
    this.id,
    this.name,
    this.power,
  });

  final bool? id;

  final bool? name;

  final bool? power;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'power': power,
      };
}

class EngineGroupByOutputTypeMaxArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineGroupByOutputTypeMaxArgs({this.select});

  final _i2.EngineMaxAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class EngineGroupByOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const EngineGroupByOutputTypeSelect({
    this.id,
    this.name,
    this.power,
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  final bool? id;

  final bool? name;

  final bool? power;

  final _i1.PrismaUnion<bool, _i2.EngineGroupByOutputTypeCountArgs>? $count;

  final _i1.PrismaUnion<bool, _i2.EngineGroupByOutputTypeAvgArgs>? $avg;

  final _i1.PrismaUnion<bool, _i2.EngineGroupByOutputTypeSumArgs>? $sum;

  final _i1.PrismaUnion<bool, _i2.EngineGroupByOutputTypeMinArgs>? $min;

  final _i1.PrismaUnion<bool, _i2.EngineGroupByOutputTypeMaxArgs>? $max;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'power': power,
        '_count': $count,
        '_avg': $avg,
        '_sum': $sum,
        '_min': $min,
        '_max': $max,
      };
}

class AggregateEngine {
  const AggregateEngine({
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  factory AggregateEngine.fromJson(Map json) => AggregateEngine(
        $count: json['_count'] is Map
            ? _i2.EngineCountAggregateOutputType.fromJson(json['_count'])
            : null,
        $avg: json['_avg'] is Map
            ? _i2.EngineAvgAggregateOutputType.fromJson(json['_avg'])
            : null,
        $sum: json['_sum'] is Map
            ? _i2.EngineSumAggregateOutputType.fromJson(json['_sum'])
            : null,
        $min: json['_min'] is Map
            ? _i2.EngineMinAggregateOutputType.fromJson(json['_min'])
            : null,
        $max: json['_max'] is Map
            ? _i2.EngineMaxAggregateOutputType.fromJson(json['_max'])
            : null,
      );

  final _i2.EngineCountAggregateOutputType? $count;

  final _i2.EngineAvgAggregateOutputType? $avg;

  final _i2.EngineSumAggregateOutputType? $sum;

  final _i2.EngineMinAggregateOutputType? $min;

  final _i2.EngineMaxAggregateOutputType? $max;

  Map<String, dynamic> toJson() => {
        '_count': $count?.toJson(),
        '_avg': $avg?.toJson(),
        '_sum': $sum?.toJson(),
        '_min': $min?.toJson(),
        '_max': $max?.toJson(),
      };
}

class AggregateEngineCountArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateEngineCountArgs({this.select});

  final _i2.EngineCountAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateEngineAvgArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateEngineAvgArgs({this.select});

  final _i2.EngineAvgAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateEngineSumArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateEngineSumArgs({this.select});

  final _i2.EngineSumAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateEngineMinArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateEngineMinArgs({this.select});

  final _i2.EngineMinAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateEngineMaxArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateEngineMaxArgs({this.select});

  final _i2.EngineMaxAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateEngineSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateEngineSelect({
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  final _i1.PrismaUnion<bool, _i2.AggregateEngineCountArgs>? $count;

  final _i1.PrismaUnion<bool, _i2.AggregateEngineAvgArgs>? $avg;

  final _i1.PrismaUnion<bool, _i2.AggregateEngineSumArgs>? $sum;

  final _i1.PrismaUnion<bool, _i2.AggregateEngineMinArgs>? $min;

  final _i1.PrismaUnion<bool, _i2.AggregateEngineMaxArgs>? $max;

  @override
  Map<String, dynamic> toJson() => {
        '_count': $count,
        '_avg': $avg,
        '_sum': $sum,
        '_min': $min,
        '_max': $max,
      };
}

class CarCreateInput implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarCreateInput({
    required this.vin,
    required this.range,
    required this.createdDate,
    required this.voluem,
    required this.model,
    required this.engine,
    this.user,
  });

  final String vin;

  final double range;

  final DateTime createdDate;

  final int voluem;

  final _i2.ModelCreateNestedOneWithoutCarInput model;

  final _i2.EngineCreateNestedOneWithoutCarInput engine;

  final _i2.UserCreateNestedManyWithoutCarInput? user;

  @override
  Map<String, dynamic> toJson() => {
        'vin': vin,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'model': model,
        'engine': engine,
        'User': user,
      };
}

class CarUncheckedCreateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarUncheckedCreateInput({
    this.id,
    required this.vin,
    required this.modelId,
    required this.range,
    required this.createdDate,
    required this.voluem,
    required this.engineId,
    this.user,
  });

  final int? id;

  final String vin;

  final int modelId;

  final double range;

  final DateTime createdDate;

  final int voluem;

  final int engineId;

  final _i2.UserUncheckedCreateNestedManyWithoutCarInput? user;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'vin': vin,
        'model_id': modelId,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'engine_id': engineId,
        'User': user,
      };
}

class CarCreateManyInput implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarCreateManyInput({
    this.id,
    required this.vin,
    required this.modelId,
    required this.range,
    required this.createdDate,
    required this.voluem,
    required this.engineId,
  });

  final int? id;

  final String vin;

  final int modelId;

  final double range;

  final DateTime createdDate;

  final int voluem;

  final int engineId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'vin': vin,
        'model_id': modelId,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'engine_id': engineId,
      };
}

class CarUpdateInput implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarUpdateInput({
    this.vin,
    this.range,
    this.createdDate,
    this.voluem,
    this.model,
    this.engine,
    this.user,
  });

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? vin;

  final _i1.PrismaUnion<double, _i2.FloatFieldUpdateOperationsInput>? range;

  final _i1.PrismaUnion<DateTime, _i2.DateTimeFieldUpdateOperationsInput>?
      createdDate;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? voluem;

  final _i2.ModelUpdateOneRequiredWithoutCarNestedInput? model;

  final _i2.EngineUpdateOneRequiredWithoutCarNestedInput? engine;

  final _i2.UserUpdateManyWithoutCarNestedInput? user;

  @override
  Map<String, dynamic> toJson() => {
        'vin': vin,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'model': model,
        'engine': engine,
        'User': user,
      };
}

class CarUncheckedUpdateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarUncheckedUpdateInput({
    this.id,
    this.vin,
    this.modelId,
    this.range,
    this.createdDate,
    this.voluem,
    this.engineId,
    this.user,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? vin;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? modelId;

  final _i1.PrismaUnion<double, _i2.FloatFieldUpdateOperationsInput>? range;

  final _i1.PrismaUnion<DateTime, _i2.DateTimeFieldUpdateOperationsInput>?
      createdDate;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? voluem;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? engineId;

  final _i2.UserUncheckedUpdateManyWithoutCarNestedInput? user;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'vin': vin,
        'model_id': modelId,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'engine_id': engineId,
        'User': user,
      };
}

class CarUncheckedUpdateManyInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarUncheckedUpdateManyInput({
    this.id,
    this.vin,
    this.modelId,
    this.range,
    this.createdDate,
    this.voluem,
    this.engineId,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? vin;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? modelId;

  final _i1.PrismaUnion<double, _i2.FloatFieldUpdateOperationsInput>? range;

  final _i1.PrismaUnion<DateTime, _i2.DateTimeFieldUpdateOperationsInput>?
      createdDate;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? voluem;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? engineId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'vin': vin,
        'model_id': modelId,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'engine_id': engineId,
      };
}

class CarCountAggregateOutputType {
  const CarCountAggregateOutputType({
    this.id,
    this.vin,
    this.modelId,
    this.range,
    this.createdDate,
    this.voluem,
    this.engineId,
    this.$all,
  });

  factory CarCountAggregateOutputType.fromJson(Map json) =>
      CarCountAggregateOutputType(
        id: json['id'],
        vin: json['vin'],
        modelId: json['model_id'],
        range: json['range'],
        createdDate: json['created_date'],
        voluem: json['voluem'],
        engineId: json['engine_id'],
        $all: json['_all'],
      );

  final int? id;

  final int? vin;

  final int? modelId;

  final int? range;

  final int? createdDate;

  final int? voluem;

  final int? engineId;

  final int? $all;

  Map<String, dynamic> toJson() => {
        'id': id,
        'vin': vin,
        'model_id': modelId,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'engine_id': engineId,
        '_all': $all,
      };
}

class CarAvgAggregateOutputType {
  const CarAvgAggregateOutputType({
    this.id,
    this.modelId,
    this.range,
    this.voluem,
    this.engineId,
  });

  factory CarAvgAggregateOutputType.fromJson(Map json) =>
      CarAvgAggregateOutputType(
        id: json['id'],
        modelId: json['model_id'],
        range: json['range'],
        voluem: json['voluem'],
        engineId: json['engine_id'],
      );

  final double? id;

  final double? modelId;

  final double? range;

  final double? voluem;

  final double? engineId;

  Map<String, dynamic> toJson() => {
        'id': id,
        'model_id': modelId,
        'range': range,
        'voluem': voluem,
        'engine_id': engineId,
      };
}

class CarSumAggregateOutputType {
  const CarSumAggregateOutputType({
    this.id,
    this.modelId,
    this.range,
    this.voluem,
    this.engineId,
  });

  factory CarSumAggregateOutputType.fromJson(Map json) =>
      CarSumAggregateOutputType(
        id: json['id'],
        modelId: json['model_id'],
        range: json['range'],
        voluem: json['voluem'],
        engineId: json['engine_id'],
      );

  final int? id;

  final int? modelId;

  final double? range;

  final int? voluem;

  final int? engineId;

  Map<String, dynamic> toJson() => {
        'id': id,
        'model_id': modelId,
        'range': range,
        'voluem': voluem,
        'engine_id': engineId,
      };
}

class CarMinAggregateOutputType {
  const CarMinAggregateOutputType({
    this.id,
    this.vin,
    this.modelId,
    this.range,
    this.createdDate,
    this.voluem,
    this.engineId,
  });

  factory CarMinAggregateOutputType.fromJson(Map json) =>
      CarMinAggregateOutputType(
        id: json['id'],
        vin: json['vin'],
        modelId: json['model_id'],
        range: json['range'],
        createdDate: json['created_date'],
        voluem: json['voluem'],
        engineId: json['engine_id'],
      );

  final int? id;

  final String? vin;

  final int? modelId;

  final double? range;

  final DateTime? createdDate;

  final int? voluem;

  final int? engineId;

  Map<String, dynamic> toJson() => {
        'id': id,
        'vin': vin,
        'model_id': modelId,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'engine_id': engineId,
      };
}

class CarMaxAggregateOutputType {
  const CarMaxAggregateOutputType({
    this.id,
    this.vin,
    this.modelId,
    this.range,
    this.createdDate,
    this.voluem,
    this.engineId,
  });

  factory CarMaxAggregateOutputType.fromJson(Map json) =>
      CarMaxAggregateOutputType(
        id: json['id'],
        vin: json['vin'],
        modelId: json['model_id'],
        range: json['range'],
        createdDate: json['created_date'],
        voluem: json['voluem'],
        engineId: json['engine_id'],
      );

  final int? id;

  final String? vin;

  final int? modelId;

  final double? range;

  final DateTime? createdDate;

  final int? voluem;

  final int? engineId;

  Map<String, dynamic> toJson() => {
        'id': id,
        'vin': vin,
        'model_id': modelId,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'engine_id': engineId,
      };
}

class CarGroupByOutputType {
  const CarGroupByOutputType({
    this.id,
    this.vin,
    this.modelId,
    this.range,
    this.createdDate,
    this.voluem,
    this.engineId,
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  factory CarGroupByOutputType.fromJson(Map json) => CarGroupByOutputType(
        id: json['id'],
        vin: json['vin'],
        modelId: json['model_id'],
        range: json['range'],
        createdDate: json['created_date'],
        voluem: json['voluem'],
        engineId: json['engine_id'],
        $count: json['_count'] is Map
            ? _i2.CarCountAggregateOutputType.fromJson(json['_count'])
            : null,
        $avg: json['_avg'] is Map
            ? _i2.CarAvgAggregateOutputType.fromJson(json['_avg'])
            : null,
        $sum: json['_sum'] is Map
            ? _i2.CarSumAggregateOutputType.fromJson(json['_sum'])
            : null,
        $min: json['_min'] is Map
            ? _i2.CarMinAggregateOutputType.fromJson(json['_min'])
            : null,
        $max: json['_max'] is Map
            ? _i2.CarMaxAggregateOutputType.fromJson(json['_max'])
            : null,
      );

  final int? id;

  final String? vin;

  final int? modelId;

  final double? range;

  final DateTime? createdDate;

  final int? voluem;

  final int? engineId;

  final _i2.CarCountAggregateOutputType? $count;

  final _i2.CarAvgAggregateOutputType? $avg;

  final _i2.CarSumAggregateOutputType? $sum;

  final _i2.CarMinAggregateOutputType? $min;

  final _i2.CarMaxAggregateOutputType? $max;

  Map<String, dynamic> toJson() => {
        'id': id,
        'vin': vin,
        'model_id': modelId,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'engine_id': engineId,
        '_count': $count?.toJson(),
        '_avg': $avg?.toJson(),
        '_sum': $sum?.toJson(),
        '_min': $min?.toJson(),
        '_max': $max?.toJson(),
      };
}

class CarCountOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarCountOrderByAggregateInput({
    this.id,
    this.vin,
    this.modelId,
    this.range,
    this.createdDate,
    this.voluem,
    this.engineId,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? vin;

  final _i2.SortOrder? modelId;

  final _i2.SortOrder? range;

  final _i2.SortOrder? createdDate;

  final _i2.SortOrder? voluem;

  final _i2.SortOrder? engineId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'vin': vin,
        'model_id': modelId,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'engine_id': engineId,
      };
}

class CarAvgOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarAvgOrderByAggregateInput({
    this.id,
    this.modelId,
    this.range,
    this.voluem,
    this.engineId,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? modelId;

  final _i2.SortOrder? range;

  final _i2.SortOrder? voluem;

  final _i2.SortOrder? engineId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'model_id': modelId,
        'range': range,
        'voluem': voluem,
        'engine_id': engineId,
      };
}

class CarMaxOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarMaxOrderByAggregateInput({
    this.id,
    this.vin,
    this.modelId,
    this.range,
    this.createdDate,
    this.voluem,
    this.engineId,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? vin;

  final _i2.SortOrder? modelId;

  final _i2.SortOrder? range;

  final _i2.SortOrder? createdDate;

  final _i2.SortOrder? voluem;

  final _i2.SortOrder? engineId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'vin': vin,
        'model_id': modelId,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'engine_id': engineId,
      };
}

class CarMinOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarMinOrderByAggregateInput({
    this.id,
    this.vin,
    this.modelId,
    this.range,
    this.createdDate,
    this.voluem,
    this.engineId,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? vin;

  final _i2.SortOrder? modelId;

  final _i2.SortOrder? range;

  final _i2.SortOrder? createdDate;

  final _i2.SortOrder? voluem;

  final _i2.SortOrder? engineId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'vin': vin,
        'model_id': modelId,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'engine_id': engineId,
      };
}

class CarSumOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarSumOrderByAggregateInput({
    this.id,
    this.modelId,
    this.range,
    this.voluem,
    this.engineId,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? modelId;

  final _i2.SortOrder? range;

  final _i2.SortOrder? voluem;

  final _i2.SortOrder? engineId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'model_id': modelId,
        'range': range,
        'voluem': voluem,
        'engine_id': engineId,
      };
}

class CarOrderByWithAggregationInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarOrderByWithAggregationInput({
    this.id,
    this.vin,
    this.modelId,
    this.range,
    this.createdDate,
    this.voluem,
    this.engineId,
    this.$count,
    this.$avg,
    this.$max,
    this.$min,
    this.$sum,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? vin;

  final _i2.SortOrder? modelId;

  final _i2.SortOrder? range;

  final _i2.SortOrder? createdDate;

  final _i2.SortOrder? voluem;

  final _i2.SortOrder? engineId;

  final _i2.CarCountOrderByAggregateInput? $count;

  final _i2.CarAvgOrderByAggregateInput? $avg;

  final _i2.CarMaxOrderByAggregateInput? $max;

  final _i2.CarMinOrderByAggregateInput? $min;

  final _i2.CarSumOrderByAggregateInput? $sum;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'vin': vin,
        'model_id': modelId,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'engine_id': engineId,
        '_count': $count,
        '_avg': $avg,
        '_max': $max,
        '_min': $min,
        '_sum': $sum,
      };
}

class NestedFloatWithAggregatesFilter
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const NestedFloatWithAggregatesFilter({
    this.equals,
    this.$in,
    this.notIn,
    this.lt,
    this.lte,
    this.gt,
    this.gte,
    this.not,
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  final double? equals;

  final _i1.PrismaUnion<Iterable<double>, double>? $in;

  final _i1.PrismaUnion<Iterable<double>, double>? notIn;

  final double? lt;

  final double? lte;

  final double? gt;

  final double? gte;

  final _i1.PrismaUnion<double, _i2.NestedFloatWithAggregatesFilter>? not;

  final _i2.NestedIntFilter? $count;

  final _i2.NestedFloatFilter? $avg;

  final _i2.NestedFloatFilter? $sum;

  final _i2.NestedFloatFilter? $min;

  final _i2.NestedFloatFilter? $max;

  @override
  Map<String, dynamic> toJson() => {
        'equals': equals,
        'in': $in,
        'notIn': notIn,
        'lt': lt,
        'lte': lte,
        'gt': gt,
        'gte': gte,
        'not': not,
        '_count': $count,
        '_avg': $avg,
        '_sum': $sum,
        '_min': $min,
        '_max': $max,
      };
}

class FloatWithAggregatesFilter
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FloatWithAggregatesFilter({
    this.equals,
    this.$in,
    this.notIn,
    this.lt,
    this.lte,
    this.gt,
    this.gte,
    this.not,
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  final double? equals;

  final _i1.PrismaUnion<Iterable<double>, double>? $in;

  final _i1.PrismaUnion<Iterable<double>, double>? notIn;

  final double? lt;

  final double? lte;

  final double? gt;

  final double? gte;

  final _i1.PrismaUnion<double, _i2.NestedFloatWithAggregatesFilter>? not;

  final _i2.NestedIntFilter? $count;

  final _i2.NestedFloatFilter? $avg;

  final _i2.NestedFloatFilter? $sum;

  final _i2.NestedFloatFilter? $min;

  final _i2.NestedFloatFilter? $max;

  @override
  Map<String, dynamic> toJson() => {
        'equals': equals,
        'in': $in,
        'notIn': notIn,
        'lt': lt,
        'lte': lte,
        'gt': gt,
        'gte': gte,
        'not': not,
        '_count': $count,
        '_avg': $avg,
        '_sum': $sum,
        '_min': $min,
        '_max': $max,
      };
}

class NestedDateTimeWithAggregatesFilter
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const NestedDateTimeWithAggregatesFilter({
    this.equals,
    this.$in,
    this.notIn,
    this.lt,
    this.lte,
    this.gt,
    this.gte,
    this.not,
    this.$count,
    this.$min,
    this.$max,
  });

  final DateTime? equals;

  final _i1.PrismaUnion<Iterable<DateTime>, DateTime>? $in;

  final _i1.PrismaUnion<Iterable<DateTime>, DateTime>? notIn;

  final DateTime? lt;

  final DateTime? lte;

  final DateTime? gt;

  final DateTime? gte;

  final _i1.PrismaUnion<DateTime, _i2.NestedDateTimeWithAggregatesFilter>? not;

  final _i2.NestedIntFilter? $count;

  final _i2.NestedDateTimeFilter? $min;

  final _i2.NestedDateTimeFilter? $max;

  @override
  Map<String, dynamic> toJson() => {
        'equals': equals,
        'in': $in,
        'notIn': notIn,
        'lt': lt,
        'lte': lte,
        'gt': gt,
        'gte': gte,
        'not': not,
        '_count': $count,
        '_min': $min,
        '_max': $max,
      };
}

class DateTimeWithAggregatesFilter
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const DateTimeWithAggregatesFilter({
    this.equals,
    this.$in,
    this.notIn,
    this.lt,
    this.lte,
    this.gt,
    this.gte,
    this.not,
    this.$count,
    this.$min,
    this.$max,
  });

  final DateTime? equals;

  final _i1.PrismaUnion<Iterable<DateTime>, DateTime>? $in;

  final _i1.PrismaUnion<Iterable<DateTime>, DateTime>? notIn;

  final DateTime? lt;

  final DateTime? lte;

  final DateTime? gt;

  final DateTime? gte;

  final _i1.PrismaUnion<DateTime, _i2.NestedDateTimeWithAggregatesFilter>? not;

  final _i2.NestedIntFilter? $count;

  final _i2.NestedDateTimeFilter? $min;

  final _i2.NestedDateTimeFilter? $max;

  @override
  Map<String, dynamic> toJson() => {
        'equals': equals,
        'in': $in,
        'notIn': notIn,
        'lt': lt,
        'lte': lte,
        'gt': gt,
        'gte': gte,
        'not': not,
        '_count': $count,
        '_min': $min,
        '_max': $max,
      };
}

class CarScalarWhereWithAggregatesInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarScalarWhereWithAggregatesInput({
    this.AND,
    this.OR,
    this.NOT,
    this.id,
    this.vin,
    this.modelId,
    this.range,
    this.createdDate,
    this.voluem,
    this.engineId,
  });

  final _i1.PrismaUnion<_i2.CarScalarWhereWithAggregatesInput,
      Iterable<_i2.CarScalarWhereWithAggregatesInput>>? AND;

  final Iterable<_i2.CarScalarWhereWithAggregatesInput>? OR;

  final _i1.PrismaUnion<_i2.CarScalarWhereWithAggregatesInput,
      Iterable<_i2.CarScalarWhereWithAggregatesInput>>? NOT;

  final _i1.PrismaUnion<_i2.IntWithAggregatesFilter, int>? id;

  final _i1.PrismaUnion<_i2.StringWithAggregatesFilter, String>? vin;

  final _i1.PrismaUnion<_i2.IntWithAggregatesFilter, int>? modelId;

  final _i1.PrismaUnion<_i2.FloatWithAggregatesFilter, double>? range;

  final _i1.PrismaUnion<_i2.DateTimeWithAggregatesFilter, DateTime>?
      createdDate;

  final _i1.PrismaUnion<_i2.IntWithAggregatesFilter, int>? voluem;

  final _i1.PrismaUnion<_i2.IntWithAggregatesFilter, int>? engineId;

  @override
  Map<String, dynamic> toJson() => {
        'AND': AND,
        'OR': OR,
        'NOT': NOT,
        'id': id,
        'vin': vin,
        'model_id': modelId,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'engine_id': engineId,
      };
}

class CarCountAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarCountAggregateOutputTypeSelect({
    this.id,
    this.vin,
    this.modelId,
    this.range,
    this.createdDate,
    this.voluem,
    this.engineId,
    this.$all,
  });

  final bool? id;

  final bool? vin;

  final bool? modelId;

  final bool? range;

  final bool? createdDate;

  final bool? voluem;

  final bool? engineId;

  final bool? $all;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'vin': vin,
        'model_id': modelId,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'engine_id': engineId,
        '_all': $all,
      };
}

class CarGroupByOutputTypeCountArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarGroupByOutputTypeCountArgs({this.select});

  final _i2.CarCountAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class CarAvgAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarAvgAggregateOutputTypeSelect({
    this.id,
    this.modelId,
    this.range,
    this.voluem,
    this.engineId,
  });

  final bool? id;

  final bool? modelId;

  final bool? range;

  final bool? voluem;

  final bool? engineId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'model_id': modelId,
        'range': range,
        'voluem': voluem,
        'engine_id': engineId,
      };
}

class CarGroupByOutputTypeAvgArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarGroupByOutputTypeAvgArgs({this.select});

  final _i2.CarAvgAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class CarSumAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarSumAggregateOutputTypeSelect({
    this.id,
    this.modelId,
    this.range,
    this.voluem,
    this.engineId,
  });

  final bool? id;

  final bool? modelId;

  final bool? range;

  final bool? voluem;

  final bool? engineId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'model_id': modelId,
        'range': range,
        'voluem': voluem,
        'engine_id': engineId,
      };
}

class CarGroupByOutputTypeSumArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarGroupByOutputTypeSumArgs({this.select});

  final _i2.CarSumAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class CarMinAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarMinAggregateOutputTypeSelect({
    this.id,
    this.vin,
    this.modelId,
    this.range,
    this.createdDate,
    this.voluem,
    this.engineId,
  });

  final bool? id;

  final bool? vin;

  final bool? modelId;

  final bool? range;

  final bool? createdDate;

  final bool? voluem;

  final bool? engineId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'vin': vin,
        'model_id': modelId,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'engine_id': engineId,
      };
}

class CarGroupByOutputTypeMinArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarGroupByOutputTypeMinArgs({this.select});

  final _i2.CarMinAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class CarMaxAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarMaxAggregateOutputTypeSelect({
    this.id,
    this.vin,
    this.modelId,
    this.range,
    this.createdDate,
    this.voluem,
    this.engineId,
  });

  final bool? id;

  final bool? vin;

  final bool? modelId;

  final bool? range;

  final bool? createdDate;

  final bool? voluem;

  final bool? engineId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'vin': vin,
        'model_id': modelId,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'engine_id': engineId,
      };
}

class CarGroupByOutputTypeMaxArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarGroupByOutputTypeMaxArgs({this.select});

  final _i2.CarMaxAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class CarGroupByOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarGroupByOutputTypeSelect({
    this.id,
    this.vin,
    this.modelId,
    this.range,
    this.createdDate,
    this.voluem,
    this.engineId,
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  final bool? id;

  final bool? vin;

  final bool? modelId;

  final bool? range;

  final bool? createdDate;

  final bool? voluem;

  final bool? engineId;

  final _i1.PrismaUnion<bool, _i2.CarGroupByOutputTypeCountArgs>? $count;

  final _i1.PrismaUnion<bool, _i2.CarGroupByOutputTypeAvgArgs>? $avg;

  final _i1.PrismaUnion<bool, _i2.CarGroupByOutputTypeSumArgs>? $sum;

  final _i1.PrismaUnion<bool, _i2.CarGroupByOutputTypeMinArgs>? $min;

  final _i1.PrismaUnion<bool, _i2.CarGroupByOutputTypeMaxArgs>? $max;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'vin': vin,
        'model_id': modelId,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'engine_id': engineId,
        '_count': $count,
        '_avg': $avg,
        '_sum': $sum,
        '_min': $min,
        '_max': $max,
      };
}

class AggregateCar {
  const AggregateCar({
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  factory AggregateCar.fromJson(Map json) => AggregateCar(
        $count: json['_count'] is Map
            ? _i2.CarCountAggregateOutputType.fromJson(json['_count'])
            : null,
        $avg: json['_avg'] is Map
            ? _i2.CarAvgAggregateOutputType.fromJson(json['_avg'])
            : null,
        $sum: json['_sum'] is Map
            ? _i2.CarSumAggregateOutputType.fromJson(json['_sum'])
            : null,
        $min: json['_min'] is Map
            ? _i2.CarMinAggregateOutputType.fromJson(json['_min'])
            : null,
        $max: json['_max'] is Map
            ? _i2.CarMaxAggregateOutputType.fromJson(json['_max'])
            : null,
      );

  final _i2.CarCountAggregateOutputType? $count;

  final _i2.CarAvgAggregateOutputType? $avg;

  final _i2.CarSumAggregateOutputType? $sum;

  final _i2.CarMinAggregateOutputType? $min;

  final _i2.CarMaxAggregateOutputType? $max;

  Map<String, dynamic> toJson() => {
        '_count': $count?.toJson(),
        '_avg': $avg?.toJson(),
        '_sum': $sum?.toJson(),
        '_min': $min?.toJson(),
        '_max': $max?.toJson(),
      };
}

class AggregateCarCountArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateCarCountArgs({this.select});

  final _i2.CarCountAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateCarAvgArgs implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateCarAvgArgs({this.select});

  final _i2.CarAvgAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateCarSumArgs implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateCarSumArgs({this.select});

  final _i2.CarSumAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateCarMinArgs implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateCarMinArgs({this.select});

  final _i2.CarMinAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateCarMaxArgs implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateCarMaxArgs({this.select});

  final _i2.CarMaxAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateCarSelect implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateCarSelect({
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  final _i1.PrismaUnion<bool, _i2.AggregateCarCountArgs>? $count;

  final _i1.PrismaUnion<bool, _i2.AggregateCarAvgArgs>? $avg;

  final _i1.PrismaUnion<bool, _i2.AggregateCarSumArgs>? $sum;

  final _i1.PrismaUnion<bool, _i2.AggregateCarMinArgs>? $min;

  final _i1.PrismaUnion<bool, _i2.AggregateCarMaxArgs>? $max;

  @override
  Map<String, dynamic> toJson() => {
        '_count': $count,
        '_avg': $avg,
        '_sum': $sum,
        '_min': $min,
        '_max': $max,
      };
}

class CarCreateWithoutUserInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarCreateWithoutUserInput({
    required this.vin,
    required this.range,
    required this.createdDate,
    required this.voluem,
    required this.model,
    required this.engine,
  });

  final String vin;

  final double range;

  final DateTime createdDate;

  final int voluem;

  final _i2.ModelCreateNestedOneWithoutCarInput model;

  final _i2.EngineCreateNestedOneWithoutCarInput engine;

  @override
  Map<String, dynamic> toJson() => {
        'vin': vin,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'model': model,
        'engine': engine,
      };
}

class CarUncheckedCreateWithoutUserInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarUncheckedCreateWithoutUserInput({
    this.id,
    required this.vin,
    required this.modelId,
    required this.range,
    required this.createdDate,
    required this.voluem,
    required this.engineId,
  });

  final int? id;

  final String vin;

  final int modelId;

  final double range;

  final DateTime createdDate;

  final int voluem;

  final int engineId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'vin': vin,
        'model_id': modelId,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'engine_id': engineId,
      };
}

class CarCreateOrConnectWithoutUserInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarCreateOrConnectWithoutUserInput({
    required this.where,
    required this.create,
  });

  final _i2.CarWhereUniqueInput where;

  final _i1.PrismaUnion<_i2.CarCreateWithoutUserInput,
      _i2.CarUncheckedCreateWithoutUserInput> create;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'create': create,
      };
}

class CarCreateNestedOneWithoutUserInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarCreateNestedOneWithoutUserInput({
    this.create,
    this.connectOrCreate,
    this.connect,
  });

  final _i1.PrismaUnion<_i2.CarCreateWithoutUserInput,
      _i2.CarUncheckedCreateWithoutUserInput>? create;

  final _i2.CarCreateOrConnectWithoutUserInput? connectOrCreate;

  final _i2.CarWhereUniqueInput? connect;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'connect': connect,
      };
}

class UserCreateInput implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserCreateInput({
    required this.name,
    required this.phone,
    required this.birthday,
    required this.car,
    this.expensive,
  });

  final String name;

  final String phone;

  final DateTime birthday;

  final _i2.CarCreateNestedOneWithoutUserInput car;

  final _i2.ExpensiveCreateNestedManyWithoutUserInput? expensive;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'phone': phone,
        'birthday': birthday,
        'car': car,
        'Expensive': expensive,
      };
}

class UserUncheckedCreateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserUncheckedCreateInput({
    this.id,
    required this.name,
    required this.phone,
    required this.birthday,
    required this.carId,
    this.expensive,
  });

  final int? id;

  final String name;

  final String phone;

  final DateTime birthday;

  final int carId;

  final _i2.ExpensiveUncheckedCreateNestedManyWithoutUserInput? expensive;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'phone': phone,
        'birthday': birthday,
        'car_id': carId,
        'Expensive': expensive,
      };
}

class UserCreateManyInput implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserCreateManyInput({
    this.id,
    required this.name,
    required this.phone,
    required this.birthday,
    required this.carId,
  });

  final int? id;

  final String name;

  final String phone;

  final DateTime birthday;

  final int carId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'phone': phone,
        'birthday': birthday,
        'car_id': carId,
      };
}

class CarUpdateWithoutUserInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarUpdateWithoutUserInput({
    this.vin,
    this.range,
    this.createdDate,
    this.voluem,
    this.model,
    this.engine,
  });

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? vin;

  final _i1.PrismaUnion<double, _i2.FloatFieldUpdateOperationsInput>? range;

  final _i1.PrismaUnion<DateTime, _i2.DateTimeFieldUpdateOperationsInput>?
      createdDate;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? voluem;

  final _i2.ModelUpdateOneRequiredWithoutCarNestedInput? model;

  final _i2.EngineUpdateOneRequiredWithoutCarNestedInput? engine;

  @override
  Map<String, dynamic> toJson() => {
        'vin': vin,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'model': model,
        'engine': engine,
      };
}

class CarUncheckedUpdateWithoutUserInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarUncheckedUpdateWithoutUserInput({
    this.id,
    this.vin,
    this.modelId,
    this.range,
    this.createdDate,
    this.voluem,
    this.engineId,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? vin;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? modelId;

  final _i1.PrismaUnion<double, _i2.FloatFieldUpdateOperationsInput>? range;

  final _i1.PrismaUnion<DateTime, _i2.DateTimeFieldUpdateOperationsInput>?
      createdDate;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? voluem;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? engineId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'vin': vin,
        'model_id': modelId,
        'range': range,
        'created_date': createdDate,
        'voluem': voluem,
        'engine_id': engineId,
      };
}

class CarUpsertWithoutUserInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarUpsertWithoutUserInput({
    required this.update,
    required this.create,
  });

  final _i1.PrismaUnion<_i2.CarUpdateWithoutUserInput,
      _i2.CarUncheckedUpdateWithoutUserInput> update;

  final _i1.PrismaUnion<_i2.CarCreateWithoutUserInput,
      _i2.CarUncheckedCreateWithoutUserInput> create;

  @override
  Map<String, dynamic> toJson() => {
        'update': update,
        'create': create,
      };
}

class CarUpdateOneRequiredWithoutUserNestedInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CarUpdateOneRequiredWithoutUserNestedInput({
    this.create,
    this.connectOrCreate,
    this.upsert,
    this.connect,
    this.update,
  });

  final _i1.PrismaUnion<_i2.CarCreateWithoutUserInput,
      _i2.CarUncheckedCreateWithoutUserInput>? create;

  final _i2.CarCreateOrConnectWithoutUserInput? connectOrCreate;

  final _i2.CarUpsertWithoutUserInput? upsert;

  final _i2.CarWhereUniqueInput? connect;

  final _i1.PrismaUnion<_i2.CarUpdateWithoutUserInput,
      _i2.CarUncheckedUpdateWithoutUserInput>? update;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'upsert': upsert,
        'connect': connect,
        'update': update,
      };
}

class UserUpdateInput implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserUpdateInput({
    this.name,
    this.phone,
    this.birthday,
    this.car,
    this.expensive,
  });

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? phone;

  final _i1.PrismaUnion<DateTime, _i2.DateTimeFieldUpdateOperationsInput>?
      birthday;

  final _i2.CarUpdateOneRequiredWithoutUserNestedInput? car;

  final _i2.ExpensiveUpdateManyWithoutUserNestedInput? expensive;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'phone': phone,
        'birthday': birthday,
        'car': car,
        'Expensive': expensive,
      };
}

class UserUncheckedUpdateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserUncheckedUpdateInput({
    this.id,
    this.name,
    this.phone,
    this.birthday,
    this.carId,
    this.expensive,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? phone;

  final _i1.PrismaUnion<DateTime, _i2.DateTimeFieldUpdateOperationsInput>?
      birthday;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? carId;

  final _i2.ExpensiveUncheckedUpdateManyWithoutUserNestedInput? expensive;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'phone': phone,
        'birthday': birthday,
        'car_id': carId,
        'Expensive': expensive,
      };
}

class UserUncheckedUpdateManyInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserUncheckedUpdateManyInput({
    this.id,
    this.name,
    this.phone,
    this.birthday,
    this.carId,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? phone;

  final _i1.PrismaUnion<DateTime, _i2.DateTimeFieldUpdateOperationsInput>?
      birthday;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? carId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'phone': phone,
        'birthday': birthday,
        'car_id': carId,
      };
}

class UserCountAggregateOutputType {
  const UserCountAggregateOutputType({
    this.id,
    this.name,
    this.phone,
    this.birthday,
    this.carId,
    this.$all,
  });

  factory UserCountAggregateOutputType.fromJson(Map json) =>
      UserCountAggregateOutputType(
        id: json['id'],
        name: json['name'],
        phone: json['phone'],
        birthday: json['birthday'],
        carId: json['car_id'],
        $all: json['_all'],
      );

  final int? id;

  final int? name;

  final int? phone;

  final int? birthday;

  final int? carId;

  final int? $all;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'phone': phone,
        'birthday': birthday,
        'car_id': carId,
        '_all': $all,
      };
}

class UserAvgAggregateOutputType {
  const UserAvgAggregateOutputType({
    this.id,
    this.carId,
  });

  factory UserAvgAggregateOutputType.fromJson(Map json) =>
      UserAvgAggregateOutputType(
        id: json['id'],
        carId: json['car_id'],
      );

  final double? id;

  final double? carId;

  Map<String, dynamic> toJson() => {
        'id': id,
        'car_id': carId,
      };
}

class UserSumAggregateOutputType {
  const UserSumAggregateOutputType({
    this.id,
    this.carId,
  });

  factory UserSumAggregateOutputType.fromJson(Map json) =>
      UserSumAggregateOutputType(
        id: json['id'],
        carId: json['car_id'],
      );

  final int? id;

  final int? carId;

  Map<String, dynamic> toJson() => {
        'id': id,
        'car_id': carId,
      };
}

class UserMinAggregateOutputType {
  const UserMinAggregateOutputType({
    this.id,
    this.name,
    this.phone,
    this.birthday,
    this.carId,
  });

  factory UserMinAggregateOutputType.fromJson(Map json) =>
      UserMinAggregateOutputType(
        id: json['id'],
        name: json['name'],
        phone: json['phone'],
        birthday: json['birthday'],
        carId: json['car_id'],
      );

  final int? id;

  final String? name;

  final String? phone;

  final DateTime? birthday;

  final int? carId;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'phone': phone,
        'birthday': birthday,
        'car_id': carId,
      };
}

class UserMaxAggregateOutputType {
  const UserMaxAggregateOutputType({
    this.id,
    this.name,
    this.phone,
    this.birthday,
    this.carId,
  });

  factory UserMaxAggregateOutputType.fromJson(Map json) =>
      UserMaxAggregateOutputType(
        id: json['id'],
        name: json['name'],
        phone: json['phone'],
        birthday: json['birthday'],
        carId: json['car_id'],
      );

  final int? id;

  final String? name;

  final String? phone;

  final DateTime? birthday;

  final int? carId;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'phone': phone,
        'birthday': birthday,
        'car_id': carId,
      };
}

class UserGroupByOutputType {
  const UserGroupByOutputType({
    this.id,
    this.name,
    this.phone,
    this.birthday,
    this.carId,
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  factory UserGroupByOutputType.fromJson(Map json) => UserGroupByOutputType(
        id: json['id'],
        name: json['name'],
        phone: json['phone'],
        birthday: json['birthday'],
        carId: json['car_id'],
        $count: json['_count'] is Map
            ? _i2.UserCountAggregateOutputType.fromJson(json['_count'])
            : null,
        $avg: json['_avg'] is Map
            ? _i2.UserAvgAggregateOutputType.fromJson(json['_avg'])
            : null,
        $sum: json['_sum'] is Map
            ? _i2.UserSumAggregateOutputType.fromJson(json['_sum'])
            : null,
        $min: json['_min'] is Map
            ? _i2.UserMinAggregateOutputType.fromJson(json['_min'])
            : null,
        $max: json['_max'] is Map
            ? _i2.UserMaxAggregateOutputType.fromJson(json['_max'])
            : null,
      );

  final int? id;

  final String? name;

  final String? phone;

  final DateTime? birthday;

  final int? carId;

  final _i2.UserCountAggregateOutputType? $count;

  final _i2.UserAvgAggregateOutputType? $avg;

  final _i2.UserSumAggregateOutputType? $sum;

  final _i2.UserMinAggregateOutputType? $min;

  final _i2.UserMaxAggregateOutputType? $max;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'phone': phone,
        'birthday': birthday,
        'car_id': carId,
        '_count': $count?.toJson(),
        '_avg': $avg?.toJson(),
        '_sum': $sum?.toJson(),
        '_min': $min?.toJson(),
        '_max': $max?.toJson(),
      };
}

class UserCountOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserCountOrderByAggregateInput({
    this.id,
    this.name,
    this.phone,
    this.birthday,
    this.carId,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  final _i2.SortOrder? phone;

  final _i2.SortOrder? birthday;

  final _i2.SortOrder? carId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'phone': phone,
        'birthday': birthday,
        'car_id': carId,
      };
}

class UserAvgOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserAvgOrderByAggregateInput({
    this.id,
    this.carId,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? carId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'car_id': carId,
      };
}

class UserMaxOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserMaxOrderByAggregateInput({
    this.id,
    this.name,
    this.phone,
    this.birthday,
    this.carId,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  final _i2.SortOrder? phone;

  final _i2.SortOrder? birthday;

  final _i2.SortOrder? carId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'phone': phone,
        'birthday': birthday,
        'car_id': carId,
      };
}

class UserMinOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserMinOrderByAggregateInput({
    this.id,
    this.name,
    this.phone,
    this.birthday,
    this.carId,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  final _i2.SortOrder? phone;

  final _i2.SortOrder? birthday;

  final _i2.SortOrder? carId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'phone': phone,
        'birthday': birthday,
        'car_id': carId,
      };
}

class UserSumOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserSumOrderByAggregateInput({
    this.id,
    this.carId,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? carId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'car_id': carId,
      };
}

class UserOrderByWithAggregationInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserOrderByWithAggregationInput({
    this.id,
    this.name,
    this.phone,
    this.birthday,
    this.carId,
    this.$count,
    this.$avg,
    this.$max,
    this.$min,
    this.$sum,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  final _i2.SortOrder? phone;

  final _i2.SortOrder? birthday;

  final _i2.SortOrder? carId;

  final _i2.UserCountOrderByAggregateInput? $count;

  final _i2.UserAvgOrderByAggregateInput? $avg;

  final _i2.UserMaxOrderByAggregateInput? $max;

  final _i2.UserMinOrderByAggregateInput? $min;

  final _i2.UserSumOrderByAggregateInput? $sum;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'phone': phone,
        'birthday': birthday,
        'car_id': carId,
        '_count': $count,
        '_avg': $avg,
        '_max': $max,
        '_min': $min,
        '_sum': $sum,
      };
}

class UserScalarWhereWithAggregatesInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserScalarWhereWithAggregatesInput({
    this.AND,
    this.OR,
    this.NOT,
    this.id,
    this.name,
    this.phone,
    this.birthday,
    this.carId,
  });

  final _i1.PrismaUnion<_i2.UserScalarWhereWithAggregatesInput,
      Iterable<_i2.UserScalarWhereWithAggregatesInput>>? AND;

  final Iterable<_i2.UserScalarWhereWithAggregatesInput>? OR;

  final _i1.PrismaUnion<_i2.UserScalarWhereWithAggregatesInput,
      Iterable<_i2.UserScalarWhereWithAggregatesInput>>? NOT;

  final _i1.PrismaUnion<_i2.IntWithAggregatesFilter, int>? id;

  final _i1.PrismaUnion<_i2.StringWithAggregatesFilter, String>? name;

  final _i1.PrismaUnion<_i2.StringWithAggregatesFilter, String>? phone;

  final _i1.PrismaUnion<_i2.DateTimeWithAggregatesFilter, DateTime>? birthday;

  final _i1.PrismaUnion<_i2.IntWithAggregatesFilter, int>? carId;

  @override
  Map<String, dynamic> toJson() => {
        'AND': AND,
        'OR': OR,
        'NOT': NOT,
        'id': id,
        'name': name,
        'phone': phone,
        'birthday': birthday,
        'car_id': carId,
      };
}

class UserCountAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserCountAggregateOutputTypeSelect({
    this.id,
    this.name,
    this.phone,
    this.birthday,
    this.carId,
    this.$all,
  });

  final bool? id;

  final bool? name;

  final bool? phone;

  final bool? birthday;

  final bool? carId;

  final bool? $all;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'phone': phone,
        'birthday': birthday,
        'car_id': carId,
        '_all': $all,
      };
}

class UserGroupByOutputTypeCountArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserGroupByOutputTypeCountArgs({this.select});

  final _i2.UserCountAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class UserAvgAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserAvgAggregateOutputTypeSelect({
    this.id,
    this.carId,
  });

  final bool? id;

  final bool? carId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'car_id': carId,
      };
}

class UserGroupByOutputTypeAvgArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserGroupByOutputTypeAvgArgs({this.select});

  final _i2.UserAvgAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class UserSumAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserSumAggregateOutputTypeSelect({
    this.id,
    this.carId,
  });

  final bool? id;

  final bool? carId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'car_id': carId,
      };
}

class UserGroupByOutputTypeSumArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserGroupByOutputTypeSumArgs({this.select});

  final _i2.UserSumAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class UserMinAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserMinAggregateOutputTypeSelect({
    this.id,
    this.name,
    this.phone,
    this.birthday,
    this.carId,
  });

  final bool? id;

  final bool? name;

  final bool? phone;

  final bool? birthday;

  final bool? carId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'phone': phone,
        'birthday': birthday,
        'car_id': carId,
      };
}

class UserGroupByOutputTypeMinArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserGroupByOutputTypeMinArgs({this.select});

  final _i2.UserMinAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class UserMaxAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserMaxAggregateOutputTypeSelect({
    this.id,
    this.name,
    this.phone,
    this.birthday,
    this.carId,
  });

  final bool? id;

  final bool? name;

  final bool? phone;

  final bool? birthday;

  final bool? carId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'phone': phone,
        'birthday': birthday,
        'car_id': carId,
      };
}

class UserGroupByOutputTypeMaxArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserGroupByOutputTypeMaxArgs({this.select});

  final _i2.UserMaxAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class UserGroupByOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserGroupByOutputTypeSelect({
    this.id,
    this.name,
    this.phone,
    this.birthday,
    this.carId,
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  final bool? id;

  final bool? name;

  final bool? phone;

  final bool? birthday;

  final bool? carId;

  final _i1.PrismaUnion<bool, _i2.UserGroupByOutputTypeCountArgs>? $count;

  final _i1.PrismaUnion<bool, _i2.UserGroupByOutputTypeAvgArgs>? $avg;

  final _i1.PrismaUnion<bool, _i2.UserGroupByOutputTypeSumArgs>? $sum;

  final _i1.PrismaUnion<bool, _i2.UserGroupByOutputTypeMinArgs>? $min;

  final _i1.PrismaUnion<bool, _i2.UserGroupByOutputTypeMaxArgs>? $max;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'phone': phone,
        'birthday': birthday,
        'car_id': carId,
        '_count': $count,
        '_avg': $avg,
        '_sum': $sum,
        '_min': $min,
        '_max': $max,
      };
}

class AggregateUser {
  const AggregateUser({
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  factory AggregateUser.fromJson(Map json) => AggregateUser(
        $count: json['_count'] is Map
            ? _i2.UserCountAggregateOutputType.fromJson(json['_count'])
            : null,
        $avg: json['_avg'] is Map
            ? _i2.UserAvgAggregateOutputType.fromJson(json['_avg'])
            : null,
        $sum: json['_sum'] is Map
            ? _i2.UserSumAggregateOutputType.fromJson(json['_sum'])
            : null,
        $min: json['_min'] is Map
            ? _i2.UserMinAggregateOutputType.fromJson(json['_min'])
            : null,
        $max: json['_max'] is Map
            ? _i2.UserMaxAggregateOutputType.fromJson(json['_max'])
            : null,
      );

  final _i2.UserCountAggregateOutputType? $count;

  final _i2.UserAvgAggregateOutputType? $avg;

  final _i2.UserSumAggregateOutputType? $sum;

  final _i2.UserMinAggregateOutputType? $min;

  final _i2.UserMaxAggregateOutputType? $max;

  Map<String, dynamic> toJson() => {
        '_count': $count?.toJson(),
        '_avg': $avg?.toJson(),
        '_sum': $sum?.toJson(),
        '_min': $min?.toJson(),
        '_max': $max?.toJson(),
      };
}

class AggregateUserCountArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateUserCountArgs({this.select});

  final _i2.UserCountAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateUserAvgArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateUserAvgArgs({this.select});

  final _i2.UserAvgAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateUserSumArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateUserSumArgs({this.select});

  final _i2.UserSumAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateUserMinArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateUserMinArgs({this.select});

  final _i2.UserMinAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateUserMaxArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateUserMaxArgs({this.select});

  final _i2.UserMaxAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateUserSelect implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateUserSelect({
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  final _i1.PrismaUnion<bool, _i2.AggregateUserCountArgs>? $count;

  final _i1.PrismaUnion<bool, _i2.AggregateUserAvgArgs>? $avg;

  final _i1.PrismaUnion<bool, _i2.AggregateUserSumArgs>? $sum;

  final _i1.PrismaUnion<bool, _i2.AggregateUserMinArgs>? $min;

  final _i1.PrismaUnion<bool, _i2.AggregateUserMaxArgs>? $max;

  @override
  Map<String, dynamic> toJson() => {
        '_count': $count,
        '_avg': $avg,
        '_sum': $sum,
        '_min': $min,
        '_max': $max,
      };
}

enum FuelScalar<T> implements _i1.PrismaEnum, _i1.Reference<T> {
  id<int>('id', 'Fuel'),
  name$<String>('name', 'Fuel');

  const FuelScalar(
    this.name,
    this.model,
  );

  @override
  final String name;

  @override
  final String model;
}

class UserCreateWithoutExpensiveInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserCreateWithoutExpensiveInput({
    required this.name,
    required this.phone,
    required this.birthday,
    required this.car,
  });

  final String name;

  final String phone;

  final DateTime birthday;

  final _i2.CarCreateNestedOneWithoutUserInput car;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'phone': phone,
        'birthday': birthday,
        'car': car,
      };
}

class UserUncheckedCreateWithoutExpensiveInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserUncheckedCreateWithoutExpensiveInput({
    this.id,
    required this.name,
    required this.phone,
    required this.birthday,
    required this.carId,
  });

  final int? id;

  final String name;

  final String phone;

  final DateTime birthday;

  final int carId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'phone': phone,
        'birthday': birthday,
        'car_id': carId,
      };
}

class UserCreateOrConnectWithoutExpensiveInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserCreateOrConnectWithoutExpensiveInput({
    required this.where,
    required this.create,
  });

  final _i2.UserWhereUniqueInput where;

  final _i1.PrismaUnion<_i2.UserCreateWithoutExpensiveInput,
      _i2.UserUncheckedCreateWithoutExpensiveInput> create;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'create': create,
      };
}

class UserCreateNestedOneWithoutExpensiveInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserCreateNestedOneWithoutExpensiveInput({
    this.create,
    this.connectOrCreate,
    this.connect,
  });

  final _i1.PrismaUnion<_i2.UserCreateWithoutExpensiveInput,
      _i2.UserUncheckedCreateWithoutExpensiveInput>? create;

  final _i2.UserCreateOrConnectWithoutExpensiveInput? connectOrCreate;

  final _i2.UserWhereUniqueInput? connect;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'connect': connect,
      };
}

class ExpensiveCreateWithoutFuelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveCreateWithoutFuelInput({
    required this.name,
    required this.description,
    required this.createdDate,
    required this.range,
    required this.price,
    this.liters,
    required this.category,
    required this.user,
  });

  final String name;

  final String description;

  final DateTime createdDate;

  final double range;

  final double price;

  final _i1.PrismaUnion<double, _i1.PrismaNull>? liters;

  final _i2.CategoryCreateNestedOneWithoutExpensiveInput category;

  final _i2.UserCreateNestedOneWithoutExpensiveInput user;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'liters': liters,
        'category': category,
        'User': user,
      };
}

class ExpensiveUncheckedCreateWithoutFuelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveUncheckedCreateWithoutFuelInput({
    this.id,
    required this.name,
    required this.categoryId,
    required this.description,
    required this.createdDate,
    required this.range,
    required this.price,
    required this.userId,
    this.liters,
  });

  final int? id;

  final String name;

  final int categoryId;

  final String description;

  final DateTime createdDate;

  final double range;

  final double price;

  final int userId;

  final _i1.PrismaUnion<double, _i1.PrismaNull>? liters;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'category_id': categoryId,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'user_id': userId,
        'liters': liters,
      };
}

class ExpensiveCreateOrConnectWithoutFuelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveCreateOrConnectWithoutFuelInput({
    required this.where,
    required this.create,
  });

  final _i2.ExpensiveWhereUniqueInput where;

  final _i1.PrismaUnion<_i2.ExpensiveCreateWithoutFuelInput,
      _i2.ExpensiveUncheckedCreateWithoutFuelInput> create;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'create': create,
      };
}

class ExpensiveCreateManyFuelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveCreateManyFuelInput({
    this.id,
    required this.name,
    required this.categoryId,
    required this.description,
    required this.createdDate,
    required this.range,
    required this.price,
    required this.userId,
    this.liters,
  });

  final int? id;

  final String name;

  final int categoryId;

  final String description;

  final DateTime createdDate;

  final double range;

  final double price;

  final int userId;

  final _i1.PrismaUnion<double, _i1.PrismaNull>? liters;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'category_id': categoryId,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'user_id': userId,
        'liters': liters,
      };
}

class ExpensiveCreateManyFuelInputEnvelope
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveCreateManyFuelInputEnvelope({
    required this.data,
    this.skipDuplicates,
  });

  final _i1.PrismaUnion<_i2.ExpensiveCreateManyFuelInput,
      Iterable<_i2.ExpensiveCreateManyFuelInput>> data;

  final bool? skipDuplicates;

  @override
  Map<String, dynamic> toJson() => {
        'data': data,
        'skipDuplicates': skipDuplicates,
      };
}

class ExpensiveCreateNestedManyWithoutFuelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveCreateNestedManyWithoutFuelInput({
    this.create,
    this.connectOrCreate,
    this.createMany,
    this.connect,
  });

  final _i1.PrismaUnion<
      _i2.ExpensiveCreateWithoutFuelInput,
      _i1.PrismaUnion<
          Iterable<_i2.ExpensiveCreateWithoutFuelInput>,
          _i1.PrismaUnion<_i2.ExpensiveUncheckedCreateWithoutFuelInput,
              Iterable<_i2.ExpensiveUncheckedCreateWithoutFuelInput>>>>? create;

  final _i1.PrismaUnion<_i2.ExpensiveCreateOrConnectWithoutFuelInput,
      Iterable<_i2.ExpensiveCreateOrConnectWithoutFuelInput>>? connectOrCreate;

  final _i2.ExpensiveCreateManyFuelInputEnvelope? createMany;

  final _i1.PrismaUnion<_i2.ExpensiveWhereUniqueInput,
      Iterable<_i2.ExpensiveWhereUniqueInput>>? connect;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'createMany': createMany,
        'connect': connect,
      };
}

class FuelCreateInput implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelCreateInput({
    required this.name,
    this.expensive,
  });

  final String name;

  final _i2.ExpensiveCreateNestedManyWithoutFuelInput? expensive;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'Expensive': expensive,
      };
}

class ExpensiveUncheckedCreateNestedManyWithoutFuelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveUncheckedCreateNestedManyWithoutFuelInput({
    this.create,
    this.connectOrCreate,
    this.createMany,
    this.connect,
  });

  final _i1.PrismaUnion<
      _i2.ExpensiveCreateWithoutFuelInput,
      _i1.PrismaUnion<
          Iterable<_i2.ExpensiveCreateWithoutFuelInput>,
          _i1.PrismaUnion<_i2.ExpensiveUncheckedCreateWithoutFuelInput,
              Iterable<_i2.ExpensiveUncheckedCreateWithoutFuelInput>>>>? create;

  final _i1.PrismaUnion<_i2.ExpensiveCreateOrConnectWithoutFuelInput,
      Iterable<_i2.ExpensiveCreateOrConnectWithoutFuelInput>>? connectOrCreate;

  final _i2.ExpensiveCreateManyFuelInputEnvelope? createMany;

  final _i1.PrismaUnion<_i2.ExpensiveWhereUniqueInput,
      Iterable<_i2.ExpensiveWhereUniqueInput>>? connect;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'createMany': createMany,
        'connect': connect,
      };
}

class FuelUncheckedCreateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelUncheckedCreateInput({
    this.id,
    required this.name,
    this.expensive,
  });

  final int? id;

  final String name;

  final _i2.ExpensiveUncheckedCreateNestedManyWithoutFuelInput? expensive;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'Expensive': expensive,
      };
}

class FuelCreateManyInput implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelCreateManyInput({
    this.id,
    required this.name,
  });

  final int? id;

  final String name;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
      };
}

class UserUpdateWithoutExpensiveInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserUpdateWithoutExpensiveInput({
    this.name,
    this.phone,
    this.birthday,
    this.car,
  });

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? phone;

  final _i1.PrismaUnion<DateTime, _i2.DateTimeFieldUpdateOperationsInput>?
      birthday;

  final _i2.CarUpdateOneRequiredWithoutUserNestedInput? car;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'phone': phone,
        'birthday': birthday,
        'car': car,
      };
}

class UserUncheckedUpdateWithoutExpensiveInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserUncheckedUpdateWithoutExpensiveInput({
    this.id,
    this.name,
    this.phone,
    this.birthday,
    this.carId,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? phone;

  final _i1.PrismaUnion<DateTime, _i2.DateTimeFieldUpdateOperationsInput>?
      birthday;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? carId;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'phone': phone,
        'birthday': birthday,
        'car_id': carId,
      };
}

class UserUpsertWithoutExpensiveInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserUpsertWithoutExpensiveInput({
    required this.update,
    required this.create,
  });

  final _i1.PrismaUnion<_i2.UserUpdateWithoutExpensiveInput,
      _i2.UserUncheckedUpdateWithoutExpensiveInput> update;

  final _i1.PrismaUnion<_i2.UserCreateWithoutExpensiveInput,
      _i2.UserUncheckedCreateWithoutExpensiveInput> create;

  @override
  Map<String, dynamic> toJson() => {
        'update': update,
        'create': create,
      };
}

class UserUpdateOneRequiredWithoutExpensiveNestedInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const UserUpdateOneRequiredWithoutExpensiveNestedInput({
    this.create,
    this.connectOrCreate,
    this.upsert,
    this.connect,
    this.update,
  });

  final _i1.PrismaUnion<_i2.UserCreateWithoutExpensiveInput,
      _i2.UserUncheckedCreateWithoutExpensiveInput>? create;

  final _i2.UserCreateOrConnectWithoutExpensiveInput? connectOrCreate;

  final _i2.UserUpsertWithoutExpensiveInput? upsert;

  final _i2.UserWhereUniqueInput? connect;

  final _i1.PrismaUnion<_i2.UserUpdateWithoutExpensiveInput,
      _i2.UserUncheckedUpdateWithoutExpensiveInput>? update;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'upsert': upsert,
        'connect': connect,
        'update': update,
      };
}

class ExpensiveUpdateWithoutFuelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveUpdateWithoutFuelInput({
    this.name,
    this.description,
    this.createdDate,
    this.range,
    this.price,
    this.liters,
    this.category,
    this.user,
  });

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>?
      description;

  final _i1.PrismaUnion<DateTime, _i2.DateTimeFieldUpdateOperationsInput>?
      createdDate;

  final _i1.PrismaUnion<double, _i2.FloatFieldUpdateOperationsInput>? range;

  final _i1.PrismaUnion<double, _i2.FloatFieldUpdateOperationsInput>? price;

  final _i1.PrismaUnion<
      double,
      _i1.PrismaUnion<_i2.NullableFloatFieldUpdateOperationsInput,
          _i1.PrismaNull>>? liters;

  final _i2.CategoryUpdateOneRequiredWithoutExpensiveNestedInput? category;

  final _i2.UserUpdateOneRequiredWithoutExpensiveNestedInput? user;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'liters': liters,
        'category': category,
        'User': user,
      };
}

class ExpensiveUncheckedUpdateWithoutFuelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveUncheckedUpdateWithoutFuelInput({
    this.id,
    this.name,
    this.categoryId,
    this.description,
    this.createdDate,
    this.range,
    this.price,
    this.userId,
    this.liters,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? categoryId;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>?
      description;

  final _i1.PrismaUnion<DateTime, _i2.DateTimeFieldUpdateOperationsInput>?
      createdDate;

  final _i1.PrismaUnion<double, _i2.FloatFieldUpdateOperationsInput>? range;

  final _i1.PrismaUnion<double, _i2.FloatFieldUpdateOperationsInput>? price;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? userId;

  final _i1.PrismaUnion<
      double,
      _i1.PrismaUnion<_i2.NullableFloatFieldUpdateOperationsInput,
          _i1.PrismaNull>>? liters;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'category_id': categoryId,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'user_id': userId,
        'liters': liters,
      };
}

class ExpensiveUpsertWithWhereUniqueWithoutFuelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveUpsertWithWhereUniqueWithoutFuelInput({
    required this.where,
    required this.update,
    required this.create,
  });

  final _i2.ExpensiveWhereUniqueInput where;

  final _i1.PrismaUnion<_i2.ExpensiveUpdateWithoutFuelInput,
      _i2.ExpensiveUncheckedUpdateWithoutFuelInput> update;

  final _i1.PrismaUnion<_i2.ExpensiveCreateWithoutFuelInput,
      _i2.ExpensiveUncheckedCreateWithoutFuelInput> create;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'update': update,
        'create': create,
      };
}

class ExpensiveUpdateWithWhereUniqueWithoutFuelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveUpdateWithWhereUniqueWithoutFuelInput({
    required this.where,
    required this.data,
  });

  final _i2.ExpensiveWhereUniqueInput where;

  final _i1.PrismaUnion<_i2.ExpensiveUpdateWithoutFuelInput,
      _i2.ExpensiveUncheckedUpdateWithoutFuelInput> data;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'data': data,
      };
}

class ExpensiveUpdateManyWithWhereWithoutFuelInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveUpdateManyWithWhereWithoutFuelInput({
    required this.where,
    required this.data,
  });

  final _i2.ExpensiveScalarWhereInput where;

  final _i1.PrismaUnion<_i2.ExpensiveUpdateManyMutationInput,
      _i2.ExpensiveUncheckedUpdateManyWithoutExpensiveInput> data;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'data': data,
      };
}

class ExpensiveUpdateManyWithoutFuelNestedInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveUpdateManyWithoutFuelNestedInput({
    this.create,
    this.connectOrCreate,
    this.upsert,
    this.createMany,
    this.set,
    this.disconnect,
    this.delete,
    this.connect,
    this.update,
    this.updateMany,
    this.deleteMany,
  });

  final _i1.PrismaUnion<
      _i2.ExpensiveCreateWithoutFuelInput,
      _i1.PrismaUnion<
          Iterable<_i2.ExpensiveCreateWithoutFuelInput>,
          _i1.PrismaUnion<_i2.ExpensiveUncheckedCreateWithoutFuelInput,
              Iterable<_i2.ExpensiveUncheckedCreateWithoutFuelInput>>>>? create;

  final _i1.PrismaUnion<_i2.ExpensiveCreateOrConnectWithoutFuelInput,
      Iterable<_i2.ExpensiveCreateOrConnectWithoutFuelInput>>? connectOrCreate;

  final _i1.PrismaUnion<_i2.ExpensiveUpsertWithWhereUniqueWithoutFuelInput,
      Iterable<_i2.ExpensiveUpsertWithWhereUniqueWithoutFuelInput>>? upsert;

  final _i2.ExpensiveCreateManyFuelInputEnvelope? createMany;

  final _i1.PrismaUnion<_i2.ExpensiveWhereUniqueInput,
      Iterable<_i2.ExpensiveWhereUniqueInput>>? set;

  final _i1.PrismaUnion<_i2.ExpensiveWhereUniqueInput,
      Iterable<_i2.ExpensiveWhereUniqueInput>>? disconnect;

  final _i1.PrismaUnion<_i2.ExpensiveWhereUniqueInput,
      Iterable<_i2.ExpensiveWhereUniqueInput>>? delete;

  final _i1.PrismaUnion<_i2.ExpensiveWhereUniqueInput,
      Iterable<_i2.ExpensiveWhereUniqueInput>>? connect;

  final _i1.PrismaUnion<_i2.ExpensiveUpdateWithWhereUniqueWithoutFuelInput,
      Iterable<_i2.ExpensiveUpdateWithWhereUniqueWithoutFuelInput>>? update;

  final _i1.PrismaUnion<_i2.ExpensiveUpdateManyWithWhereWithoutFuelInput,
      Iterable<_i2.ExpensiveUpdateManyWithWhereWithoutFuelInput>>? updateMany;

  final _i1.PrismaUnion<_i2.ExpensiveScalarWhereInput,
      Iterable<_i2.ExpensiveScalarWhereInput>>? deleteMany;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'upsert': upsert,
        'createMany': createMany,
        'set': set,
        'disconnect': disconnect,
        'delete': delete,
        'connect': connect,
        'update': update,
        'updateMany': updateMany,
        'deleteMany': deleteMany,
      };
}

class FuelUpdateInput implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelUpdateInput({
    this.name,
    this.expensive,
  });

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i2.ExpensiveUpdateManyWithoutFuelNestedInput? expensive;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'Expensive': expensive,
      };
}

class ExpensiveUncheckedUpdateManyWithoutFuelNestedInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveUncheckedUpdateManyWithoutFuelNestedInput({
    this.create,
    this.connectOrCreate,
    this.upsert,
    this.createMany,
    this.set,
    this.disconnect,
    this.delete,
    this.connect,
    this.update,
    this.updateMany,
    this.deleteMany,
  });

  final _i1.PrismaUnion<
      _i2.ExpensiveCreateWithoutFuelInput,
      _i1.PrismaUnion<
          Iterable<_i2.ExpensiveCreateWithoutFuelInput>,
          _i1.PrismaUnion<_i2.ExpensiveUncheckedCreateWithoutFuelInput,
              Iterable<_i2.ExpensiveUncheckedCreateWithoutFuelInput>>>>? create;

  final _i1.PrismaUnion<_i2.ExpensiveCreateOrConnectWithoutFuelInput,
      Iterable<_i2.ExpensiveCreateOrConnectWithoutFuelInput>>? connectOrCreate;

  final _i1.PrismaUnion<_i2.ExpensiveUpsertWithWhereUniqueWithoutFuelInput,
      Iterable<_i2.ExpensiveUpsertWithWhereUniqueWithoutFuelInput>>? upsert;

  final _i2.ExpensiveCreateManyFuelInputEnvelope? createMany;

  final _i1.PrismaUnion<_i2.ExpensiveWhereUniqueInput,
      Iterable<_i2.ExpensiveWhereUniqueInput>>? set;

  final _i1.PrismaUnion<_i2.ExpensiveWhereUniqueInput,
      Iterable<_i2.ExpensiveWhereUniqueInput>>? disconnect;

  final _i1.PrismaUnion<_i2.ExpensiveWhereUniqueInput,
      Iterable<_i2.ExpensiveWhereUniqueInput>>? delete;

  final _i1.PrismaUnion<_i2.ExpensiveWhereUniqueInput,
      Iterable<_i2.ExpensiveWhereUniqueInput>>? connect;

  final _i1.PrismaUnion<_i2.ExpensiveUpdateWithWhereUniqueWithoutFuelInput,
      Iterable<_i2.ExpensiveUpdateWithWhereUniqueWithoutFuelInput>>? update;

  final _i1.PrismaUnion<_i2.ExpensiveUpdateManyWithWhereWithoutFuelInput,
      Iterable<_i2.ExpensiveUpdateManyWithWhereWithoutFuelInput>>? updateMany;

  final _i1.PrismaUnion<_i2.ExpensiveScalarWhereInput,
      Iterable<_i2.ExpensiveScalarWhereInput>>? deleteMany;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'upsert': upsert,
        'createMany': createMany,
        'set': set,
        'disconnect': disconnect,
        'delete': delete,
        'connect': connect,
        'update': update,
        'updateMany': updateMany,
        'deleteMany': deleteMany,
      };
}

class FuelUncheckedUpdateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelUncheckedUpdateInput({
    this.id,
    this.name,
    this.expensive,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i2.ExpensiveUncheckedUpdateManyWithoutFuelNestedInput? expensive;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'Expensive': expensive,
      };
}

class FuelUpdateManyMutationInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelUpdateManyMutationInput({this.name});

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  @override
  Map<String, dynamic> toJson() => {'name': name};
}

class FuelUncheckedUpdateManyInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelUncheckedUpdateManyInput({
    this.id,
    this.name,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
      };
}

class FuelCountAggregateOutputType {
  const FuelCountAggregateOutputType({
    this.id,
    this.name,
    this.$all,
  });

  factory FuelCountAggregateOutputType.fromJson(Map json) =>
      FuelCountAggregateOutputType(
        id: json['id'],
        name: json['name'],
        $all: json['_all'],
      );

  final int? id;

  final int? name;

  final int? $all;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        '_all': $all,
      };
}

class FuelAvgAggregateOutputType {
  const FuelAvgAggregateOutputType({this.id});

  factory FuelAvgAggregateOutputType.fromJson(Map json) =>
      FuelAvgAggregateOutputType(id: json['id']);

  final double? id;

  Map<String, dynamic> toJson() => {'id': id};
}

class FuelSumAggregateOutputType {
  const FuelSumAggregateOutputType({this.id});

  factory FuelSumAggregateOutputType.fromJson(Map json) =>
      FuelSumAggregateOutputType(id: json['id']);

  final int? id;

  Map<String, dynamic> toJson() => {'id': id};
}

class FuelMinAggregateOutputType {
  const FuelMinAggregateOutputType({
    this.id,
    this.name,
  });

  factory FuelMinAggregateOutputType.fromJson(Map json) =>
      FuelMinAggregateOutputType(
        id: json['id'],
        name: json['name'],
      );

  final int? id;

  final String? name;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
      };
}

class FuelMaxAggregateOutputType {
  const FuelMaxAggregateOutputType({
    this.id,
    this.name,
  });

  factory FuelMaxAggregateOutputType.fromJson(Map json) =>
      FuelMaxAggregateOutputType(
        id: json['id'],
        name: json['name'],
      );

  final int? id;

  final String? name;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
      };
}

class FuelGroupByOutputType {
  const FuelGroupByOutputType({
    this.id,
    this.name,
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  factory FuelGroupByOutputType.fromJson(Map json) => FuelGroupByOutputType(
        id: json['id'],
        name: json['name'],
        $count: json['_count'] is Map
            ? _i2.FuelCountAggregateOutputType.fromJson(json['_count'])
            : null,
        $avg: json['_avg'] is Map
            ? _i2.FuelAvgAggregateOutputType.fromJson(json['_avg'])
            : null,
        $sum: json['_sum'] is Map
            ? _i2.FuelSumAggregateOutputType.fromJson(json['_sum'])
            : null,
        $min: json['_min'] is Map
            ? _i2.FuelMinAggregateOutputType.fromJson(json['_min'])
            : null,
        $max: json['_max'] is Map
            ? _i2.FuelMaxAggregateOutputType.fromJson(json['_max'])
            : null,
      );

  final int? id;

  final String? name;

  final _i2.FuelCountAggregateOutputType? $count;

  final _i2.FuelAvgAggregateOutputType? $avg;

  final _i2.FuelSumAggregateOutputType? $sum;

  final _i2.FuelMinAggregateOutputType? $min;

  final _i2.FuelMaxAggregateOutputType? $max;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        '_count': $count?.toJson(),
        '_avg': $avg?.toJson(),
        '_sum': $sum?.toJson(),
        '_min': $min?.toJson(),
        '_max': $max?.toJson(),
      };
}

class FuelCountOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelCountOrderByAggregateInput({
    this.id,
    this.name,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
      };
}

class FuelAvgOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelAvgOrderByAggregateInput({this.id});

  final _i2.SortOrder? id;

  @override
  Map<String, dynamic> toJson() => {'id': id};
}

class FuelMaxOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelMaxOrderByAggregateInput({
    this.id,
    this.name,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
      };
}

class FuelMinOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelMinOrderByAggregateInput({
    this.id,
    this.name,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
      };
}

class FuelSumOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelSumOrderByAggregateInput({this.id});

  final _i2.SortOrder? id;

  @override
  Map<String, dynamic> toJson() => {'id': id};
}

class FuelOrderByWithAggregationInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelOrderByWithAggregationInput({
    this.id,
    this.name,
    this.$count,
    this.$avg,
    this.$max,
    this.$min,
    this.$sum,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  final _i2.FuelCountOrderByAggregateInput? $count;

  final _i2.FuelAvgOrderByAggregateInput? $avg;

  final _i2.FuelMaxOrderByAggregateInput? $max;

  final _i2.FuelMinOrderByAggregateInput? $min;

  final _i2.FuelSumOrderByAggregateInput? $sum;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        '_count': $count,
        '_avg': $avg,
        '_max': $max,
        '_min': $min,
        '_sum': $sum,
      };
}

class FuelScalarWhereWithAggregatesInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelScalarWhereWithAggregatesInput({
    this.AND,
    this.OR,
    this.NOT,
    this.id,
    this.name,
  });

  final _i1.PrismaUnion<_i2.FuelScalarWhereWithAggregatesInput,
      Iterable<_i2.FuelScalarWhereWithAggregatesInput>>? AND;

  final Iterable<_i2.FuelScalarWhereWithAggregatesInput>? OR;

  final _i1.PrismaUnion<_i2.FuelScalarWhereWithAggregatesInput,
      Iterable<_i2.FuelScalarWhereWithAggregatesInput>>? NOT;

  final _i1.PrismaUnion<_i2.IntWithAggregatesFilter, int>? id;

  final _i1.PrismaUnion<_i2.StringWithAggregatesFilter, String>? name;

  @override
  Map<String, dynamic> toJson() => {
        'AND': AND,
        'OR': OR,
        'NOT': NOT,
        'id': id,
        'name': name,
      };
}

class FuelCountAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelCountAggregateOutputTypeSelect({
    this.id,
    this.name,
    this.$all,
  });

  final bool? id;

  final bool? name;

  final bool? $all;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        '_all': $all,
      };
}

class FuelGroupByOutputTypeCountArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelGroupByOutputTypeCountArgs({this.select});

  final _i2.FuelCountAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class FuelAvgAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelAvgAggregateOutputTypeSelect({this.id});

  final bool? id;

  @override
  Map<String, dynamic> toJson() => {'id': id};
}

class FuelGroupByOutputTypeAvgArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelGroupByOutputTypeAvgArgs({this.select});

  final _i2.FuelAvgAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class FuelSumAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelSumAggregateOutputTypeSelect({this.id});

  final bool? id;

  @override
  Map<String, dynamic> toJson() => {'id': id};
}

class FuelGroupByOutputTypeSumArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelGroupByOutputTypeSumArgs({this.select});

  final _i2.FuelSumAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class FuelMinAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelMinAggregateOutputTypeSelect({
    this.id,
    this.name,
  });

  final bool? id;

  final bool? name;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
      };
}

class FuelGroupByOutputTypeMinArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelGroupByOutputTypeMinArgs({this.select});

  final _i2.FuelMinAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class FuelMaxAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelMaxAggregateOutputTypeSelect({
    this.id,
    this.name,
  });

  final bool? id;

  final bool? name;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
      };
}

class FuelGroupByOutputTypeMaxArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelGroupByOutputTypeMaxArgs({this.select});

  final _i2.FuelMaxAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class FuelGroupByOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FuelGroupByOutputTypeSelect({
    this.id,
    this.name,
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  final bool? id;

  final bool? name;

  final _i1.PrismaUnion<bool, _i2.FuelGroupByOutputTypeCountArgs>? $count;

  final _i1.PrismaUnion<bool, _i2.FuelGroupByOutputTypeAvgArgs>? $avg;

  final _i1.PrismaUnion<bool, _i2.FuelGroupByOutputTypeSumArgs>? $sum;

  final _i1.PrismaUnion<bool, _i2.FuelGroupByOutputTypeMinArgs>? $min;

  final _i1.PrismaUnion<bool, _i2.FuelGroupByOutputTypeMaxArgs>? $max;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        '_count': $count,
        '_avg': $avg,
        '_sum': $sum,
        '_min': $min,
        '_max': $max,
      };
}

class AggregateFuel {
  const AggregateFuel({
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  factory AggregateFuel.fromJson(Map json) => AggregateFuel(
        $count: json['_count'] is Map
            ? _i2.FuelCountAggregateOutputType.fromJson(json['_count'])
            : null,
        $avg: json['_avg'] is Map
            ? _i2.FuelAvgAggregateOutputType.fromJson(json['_avg'])
            : null,
        $sum: json['_sum'] is Map
            ? _i2.FuelSumAggregateOutputType.fromJson(json['_sum'])
            : null,
        $min: json['_min'] is Map
            ? _i2.FuelMinAggregateOutputType.fromJson(json['_min'])
            : null,
        $max: json['_max'] is Map
            ? _i2.FuelMaxAggregateOutputType.fromJson(json['_max'])
            : null,
      );

  final _i2.FuelCountAggregateOutputType? $count;

  final _i2.FuelAvgAggregateOutputType? $avg;

  final _i2.FuelSumAggregateOutputType? $sum;

  final _i2.FuelMinAggregateOutputType? $min;

  final _i2.FuelMaxAggregateOutputType? $max;

  Map<String, dynamic> toJson() => {
        '_count': $count?.toJson(),
        '_avg': $avg?.toJson(),
        '_sum': $sum?.toJson(),
        '_min': $min?.toJson(),
        '_max': $max?.toJson(),
      };
}

class AggregateFuelCountArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateFuelCountArgs({this.select});

  final _i2.FuelCountAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateFuelAvgArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateFuelAvgArgs({this.select});

  final _i2.FuelAvgAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateFuelSumArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateFuelSumArgs({this.select});

  final _i2.FuelSumAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateFuelMinArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateFuelMinArgs({this.select});

  final _i2.FuelMinAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateFuelMaxArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateFuelMaxArgs({this.select});

  final _i2.FuelMaxAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateFuelSelect implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateFuelSelect({
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  final _i1.PrismaUnion<bool, _i2.AggregateFuelCountArgs>? $count;

  final _i1.PrismaUnion<bool, _i2.AggregateFuelAvgArgs>? $avg;

  final _i1.PrismaUnion<bool, _i2.AggregateFuelSumArgs>? $sum;

  final _i1.PrismaUnion<bool, _i2.AggregateFuelMinArgs>? $min;

  final _i1.PrismaUnion<bool, _i2.AggregateFuelMaxArgs>? $max;

  @override
  Map<String, dynamic> toJson() => {
        '_count': $count,
        '_avg': $avg,
        '_sum': $sum,
        '_min': $min,
        '_max': $max,
      };
}

enum CategoryScalar<T> implements _i1.PrismaEnum, _i1.Reference<T> {
  id<int>('id', 'Category'),
  name$<String>('name', 'Category');

  const CategoryScalar(
    this.name,
    this.model,
  );

  @override
  final String name;

  @override
  final String model;
}

class ExpensiveCreateWithoutCategoryInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveCreateWithoutCategoryInput({
    required this.name,
    required this.description,
    required this.createdDate,
    required this.range,
    required this.price,
    this.liters,
    required this.user,
    this.fuel,
  });

  final String name;

  final String description;

  final DateTime createdDate;

  final double range;

  final double price;

  final _i1.PrismaUnion<double, _i1.PrismaNull>? liters;

  final _i2.UserCreateNestedOneWithoutExpensiveInput user;

  final _i2.FuelCreateNestedOneWithoutExpensiveInput? fuel;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'liters': liters,
        'User': user,
        'Fuel': fuel,
      };
}

class ExpensiveUncheckedCreateWithoutCategoryInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveUncheckedCreateWithoutCategoryInput({
    this.id,
    required this.name,
    required this.description,
    required this.createdDate,
    required this.range,
    required this.price,
    required this.userId,
    this.fuelTypeId,
    this.liters,
  });

  final int? id;

  final String name;

  final String description;

  final DateTime createdDate;

  final double range;

  final double price;

  final int userId;

  final _i1.PrismaUnion<int, _i1.PrismaNull>? fuelTypeId;

  final _i1.PrismaUnion<double, _i1.PrismaNull>? liters;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'user_id': userId,
        'fuel_type_id': fuelTypeId,
        'liters': liters,
      };
}

class ExpensiveCreateOrConnectWithoutCategoryInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveCreateOrConnectWithoutCategoryInput({
    required this.where,
    required this.create,
  });

  final _i2.ExpensiveWhereUniqueInput where;

  final _i1.PrismaUnion<_i2.ExpensiveCreateWithoutCategoryInput,
      _i2.ExpensiveUncheckedCreateWithoutCategoryInput> create;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'create': create,
      };
}

class ExpensiveCreateManyCategoryInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveCreateManyCategoryInput({
    this.id,
    required this.name,
    required this.description,
    required this.createdDate,
    required this.range,
    required this.price,
    required this.userId,
    this.fuelTypeId,
    this.liters,
  });

  final int? id;

  final String name;

  final String description;

  final DateTime createdDate;

  final double range;

  final double price;

  final int userId;

  final _i1.PrismaUnion<int, _i1.PrismaNull>? fuelTypeId;

  final _i1.PrismaUnion<double, _i1.PrismaNull>? liters;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'user_id': userId,
        'fuel_type_id': fuelTypeId,
        'liters': liters,
      };
}

class ExpensiveCreateManyCategoryInputEnvelope
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveCreateManyCategoryInputEnvelope({
    required this.data,
    this.skipDuplicates,
  });

  final _i1.PrismaUnion<_i2.ExpensiveCreateManyCategoryInput,
      Iterable<_i2.ExpensiveCreateManyCategoryInput>> data;

  final bool? skipDuplicates;

  @override
  Map<String, dynamic> toJson() => {
        'data': data,
        'skipDuplicates': skipDuplicates,
      };
}

class ExpensiveCreateNestedManyWithoutCategoryInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveCreateNestedManyWithoutCategoryInput({
    this.create,
    this.connectOrCreate,
    this.createMany,
    this.connect,
  });

  final _i1.PrismaUnion<
          _i2.ExpensiveCreateWithoutCategoryInput,
          _i1.PrismaUnion<
              Iterable<_i2.ExpensiveCreateWithoutCategoryInput>,
              _i1.PrismaUnion<_i2.ExpensiveUncheckedCreateWithoutCategoryInput,
                  Iterable<_i2.ExpensiveUncheckedCreateWithoutCategoryInput>>>>?
      create;

  final _i1.PrismaUnion<_i2.ExpensiveCreateOrConnectWithoutCategoryInput,
          Iterable<_i2.ExpensiveCreateOrConnectWithoutCategoryInput>>?
      connectOrCreate;

  final _i2.ExpensiveCreateManyCategoryInputEnvelope? createMany;

  final _i1.PrismaUnion<_i2.ExpensiveWhereUniqueInput,
      Iterable<_i2.ExpensiveWhereUniqueInput>>? connect;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'createMany': createMany,
        'connect': connect,
      };
}

class CategoryCreateInput implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryCreateInput({
    required this.name,
    this.expensive,
  });

  final String name;

  final _i2.ExpensiveCreateNestedManyWithoutCategoryInput? expensive;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'Expensive': expensive,
      };
}

class ExpensiveUncheckedCreateNestedManyWithoutCategoryInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveUncheckedCreateNestedManyWithoutCategoryInput({
    this.create,
    this.connectOrCreate,
    this.createMany,
    this.connect,
  });

  final _i1.PrismaUnion<
          _i2.ExpensiveCreateWithoutCategoryInput,
          _i1.PrismaUnion<
              Iterable<_i2.ExpensiveCreateWithoutCategoryInput>,
              _i1.PrismaUnion<_i2.ExpensiveUncheckedCreateWithoutCategoryInput,
                  Iterable<_i2.ExpensiveUncheckedCreateWithoutCategoryInput>>>>?
      create;

  final _i1.PrismaUnion<_i2.ExpensiveCreateOrConnectWithoutCategoryInput,
          Iterable<_i2.ExpensiveCreateOrConnectWithoutCategoryInput>>?
      connectOrCreate;

  final _i2.ExpensiveCreateManyCategoryInputEnvelope? createMany;

  final _i1.PrismaUnion<_i2.ExpensiveWhereUniqueInput,
      Iterable<_i2.ExpensiveWhereUniqueInput>>? connect;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'createMany': createMany,
        'connect': connect,
      };
}

class CategoryUncheckedCreateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryUncheckedCreateInput({
    this.id,
    required this.name,
    this.expensive,
  });

  final int? id;

  final String name;

  final _i2.ExpensiveUncheckedCreateNestedManyWithoutCategoryInput? expensive;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'Expensive': expensive,
      };
}

class CategoryCreateManyInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryCreateManyInput({
    this.id,
    required this.name,
  });

  final int? id;

  final String name;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
      };
}

class ExpensiveUpdateWithoutCategoryInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveUpdateWithoutCategoryInput({
    this.name,
    this.description,
    this.createdDate,
    this.range,
    this.price,
    this.liters,
    this.user,
    this.fuel,
  });

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>?
      description;

  final _i1.PrismaUnion<DateTime, _i2.DateTimeFieldUpdateOperationsInput>?
      createdDate;

  final _i1.PrismaUnion<double, _i2.FloatFieldUpdateOperationsInput>? range;

  final _i1.PrismaUnion<double, _i2.FloatFieldUpdateOperationsInput>? price;

  final _i1.PrismaUnion<
      double,
      _i1.PrismaUnion<_i2.NullableFloatFieldUpdateOperationsInput,
          _i1.PrismaNull>>? liters;

  final _i2.UserUpdateOneRequiredWithoutExpensiveNestedInput? user;

  final _i2.FuelUpdateOneWithoutExpensiveNestedInput? fuel;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'liters': liters,
        'User': user,
        'Fuel': fuel,
      };
}

class ExpensiveUncheckedUpdateWithoutCategoryInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveUncheckedUpdateWithoutCategoryInput({
    this.id,
    this.name,
    this.description,
    this.createdDate,
    this.range,
    this.price,
    this.userId,
    this.fuelTypeId,
    this.liters,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>?
      description;

  final _i1.PrismaUnion<DateTime, _i2.DateTimeFieldUpdateOperationsInput>?
      createdDate;

  final _i1.PrismaUnion<double, _i2.FloatFieldUpdateOperationsInput>? range;

  final _i1.PrismaUnion<double, _i2.FloatFieldUpdateOperationsInput>? price;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? userId;

  final _i1.PrismaUnion<
      int,
      _i1.PrismaUnion<_i2.NullableIntFieldUpdateOperationsInput,
          _i1.PrismaNull>>? fuelTypeId;

  final _i1.PrismaUnion<
      double,
      _i1.PrismaUnion<_i2.NullableFloatFieldUpdateOperationsInput,
          _i1.PrismaNull>>? liters;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'user_id': userId,
        'fuel_type_id': fuelTypeId,
        'liters': liters,
      };
}

class ExpensiveUpsertWithWhereUniqueWithoutCategoryInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveUpsertWithWhereUniqueWithoutCategoryInput({
    required this.where,
    required this.update,
    required this.create,
  });

  final _i2.ExpensiveWhereUniqueInput where;

  final _i1.PrismaUnion<_i2.ExpensiveUpdateWithoutCategoryInput,
      _i2.ExpensiveUncheckedUpdateWithoutCategoryInput> update;

  final _i1.PrismaUnion<_i2.ExpensiveCreateWithoutCategoryInput,
      _i2.ExpensiveUncheckedCreateWithoutCategoryInput> create;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'update': update,
        'create': create,
      };
}

class ExpensiveUpdateWithWhereUniqueWithoutCategoryInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveUpdateWithWhereUniqueWithoutCategoryInput({
    required this.where,
    required this.data,
  });

  final _i2.ExpensiveWhereUniqueInput where;

  final _i1.PrismaUnion<_i2.ExpensiveUpdateWithoutCategoryInput,
      _i2.ExpensiveUncheckedUpdateWithoutCategoryInput> data;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'data': data,
      };
}

class ExpensiveUpdateManyWithWhereWithoutCategoryInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveUpdateManyWithWhereWithoutCategoryInput({
    required this.where,
    required this.data,
  });

  final _i2.ExpensiveScalarWhereInput where;

  final _i1.PrismaUnion<_i2.ExpensiveUpdateManyMutationInput,
      _i2.ExpensiveUncheckedUpdateManyWithoutExpensiveInput> data;

  @override
  Map<String, dynamic> toJson() => {
        'where': where,
        'data': data,
      };
}

class ExpensiveUpdateManyWithoutCategoryNestedInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveUpdateManyWithoutCategoryNestedInput({
    this.create,
    this.connectOrCreate,
    this.upsert,
    this.createMany,
    this.set,
    this.disconnect,
    this.delete,
    this.connect,
    this.update,
    this.updateMany,
    this.deleteMany,
  });

  final _i1.PrismaUnion<
          _i2.ExpensiveCreateWithoutCategoryInput,
          _i1.PrismaUnion<
              Iterable<_i2.ExpensiveCreateWithoutCategoryInput>,
              _i1.PrismaUnion<_i2.ExpensiveUncheckedCreateWithoutCategoryInput,
                  Iterable<_i2.ExpensiveUncheckedCreateWithoutCategoryInput>>>>?
      create;

  final _i1.PrismaUnion<_i2.ExpensiveCreateOrConnectWithoutCategoryInput,
          Iterable<_i2.ExpensiveCreateOrConnectWithoutCategoryInput>>?
      connectOrCreate;

  final _i1.PrismaUnion<_i2.ExpensiveUpsertWithWhereUniqueWithoutCategoryInput,
      Iterable<_i2.ExpensiveUpsertWithWhereUniqueWithoutCategoryInput>>? upsert;

  final _i2.ExpensiveCreateManyCategoryInputEnvelope? createMany;

  final _i1.PrismaUnion<_i2.ExpensiveWhereUniqueInput,
      Iterable<_i2.ExpensiveWhereUniqueInput>>? set;

  final _i1.PrismaUnion<_i2.ExpensiveWhereUniqueInput,
      Iterable<_i2.ExpensiveWhereUniqueInput>>? disconnect;

  final _i1.PrismaUnion<_i2.ExpensiveWhereUniqueInput,
      Iterable<_i2.ExpensiveWhereUniqueInput>>? delete;

  final _i1.PrismaUnion<_i2.ExpensiveWhereUniqueInput,
      Iterable<_i2.ExpensiveWhereUniqueInput>>? connect;

  final _i1.PrismaUnion<_i2.ExpensiveUpdateWithWhereUniqueWithoutCategoryInput,
      Iterable<_i2.ExpensiveUpdateWithWhereUniqueWithoutCategoryInput>>? update;

  final _i1.PrismaUnion<_i2.ExpensiveUpdateManyWithWhereWithoutCategoryInput,
          Iterable<_i2.ExpensiveUpdateManyWithWhereWithoutCategoryInput>>?
      updateMany;

  final _i1.PrismaUnion<_i2.ExpensiveScalarWhereInput,
      Iterable<_i2.ExpensiveScalarWhereInput>>? deleteMany;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'upsert': upsert,
        'createMany': createMany,
        'set': set,
        'disconnect': disconnect,
        'delete': delete,
        'connect': connect,
        'update': update,
        'updateMany': updateMany,
        'deleteMany': deleteMany,
      };
}

class CategoryUpdateInput implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryUpdateInput({
    this.name,
    this.expensive,
  });

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i2.ExpensiveUpdateManyWithoutCategoryNestedInput? expensive;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'Expensive': expensive,
      };
}

class ExpensiveUncheckedUpdateManyWithoutCategoryNestedInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveUncheckedUpdateManyWithoutCategoryNestedInput({
    this.create,
    this.connectOrCreate,
    this.upsert,
    this.createMany,
    this.set,
    this.disconnect,
    this.delete,
    this.connect,
    this.update,
    this.updateMany,
    this.deleteMany,
  });

  final _i1.PrismaUnion<
          _i2.ExpensiveCreateWithoutCategoryInput,
          _i1.PrismaUnion<
              Iterable<_i2.ExpensiveCreateWithoutCategoryInput>,
              _i1.PrismaUnion<_i2.ExpensiveUncheckedCreateWithoutCategoryInput,
                  Iterable<_i2.ExpensiveUncheckedCreateWithoutCategoryInput>>>>?
      create;

  final _i1.PrismaUnion<_i2.ExpensiveCreateOrConnectWithoutCategoryInput,
          Iterable<_i2.ExpensiveCreateOrConnectWithoutCategoryInput>>?
      connectOrCreate;

  final _i1.PrismaUnion<_i2.ExpensiveUpsertWithWhereUniqueWithoutCategoryInput,
      Iterable<_i2.ExpensiveUpsertWithWhereUniqueWithoutCategoryInput>>? upsert;

  final _i2.ExpensiveCreateManyCategoryInputEnvelope? createMany;

  final _i1.PrismaUnion<_i2.ExpensiveWhereUniqueInput,
      Iterable<_i2.ExpensiveWhereUniqueInput>>? set;

  final _i1.PrismaUnion<_i2.ExpensiveWhereUniqueInput,
      Iterable<_i2.ExpensiveWhereUniqueInput>>? disconnect;

  final _i1.PrismaUnion<_i2.ExpensiveWhereUniqueInput,
      Iterable<_i2.ExpensiveWhereUniqueInput>>? delete;

  final _i1.PrismaUnion<_i2.ExpensiveWhereUniqueInput,
      Iterable<_i2.ExpensiveWhereUniqueInput>>? connect;

  final _i1.PrismaUnion<_i2.ExpensiveUpdateWithWhereUniqueWithoutCategoryInput,
      Iterable<_i2.ExpensiveUpdateWithWhereUniqueWithoutCategoryInput>>? update;

  final _i1.PrismaUnion<_i2.ExpensiveUpdateManyWithWhereWithoutCategoryInput,
          Iterable<_i2.ExpensiveUpdateManyWithWhereWithoutCategoryInput>>?
      updateMany;

  final _i1.PrismaUnion<_i2.ExpensiveScalarWhereInput,
      Iterable<_i2.ExpensiveScalarWhereInput>>? deleteMany;

  @override
  Map<String, dynamic> toJson() => {
        'create': create,
        'connectOrCreate': connectOrCreate,
        'upsert': upsert,
        'createMany': createMany,
        'set': set,
        'disconnect': disconnect,
        'delete': delete,
        'connect': connect,
        'update': update,
        'updateMany': updateMany,
        'deleteMany': deleteMany,
      };
}

class CategoryUncheckedUpdateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryUncheckedUpdateInput({
    this.id,
    this.name,
    this.expensive,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i2.ExpensiveUncheckedUpdateManyWithoutCategoryNestedInput? expensive;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'Expensive': expensive,
      };
}

class CategoryUpdateManyMutationInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryUpdateManyMutationInput({this.name});

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  @override
  Map<String, dynamic> toJson() => {'name': name};
}

class CategoryUncheckedUpdateManyInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryUncheckedUpdateManyInput({
    this.id,
    this.name,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
      };
}

class CategoryCountAggregateOutputType {
  const CategoryCountAggregateOutputType({
    this.id,
    this.name,
    this.$all,
  });

  factory CategoryCountAggregateOutputType.fromJson(Map json) =>
      CategoryCountAggregateOutputType(
        id: json['id'],
        name: json['name'],
        $all: json['_all'],
      );

  final int? id;

  final int? name;

  final int? $all;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        '_all': $all,
      };
}

class CategoryAvgAggregateOutputType {
  const CategoryAvgAggregateOutputType({this.id});

  factory CategoryAvgAggregateOutputType.fromJson(Map json) =>
      CategoryAvgAggregateOutputType(id: json['id']);

  final double? id;

  Map<String, dynamic> toJson() => {'id': id};
}

class CategorySumAggregateOutputType {
  const CategorySumAggregateOutputType({this.id});

  factory CategorySumAggregateOutputType.fromJson(Map json) =>
      CategorySumAggregateOutputType(id: json['id']);

  final int? id;

  Map<String, dynamic> toJson() => {'id': id};
}

class CategoryMinAggregateOutputType {
  const CategoryMinAggregateOutputType({
    this.id,
    this.name,
  });

  factory CategoryMinAggregateOutputType.fromJson(Map json) =>
      CategoryMinAggregateOutputType(
        id: json['id'],
        name: json['name'],
      );

  final int? id;

  final String? name;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
      };
}

class CategoryMaxAggregateOutputType {
  const CategoryMaxAggregateOutputType({
    this.id,
    this.name,
  });

  factory CategoryMaxAggregateOutputType.fromJson(Map json) =>
      CategoryMaxAggregateOutputType(
        id: json['id'],
        name: json['name'],
      );

  final int? id;

  final String? name;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
      };
}

class CategoryGroupByOutputType {
  const CategoryGroupByOutputType({
    this.id,
    this.name,
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  factory CategoryGroupByOutputType.fromJson(Map json) =>
      CategoryGroupByOutputType(
        id: json['id'],
        name: json['name'],
        $count: json['_count'] is Map
            ? _i2.CategoryCountAggregateOutputType.fromJson(json['_count'])
            : null,
        $avg: json['_avg'] is Map
            ? _i2.CategoryAvgAggregateOutputType.fromJson(json['_avg'])
            : null,
        $sum: json['_sum'] is Map
            ? _i2.CategorySumAggregateOutputType.fromJson(json['_sum'])
            : null,
        $min: json['_min'] is Map
            ? _i2.CategoryMinAggregateOutputType.fromJson(json['_min'])
            : null,
        $max: json['_max'] is Map
            ? _i2.CategoryMaxAggregateOutputType.fromJson(json['_max'])
            : null,
      );

  final int? id;

  final String? name;

  final _i2.CategoryCountAggregateOutputType? $count;

  final _i2.CategoryAvgAggregateOutputType? $avg;

  final _i2.CategorySumAggregateOutputType? $sum;

  final _i2.CategoryMinAggregateOutputType? $min;

  final _i2.CategoryMaxAggregateOutputType? $max;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        '_count': $count?.toJson(),
        '_avg': $avg?.toJson(),
        '_sum': $sum?.toJson(),
        '_min': $min?.toJson(),
        '_max': $max?.toJson(),
      };
}

class CategoryCountOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryCountOrderByAggregateInput({
    this.id,
    this.name,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
      };
}

class CategoryAvgOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryAvgOrderByAggregateInput({this.id});

  final _i2.SortOrder? id;

  @override
  Map<String, dynamic> toJson() => {'id': id};
}

class CategoryMaxOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryMaxOrderByAggregateInput({
    this.id,
    this.name,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
      };
}

class CategoryMinOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryMinOrderByAggregateInput({
    this.id,
    this.name,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
      };
}

class CategorySumOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategorySumOrderByAggregateInput({this.id});

  final _i2.SortOrder? id;

  @override
  Map<String, dynamic> toJson() => {'id': id};
}

class CategoryOrderByWithAggregationInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryOrderByWithAggregationInput({
    this.id,
    this.name,
    this.$count,
    this.$avg,
    this.$max,
    this.$min,
    this.$sum,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  final _i2.CategoryCountOrderByAggregateInput? $count;

  final _i2.CategoryAvgOrderByAggregateInput? $avg;

  final _i2.CategoryMaxOrderByAggregateInput? $max;

  final _i2.CategoryMinOrderByAggregateInput? $min;

  final _i2.CategorySumOrderByAggregateInput? $sum;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        '_count': $count,
        '_avg': $avg,
        '_max': $max,
        '_min': $min,
        '_sum': $sum,
      };
}

class CategoryScalarWhereWithAggregatesInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryScalarWhereWithAggregatesInput({
    this.AND,
    this.OR,
    this.NOT,
    this.id,
    this.name,
  });

  final _i1.PrismaUnion<_i2.CategoryScalarWhereWithAggregatesInput,
      Iterable<_i2.CategoryScalarWhereWithAggregatesInput>>? AND;

  final Iterable<_i2.CategoryScalarWhereWithAggregatesInput>? OR;

  final _i1.PrismaUnion<_i2.CategoryScalarWhereWithAggregatesInput,
      Iterable<_i2.CategoryScalarWhereWithAggregatesInput>>? NOT;

  final _i1.PrismaUnion<_i2.IntWithAggregatesFilter, int>? id;

  final _i1.PrismaUnion<_i2.StringWithAggregatesFilter, String>? name;

  @override
  Map<String, dynamic> toJson() => {
        'AND': AND,
        'OR': OR,
        'NOT': NOT,
        'id': id,
        'name': name,
      };
}

class CategoryCountAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryCountAggregateOutputTypeSelect({
    this.id,
    this.name,
    this.$all,
  });

  final bool? id;

  final bool? name;

  final bool? $all;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        '_all': $all,
      };
}

class CategoryGroupByOutputTypeCountArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryGroupByOutputTypeCountArgs({this.select});

  final _i2.CategoryCountAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class CategoryAvgAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryAvgAggregateOutputTypeSelect({this.id});

  final bool? id;

  @override
  Map<String, dynamic> toJson() => {'id': id};
}

class CategoryGroupByOutputTypeAvgArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryGroupByOutputTypeAvgArgs({this.select});

  final _i2.CategoryAvgAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class CategorySumAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategorySumAggregateOutputTypeSelect({this.id});

  final bool? id;

  @override
  Map<String, dynamic> toJson() => {'id': id};
}

class CategoryGroupByOutputTypeSumArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryGroupByOutputTypeSumArgs({this.select});

  final _i2.CategorySumAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class CategoryMinAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryMinAggregateOutputTypeSelect({
    this.id,
    this.name,
  });

  final bool? id;

  final bool? name;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
      };
}

class CategoryGroupByOutputTypeMinArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryGroupByOutputTypeMinArgs({this.select});

  final _i2.CategoryMinAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class CategoryMaxAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryMaxAggregateOutputTypeSelect({
    this.id,
    this.name,
  });

  final bool? id;

  final bool? name;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
      };
}

class CategoryGroupByOutputTypeMaxArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryGroupByOutputTypeMaxArgs({this.select});

  final _i2.CategoryMaxAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class CategoryGroupByOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const CategoryGroupByOutputTypeSelect({
    this.id,
    this.name,
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  final bool? id;

  final bool? name;

  final _i1.PrismaUnion<bool, _i2.CategoryGroupByOutputTypeCountArgs>? $count;

  final _i1.PrismaUnion<bool, _i2.CategoryGroupByOutputTypeAvgArgs>? $avg;

  final _i1.PrismaUnion<bool, _i2.CategoryGroupByOutputTypeSumArgs>? $sum;

  final _i1.PrismaUnion<bool, _i2.CategoryGroupByOutputTypeMinArgs>? $min;

  final _i1.PrismaUnion<bool, _i2.CategoryGroupByOutputTypeMaxArgs>? $max;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        '_count': $count,
        '_avg': $avg,
        '_sum': $sum,
        '_min': $min,
        '_max': $max,
      };
}

class AggregateCategory {
  const AggregateCategory({
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  factory AggregateCategory.fromJson(Map json) => AggregateCategory(
        $count: json['_count'] is Map
            ? _i2.CategoryCountAggregateOutputType.fromJson(json['_count'])
            : null,
        $avg: json['_avg'] is Map
            ? _i2.CategoryAvgAggregateOutputType.fromJson(json['_avg'])
            : null,
        $sum: json['_sum'] is Map
            ? _i2.CategorySumAggregateOutputType.fromJson(json['_sum'])
            : null,
        $min: json['_min'] is Map
            ? _i2.CategoryMinAggregateOutputType.fromJson(json['_min'])
            : null,
        $max: json['_max'] is Map
            ? _i2.CategoryMaxAggregateOutputType.fromJson(json['_max'])
            : null,
      );

  final _i2.CategoryCountAggregateOutputType? $count;

  final _i2.CategoryAvgAggregateOutputType? $avg;

  final _i2.CategorySumAggregateOutputType? $sum;

  final _i2.CategoryMinAggregateOutputType? $min;

  final _i2.CategoryMaxAggregateOutputType? $max;

  Map<String, dynamic> toJson() => {
        '_count': $count?.toJson(),
        '_avg': $avg?.toJson(),
        '_sum': $sum?.toJson(),
        '_min': $min?.toJson(),
        '_max': $max?.toJson(),
      };
}

class AggregateCategoryCountArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateCategoryCountArgs({this.select});

  final _i2.CategoryCountAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateCategoryAvgArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateCategoryAvgArgs({this.select});

  final _i2.CategoryAvgAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateCategorySumArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateCategorySumArgs({this.select});

  final _i2.CategorySumAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateCategoryMinArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateCategoryMinArgs({this.select});

  final _i2.CategoryMinAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateCategoryMaxArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateCategoryMaxArgs({this.select});

  final _i2.CategoryMaxAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateCategorySelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateCategorySelect({
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  final _i1.PrismaUnion<bool, _i2.AggregateCategoryCountArgs>? $count;

  final _i1.PrismaUnion<bool, _i2.AggregateCategoryAvgArgs>? $avg;

  final _i1.PrismaUnion<bool, _i2.AggregateCategorySumArgs>? $sum;

  final _i1.PrismaUnion<bool, _i2.AggregateCategoryMinArgs>? $min;

  final _i1.PrismaUnion<bool, _i2.AggregateCategoryMaxArgs>? $max;

  @override
  Map<String, dynamic> toJson() => {
        '_count': $count,
        '_avg': $avg,
        '_sum': $sum,
        '_min': $min,
        '_max': $max,
      };
}

class ExpensiveCreateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveCreateInput({
    required this.name,
    required this.description,
    required this.createdDate,
    required this.range,
    required this.price,
    this.liters,
    required this.category,
    required this.user,
    this.fuel,
  });

  final String name;

  final String description;

  final DateTime createdDate;

  final double range;

  final double price;

  final _i1.PrismaUnion<double, _i1.PrismaNull>? liters;

  final _i2.CategoryCreateNestedOneWithoutExpensiveInput category;

  final _i2.UserCreateNestedOneWithoutExpensiveInput user;

  final _i2.FuelCreateNestedOneWithoutExpensiveInput? fuel;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'liters': liters,
        'category': category,
        'User': user,
        'Fuel': fuel,
      };
}

class ExpensiveUncheckedCreateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveUncheckedCreateInput({
    this.id,
    required this.name,
    required this.categoryId,
    required this.description,
    required this.createdDate,
    required this.range,
    required this.price,
    required this.userId,
    this.fuelTypeId,
    this.liters,
  });

  final int? id;

  final String name;

  final int categoryId;

  final String description;

  final DateTime createdDate;

  final double range;

  final double price;

  final int userId;

  final _i1.PrismaUnion<int, _i1.PrismaNull>? fuelTypeId;

  final _i1.PrismaUnion<double, _i1.PrismaNull>? liters;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'category_id': categoryId,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'user_id': userId,
        'fuel_type_id': fuelTypeId,
        'liters': liters,
      };
}

class ExpensiveCreateManyInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveCreateManyInput({
    this.id,
    required this.name,
    required this.categoryId,
    required this.description,
    required this.createdDate,
    required this.range,
    required this.price,
    required this.userId,
    this.fuelTypeId,
    this.liters,
  });

  final int? id;

  final String name;

  final int categoryId;

  final String description;

  final DateTime createdDate;

  final double range;

  final double price;

  final int userId;

  final _i1.PrismaUnion<int, _i1.PrismaNull>? fuelTypeId;

  final _i1.PrismaUnion<double, _i1.PrismaNull>? liters;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'category_id': categoryId,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'user_id': userId,
        'fuel_type_id': fuelTypeId,
        'liters': liters,
      };
}

class ExpensiveUpdateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveUpdateInput({
    this.name,
    this.description,
    this.createdDate,
    this.range,
    this.price,
    this.liters,
    this.category,
    this.user,
    this.fuel,
  });

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>?
      description;

  final _i1.PrismaUnion<DateTime, _i2.DateTimeFieldUpdateOperationsInput>?
      createdDate;

  final _i1.PrismaUnion<double, _i2.FloatFieldUpdateOperationsInput>? range;

  final _i1.PrismaUnion<double, _i2.FloatFieldUpdateOperationsInput>? price;

  final _i1.PrismaUnion<
      double,
      _i1.PrismaUnion<_i2.NullableFloatFieldUpdateOperationsInput,
          _i1.PrismaNull>>? liters;

  final _i2.CategoryUpdateOneRequiredWithoutExpensiveNestedInput? category;

  final _i2.UserUpdateOneRequiredWithoutExpensiveNestedInput? user;

  final _i2.FuelUpdateOneWithoutExpensiveNestedInput? fuel;

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'liters': liters,
        'category': category,
        'User': user,
        'Fuel': fuel,
      };
}

class ExpensiveUncheckedUpdateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveUncheckedUpdateInput({
    this.id,
    this.name,
    this.categoryId,
    this.description,
    this.createdDate,
    this.range,
    this.price,
    this.userId,
    this.fuelTypeId,
    this.liters,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? categoryId;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>?
      description;

  final _i1.PrismaUnion<DateTime, _i2.DateTimeFieldUpdateOperationsInput>?
      createdDate;

  final _i1.PrismaUnion<double, _i2.FloatFieldUpdateOperationsInput>? range;

  final _i1.PrismaUnion<double, _i2.FloatFieldUpdateOperationsInput>? price;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? userId;

  final _i1.PrismaUnion<
      int,
      _i1.PrismaUnion<_i2.NullableIntFieldUpdateOperationsInput,
          _i1.PrismaNull>>? fuelTypeId;

  final _i1.PrismaUnion<
      double,
      _i1.PrismaUnion<_i2.NullableFloatFieldUpdateOperationsInput,
          _i1.PrismaNull>>? liters;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'category_id': categoryId,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'user_id': userId,
        'fuel_type_id': fuelTypeId,
        'liters': liters,
      };
}

class ExpensiveUncheckedUpdateManyInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveUncheckedUpdateManyInput({
    this.id,
    this.name,
    this.categoryId,
    this.description,
    this.createdDate,
    this.range,
    this.price,
    this.userId,
    this.fuelTypeId,
    this.liters,
  });

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? id;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>? name;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? categoryId;

  final _i1.PrismaUnion<String, _i2.StringFieldUpdateOperationsInput>?
      description;

  final _i1.PrismaUnion<DateTime, _i2.DateTimeFieldUpdateOperationsInput>?
      createdDate;

  final _i1.PrismaUnion<double, _i2.FloatFieldUpdateOperationsInput>? range;

  final _i1.PrismaUnion<double, _i2.FloatFieldUpdateOperationsInput>? price;

  final _i1.PrismaUnion<int, _i2.IntFieldUpdateOperationsInput>? userId;

  final _i1.PrismaUnion<
      int,
      _i1.PrismaUnion<_i2.NullableIntFieldUpdateOperationsInput,
          _i1.PrismaNull>>? fuelTypeId;

  final _i1.PrismaUnion<
      double,
      _i1.PrismaUnion<_i2.NullableFloatFieldUpdateOperationsInput,
          _i1.PrismaNull>>? liters;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'category_id': categoryId,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'user_id': userId,
        'fuel_type_id': fuelTypeId,
        'liters': liters,
      };
}

class ExpensiveCountAggregateOutputType {
  const ExpensiveCountAggregateOutputType({
    this.id,
    this.name,
    this.categoryId,
    this.description,
    this.createdDate,
    this.range,
    this.price,
    this.userId,
    this.fuelTypeId,
    this.liters,
    this.$all,
  });

  factory ExpensiveCountAggregateOutputType.fromJson(Map json) =>
      ExpensiveCountAggregateOutputType(
        id: json['id'],
        name: json['name'],
        categoryId: json['category_id'],
        description: json['description'],
        createdDate: json['created_date'],
        range: json['range'],
        price: json['price'],
        userId: json['user_id'],
        fuelTypeId: json['fuel_type_id'],
        liters: json['liters'],
        $all: json['_all'],
      );

  final int? id;

  final int? name;

  final int? categoryId;

  final int? description;

  final int? createdDate;

  final int? range;

  final int? price;

  final int? userId;

  final int? fuelTypeId;

  final int? liters;

  final int? $all;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'category_id': categoryId,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'user_id': userId,
        'fuel_type_id': fuelTypeId,
        'liters': liters,
        '_all': $all,
      };
}

class ExpensiveAvgAggregateOutputType {
  const ExpensiveAvgAggregateOutputType({
    this.id,
    this.categoryId,
    this.range,
    this.price,
    this.userId,
    this.fuelTypeId,
    this.liters,
  });

  factory ExpensiveAvgAggregateOutputType.fromJson(Map json) =>
      ExpensiveAvgAggregateOutputType(
        id: json['id'],
        categoryId: json['category_id'],
        range: json['range'],
        price: json['price'],
        userId: json['user_id'],
        fuelTypeId: json['fuel_type_id'],
        liters: json['liters'],
      );

  final double? id;

  final double? categoryId;

  final double? range;

  final double? price;

  final double? userId;

  final double? fuelTypeId;

  final double? liters;

  Map<String, dynamic> toJson() => {
        'id': id,
        'category_id': categoryId,
        'range': range,
        'price': price,
        'user_id': userId,
        'fuel_type_id': fuelTypeId,
        'liters': liters,
      };
}

class ExpensiveSumAggregateOutputType {
  const ExpensiveSumAggregateOutputType({
    this.id,
    this.categoryId,
    this.range,
    this.price,
    this.userId,
    this.fuelTypeId,
    this.liters,
  });

  factory ExpensiveSumAggregateOutputType.fromJson(Map json) =>
      ExpensiveSumAggregateOutputType(
        id: json['id'],
        categoryId: json['category_id'],
        range: json['range'],
        price: json['price'],
        userId: json['user_id'],
        fuelTypeId: json['fuel_type_id'],
        liters: json['liters'],
      );

  final int? id;

  final int? categoryId;

  final double? range;

  final double? price;

  final int? userId;

  final int? fuelTypeId;

  final double? liters;

  Map<String, dynamic> toJson() => {
        'id': id,
        'category_id': categoryId,
        'range': range,
        'price': price,
        'user_id': userId,
        'fuel_type_id': fuelTypeId,
        'liters': liters,
      };
}

class ExpensiveMinAggregateOutputType {
  const ExpensiveMinAggregateOutputType({
    this.id,
    this.name,
    this.categoryId,
    this.description,
    this.createdDate,
    this.range,
    this.price,
    this.userId,
    this.fuelTypeId,
    this.liters,
  });

  factory ExpensiveMinAggregateOutputType.fromJson(Map json) =>
      ExpensiveMinAggregateOutputType(
        id: json['id'],
        name: json['name'],
        categoryId: json['category_id'],
        description: json['description'],
        createdDate: json['created_date'],
        range: json['range'],
        price: json['price'],
        userId: json['user_id'],
        fuelTypeId: json['fuel_type_id'],
        liters: json['liters'],
      );

  final int? id;

  final String? name;

  final int? categoryId;

  final String? description;

  final DateTime? createdDate;

  final double? range;

  final double? price;

  final int? userId;

  final int? fuelTypeId;

  final double? liters;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'category_id': categoryId,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'user_id': userId,
        'fuel_type_id': fuelTypeId,
        'liters': liters,
      };
}

class ExpensiveMaxAggregateOutputType {
  const ExpensiveMaxAggregateOutputType({
    this.id,
    this.name,
    this.categoryId,
    this.description,
    this.createdDate,
    this.range,
    this.price,
    this.userId,
    this.fuelTypeId,
    this.liters,
  });

  factory ExpensiveMaxAggregateOutputType.fromJson(Map json) =>
      ExpensiveMaxAggregateOutputType(
        id: json['id'],
        name: json['name'],
        categoryId: json['category_id'],
        description: json['description'],
        createdDate: json['created_date'],
        range: json['range'],
        price: json['price'],
        userId: json['user_id'],
        fuelTypeId: json['fuel_type_id'],
        liters: json['liters'],
      );

  final int? id;

  final String? name;

  final int? categoryId;

  final String? description;

  final DateTime? createdDate;

  final double? range;

  final double? price;

  final int? userId;

  final int? fuelTypeId;

  final double? liters;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'category_id': categoryId,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'user_id': userId,
        'fuel_type_id': fuelTypeId,
        'liters': liters,
      };
}

class ExpensiveGroupByOutputType {
  const ExpensiveGroupByOutputType({
    this.id,
    this.name,
    this.categoryId,
    this.description,
    this.createdDate,
    this.range,
    this.price,
    this.userId,
    this.fuelTypeId,
    this.liters,
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  factory ExpensiveGroupByOutputType.fromJson(Map json) =>
      ExpensiveGroupByOutputType(
        id: json['id'],
        name: json['name'],
        categoryId: json['category_id'],
        description: json['description'],
        createdDate: json['created_date'],
        range: json['range'],
        price: json['price'],
        userId: json['user_id'],
        fuelTypeId: json['fuel_type_id'],
        liters: json['liters'],
        $count: json['_count'] is Map
            ? _i2.ExpensiveCountAggregateOutputType.fromJson(json['_count'])
            : null,
        $avg: json['_avg'] is Map
            ? _i2.ExpensiveAvgAggregateOutputType.fromJson(json['_avg'])
            : null,
        $sum: json['_sum'] is Map
            ? _i2.ExpensiveSumAggregateOutputType.fromJson(json['_sum'])
            : null,
        $min: json['_min'] is Map
            ? _i2.ExpensiveMinAggregateOutputType.fromJson(json['_min'])
            : null,
        $max: json['_max'] is Map
            ? _i2.ExpensiveMaxAggregateOutputType.fromJson(json['_max'])
            : null,
      );

  final int? id;

  final String? name;

  final int? categoryId;

  final String? description;

  final DateTime? createdDate;

  final double? range;

  final double? price;

  final int? userId;

  final int? fuelTypeId;

  final double? liters;

  final _i2.ExpensiveCountAggregateOutputType? $count;

  final _i2.ExpensiveAvgAggregateOutputType? $avg;

  final _i2.ExpensiveSumAggregateOutputType? $sum;

  final _i2.ExpensiveMinAggregateOutputType? $min;

  final _i2.ExpensiveMaxAggregateOutputType? $max;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'category_id': categoryId,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'user_id': userId,
        'fuel_type_id': fuelTypeId,
        'liters': liters,
        '_count': $count?.toJson(),
        '_avg': $avg?.toJson(),
        '_sum': $sum?.toJson(),
        '_min': $min?.toJson(),
        '_max': $max?.toJson(),
      };
}

class ExpensiveCountOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveCountOrderByAggregateInput({
    this.id,
    this.name,
    this.categoryId,
    this.description,
    this.createdDate,
    this.range,
    this.price,
    this.userId,
    this.fuelTypeId,
    this.liters,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  final _i2.SortOrder? categoryId;

  final _i2.SortOrder? description;

  final _i2.SortOrder? createdDate;

  final _i2.SortOrder? range;

  final _i2.SortOrder? price;

  final _i2.SortOrder? userId;

  final _i2.SortOrder? fuelTypeId;

  final _i2.SortOrder? liters;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'category_id': categoryId,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'user_id': userId,
        'fuel_type_id': fuelTypeId,
        'liters': liters,
      };
}

class ExpensiveAvgOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveAvgOrderByAggregateInput({
    this.id,
    this.categoryId,
    this.range,
    this.price,
    this.userId,
    this.fuelTypeId,
    this.liters,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? categoryId;

  final _i2.SortOrder? range;

  final _i2.SortOrder? price;

  final _i2.SortOrder? userId;

  final _i2.SortOrder? fuelTypeId;

  final _i2.SortOrder? liters;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'category_id': categoryId,
        'range': range,
        'price': price,
        'user_id': userId,
        'fuel_type_id': fuelTypeId,
        'liters': liters,
      };
}

class ExpensiveMaxOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveMaxOrderByAggregateInput({
    this.id,
    this.name,
    this.categoryId,
    this.description,
    this.createdDate,
    this.range,
    this.price,
    this.userId,
    this.fuelTypeId,
    this.liters,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  final _i2.SortOrder? categoryId;

  final _i2.SortOrder? description;

  final _i2.SortOrder? createdDate;

  final _i2.SortOrder? range;

  final _i2.SortOrder? price;

  final _i2.SortOrder? userId;

  final _i2.SortOrder? fuelTypeId;

  final _i2.SortOrder? liters;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'category_id': categoryId,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'user_id': userId,
        'fuel_type_id': fuelTypeId,
        'liters': liters,
      };
}

class ExpensiveMinOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveMinOrderByAggregateInput({
    this.id,
    this.name,
    this.categoryId,
    this.description,
    this.createdDate,
    this.range,
    this.price,
    this.userId,
    this.fuelTypeId,
    this.liters,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  final _i2.SortOrder? categoryId;

  final _i2.SortOrder? description;

  final _i2.SortOrder? createdDate;

  final _i2.SortOrder? range;

  final _i2.SortOrder? price;

  final _i2.SortOrder? userId;

  final _i2.SortOrder? fuelTypeId;

  final _i2.SortOrder? liters;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'category_id': categoryId,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'user_id': userId,
        'fuel_type_id': fuelTypeId,
        'liters': liters,
      };
}

class ExpensiveSumOrderByAggregateInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveSumOrderByAggregateInput({
    this.id,
    this.categoryId,
    this.range,
    this.price,
    this.userId,
    this.fuelTypeId,
    this.liters,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? categoryId;

  final _i2.SortOrder? range;

  final _i2.SortOrder? price;

  final _i2.SortOrder? userId;

  final _i2.SortOrder? fuelTypeId;

  final _i2.SortOrder? liters;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'category_id': categoryId,
        'range': range,
        'price': price,
        'user_id': userId,
        'fuel_type_id': fuelTypeId,
        'liters': liters,
      };
}

class ExpensiveOrderByWithAggregationInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveOrderByWithAggregationInput({
    this.id,
    this.name,
    this.categoryId,
    this.description,
    this.createdDate,
    this.range,
    this.price,
    this.userId,
    this.fuelTypeId,
    this.liters,
    this.$count,
    this.$avg,
    this.$max,
    this.$min,
    this.$sum,
  });

  final _i2.SortOrder? id;

  final _i2.SortOrder? name;

  final _i2.SortOrder? categoryId;

  final _i2.SortOrder? description;

  final _i2.SortOrder? createdDate;

  final _i2.SortOrder? range;

  final _i2.SortOrder? price;

  final _i2.SortOrder? userId;

  final _i1.PrismaUnion<_i2.SortOrder, _i2.SortOrderInput>? fuelTypeId;

  final _i1.PrismaUnion<_i2.SortOrder, _i2.SortOrderInput>? liters;

  final _i2.ExpensiveCountOrderByAggregateInput? $count;

  final _i2.ExpensiveAvgOrderByAggregateInput? $avg;

  final _i2.ExpensiveMaxOrderByAggregateInput? $max;

  final _i2.ExpensiveMinOrderByAggregateInput? $min;

  final _i2.ExpensiveSumOrderByAggregateInput? $sum;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'category_id': categoryId,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'user_id': userId,
        'fuel_type_id': fuelTypeId,
        'liters': liters,
        '_count': $count,
        '_avg': $avg,
        '_max': $max,
        '_min': $min,
        '_sum': $sum,
      };
}

class NestedIntNullableWithAggregatesFilter
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const NestedIntNullableWithAggregatesFilter({
    this.equals,
    this.$in,
    this.notIn,
    this.lt,
    this.lte,
    this.gt,
    this.gte,
    this.not,
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  final _i1.PrismaUnion<int, _i1.PrismaNull>? equals;

  final _i1.PrismaUnion<Iterable<int>, _i1.PrismaUnion<int, _i1.PrismaNull>>?
      $in;

  final _i1.PrismaUnion<Iterable<int>, _i1.PrismaUnion<int, _i1.PrismaNull>>?
      notIn;

  final int? lt;

  final int? lte;

  final int? gt;

  final int? gte;

  final _i1.PrismaUnion<
      int,
      _i1.PrismaUnion<_i2.NestedIntNullableWithAggregatesFilter,
          _i1.PrismaNull>>? not;

  final _i2.NestedIntNullableFilter? $count;

  final _i2.NestedFloatNullableFilter? $avg;

  final _i2.NestedIntNullableFilter? $sum;

  final _i2.NestedIntNullableFilter? $min;

  final _i2.NestedIntNullableFilter? $max;

  @override
  Map<String, dynamic> toJson() => {
        'equals': equals,
        'in': $in,
        'notIn': notIn,
        'lt': lt,
        'lte': lte,
        'gt': gt,
        'gte': gte,
        'not': not,
        '_count': $count,
        '_avg': $avg,
        '_sum': $sum,
        '_min': $min,
        '_max': $max,
      };
}

class IntNullableWithAggregatesFilter
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const IntNullableWithAggregatesFilter({
    this.equals,
    this.$in,
    this.notIn,
    this.lt,
    this.lte,
    this.gt,
    this.gte,
    this.not,
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  final _i1.PrismaUnion<int, _i1.PrismaNull>? equals;

  final _i1.PrismaUnion<Iterable<int>, _i1.PrismaUnion<int, _i1.PrismaNull>>?
      $in;

  final _i1.PrismaUnion<Iterable<int>, _i1.PrismaUnion<int, _i1.PrismaNull>>?
      notIn;

  final int? lt;

  final int? lte;

  final int? gt;

  final int? gte;

  final _i1.PrismaUnion<
      int,
      _i1.PrismaUnion<_i2.NestedIntNullableWithAggregatesFilter,
          _i1.PrismaNull>>? not;

  final _i2.NestedIntNullableFilter? $count;

  final _i2.NestedFloatNullableFilter? $avg;

  final _i2.NestedIntNullableFilter? $sum;

  final _i2.NestedIntNullableFilter? $min;

  final _i2.NestedIntNullableFilter? $max;

  @override
  Map<String, dynamic> toJson() => {
        'equals': equals,
        'in': $in,
        'notIn': notIn,
        'lt': lt,
        'lte': lte,
        'gt': gt,
        'gte': gte,
        'not': not,
        '_count': $count,
        '_avg': $avg,
        '_sum': $sum,
        '_min': $min,
        '_max': $max,
      };
}

class NestedFloatNullableWithAggregatesFilter
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const NestedFloatNullableWithAggregatesFilter({
    this.equals,
    this.$in,
    this.notIn,
    this.lt,
    this.lte,
    this.gt,
    this.gte,
    this.not,
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  final _i1.PrismaUnion<double, _i1.PrismaNull>? equals;

  final _i1
      .PrismaUnion<Iterable<double>, _i1.PrismaUnion<double, _i1.PrismaNull>>?
      $in;

  final _i1
      .PrismaUnion<Iterable<double>, _i1.PrismaUnion<double, _i1.PrismaNull>>?
      notIn;

  final double? lt;

  final double? lte;

  final double? gt;

  final double? gte;

  final _i1.PrismaUnion<
      double,
      _i1.PrismaUnion<_i2.NestedFloatNullableWithAggregatesFilter,
          _i1.PrismaNull>>? not;

  final _i2.NestedIntNullableFilter? $count;

  final _i2.NestedFloatNullableFilter? $avg;

  final _i2.NestedFloatNullableFilter? $sum;

  final _i2.NestedFloatNullableFilter? $min;

  final _i2.NestedFloatNullableFilter? $max;

  @override
  Map<String, dynamic> toJson() => {
        'equals': equals,
        'in': $in,
        'notIn': notIn,
        'lt': lt,
        'lte': lte,
        'gt': gt,
        'gte': gte,
        'not': not,
        '_count': $count,
        '_avg': $avg,
        '_sum': $sum,
        '_min': $min,
        '_max': $max,
      };
}

class FloatNullableWithAggregatesFilter
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const FloatNullableWithAggregatesFilter({
    this.equals,
    this.$in,
    this.notIn,
    this.lt,
    this.lte,
    this.gt,
    this.gte,
    this.not,
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  final _i1.PrismaUnion<double, _i1.PrismaNull>? equals;

  final _i1
      .PrismaUnion<Iterable<double>, _i1.PrismaUnion<double, _i1.PrismaNull>>?
      $in;

  final _i1
      .PrismaUnion<Iterable<double>, _i1.PrismaUnion<double, _i1.PrismaNull>>?
      notIn;

  final double? lt;

  final double? lte;

  final double? gt;

  final double? gte;

  final _i1.PrismaUnion<
      double,
      _i1.PrismaUnion<_i2.NestedFloatNullableWithAggregatesFilter,
          _i1.PrismaNull>>? not;

  final _i2.NestedIntNullableFilter? $count;

  final _i2.NestedFloatNullableFilter? $avg;

  final _i2.NestedFloatNullableFilter? $sum;

  final _i2.NestedFloatNullableFilter? $min;

  final _i2.NestedFloatNullableFilter? $max;

  @override
  Map<String, dynamic> toJson() => {
        'equals': equals,
        'in': $in,
        'notIn': notIn,
        'lt': lt,
        'lte': lte,
        'gt': gt,
        'gte': gte,
        'not': not,
        '_count': $count,
        '_avg': $avg,
        '_sum': $sum,
        '_min': $min,
        '_max': $max,
      };
}

class ExpensiveScalarWhereWithAggregatesInput
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveScalarWhereWithAggregatesInput({
    this.AND,
    this.OR,
    this.NOT,
    this.id,
    this.name,
    this.categoryId,
    this.description,
    this.createdDate,
    this.range,
    this.price,
    this.userId,
    this.fuelTypeId,
    this.liters,
  });

  final _i1.PrismaUnion<_i2.ExpensiveScalarWhereWithAggregatesInput,
      Iterable<_i2.ExpensiveScalarWhereWithAggregatesInput>>? AND;

  final Iterable<_i2.ExpensiveScalarWhereWithAggregatesInput>? OR;

  final _i1.PrismaUnion<_i2.ExpensiveScalarWhereWithAggregatesInput,
      Iterable<_i2.ExpensiveScalarWhereWithAggregatesInput>>? NOT;

  final _i1.PrismaUnion<_i2.IntWithAggregatesFilter, int>? id;

  final _i1.PrismaUnion<_i2.StringWithAggregatesFilter, String>? name;

  final _i1.PrismaUnion<_i2.IntWithAggregatesFilter, int>? categoryId;

  final _i1.PrismaUnion<_i2.StringWithAggregatesFilter, String>? description;

  final _i1.PrismaUnion<_i2.DateTimeWithAggregatesFilter, DateTime>?
      createdDate;

  final _i1.PrismaUnion<_i2.FloatWithAggregatesFilter, double>? range;

  final _i1.PrismaUnion<_i2.FloatWithAggregatesFilter, double>? price;

  final _i1.PrismaUnion<_i2.IntWithAggregatesFilter, int>? userId;

  final _i1.PrismaUnion<_i2.IntNullableWithAggregatesFilter,
      _i1.PrismaUnion<int, _i1.PrismaNull>>? fuelTypeId;

  final _i1.PrismaUnion<_i2.FloatNullableWithAggregatesFilter,
      _i1.PrismaUnion<double, _i1.PrismaNull>>? liters;

  @override
  Map<String, dynamic> toJson() => {
        'AND': AND,
        'OR': OR,
        'NOT': NOT,
        'id': id,
        'name': name,
        'category_id': categoryId,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'user_id': userId,
        'fuel_type_id': fuelTypeId,
        'liters': liters,
      };
}

class ExpensiveCountAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveCountAggregateOutputTypeSelect({
    this.id,
    this.name,
    this.categoryId,
    this.description,
    this.createdDate,
    this.range,
    this.price,
    this.userId,
    this.fuelTypeId,
    this.liters,
    this.$all,
  });

  final bool? id;

  final bool? name;

  final bool? categoryId;

  final bool? description;

  final bool? createdDate;

  final bool? range;

  final bool? price;

  final bool? userId;

  final bool? fuelTypeId;

  final bool? liters;

  final bool? $all;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'category_id': categoryId,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'user_id': userId,
        'fuel_type_id': fuelTypeId,
        'liters': liters,
        '_all': $all,
      };
}

class ExpensiveGroupByOutputTypeCountArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveGroupByOutputTypeCountArgs({this.select});

  final _i2.ExpensiveCountAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class ExpensiveAvgAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveAvgAggregateOutputTypeSelect({
    this.id,
    this.categoryId,
    this.range,
    this.price,
    this.userId,
    this.fuelTypeId,
    this.liters,
  });

  final bool? id;

  final bool? categoryId;

  final bool? range;

  final bool? price;

  final bool? userId;

  final bool? fuelTypeId;

  final bool? liters;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'category_id': categoryId,
        'range': range,
        'price': price,
        'user_id': userId,
        'fuel_type_id': fuelTypeId,
        'liters': liters,
      };
}

class ExpensiveGroupByOutputTypeAvgArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveGroupByOutputTypeAvgArgs({this.select});

  final _i2.ExpensiveAvgAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class ExpensiveSumAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveSumAggregateOutputTypeSelect({
    this.id,
    this.categoryId,
    this.range,
    this.price,
    this.userId,
    this.fuelTypeId,
    this.liters,
  });

  final bool? id;

  final bool? categoryId;

  final bool? range;

  final bool? price;

  final bool? userId;

  final bool? fuelTypeId;

  final bool? liters;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'category_id': categoryId,
        'range': range,
        'price': price,
        'user_id': userId,
        'fuel_type_id': fuelTypeId,
        'liters': liters,
      };
}

class ExpensiveGroupByOutputTypeSumArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveGroupByOutputTypeSumArgs({this.select});

  final _i2.ExpensiveSumAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class ExpensiveMinAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveMinAggregateOutputTypeSelect({
    this.id,
    this.name,
    this.categoryId,
    this.description,
    this.createdDate,
    this.range,
    this.price,
    this.userId,
    this.fuelTypeId,
    this.liters,
  });

  final bool? id;

  final bool? name;

  final bool? categoryId;

  final bool? description;

  final bool? createdDate;

  final bool? range;

  final bool? price;

  final bool? userId;

  final bool? fuelTypeId;

  final bool? liters;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'category_id': categoryId,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'user_id': userId,
        'fuel_type_id': fuelTypeId,
        'liters': liters,
      };
}

class ExpensiveGroupByOutputTypeMinArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveGroupByOutputTypeMinArgs({this.select});

  final _i2.ExpensiveMinAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class ExpensiveMaxAggregateOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveMaxAggregateOutputTypeSelect({
    this.id,
    this.name,
    this.categoryId,
    this.description,
    this.createdDate,
    this.range,
    this.price,
    this.userId,
    this.fuelTypeId,
    this.liters,
  });

  final bool? id;

  final bool? name;

  final bool? categoryId;

  final bool? description;

  final bool? createdDate;

  final bool? range;

  final bool? price;

  final bool? userId;

  final bool? fuelTypeId;

  final bool? liters;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'category_id': categoryId,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'user_id': userId,
        'fuel_type_id': fuelTypeId,
        'liters': liters,
      };
}

class ExpensiveGroupByOutputTypeMaxArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveGroupByOutputTypeMaxArgs({this.select});

  final _i2.ExpensiveMaxAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class ExpensiveGroupByOutputTypeSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const ExpensiveGroupByOutputTypeSelect({
    this.id,
    this.name,
    this.categoryId,
    this.description,
    this.createdDate,
    this.range,
    this.price,
    this.userId,
    this.fuelTypeId,
    this.liters,
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  final bool? id;

  final bool? name;

  final bool? categoryId;

  final bool? description;

  final bool? createdDate;

  final bool? range;

  final bool? price;

  final bool? userId;

  final bool? fuelTypeId;

  final bool? liters;

  final _i1.PrismaUnion<bool, _i2.ExpensiveGroupByOutputTypeCountArgs>? $count;

  final _i1.PrismaUnion<bool, _i2.ExpensiveGroupByOutputTypeAvgArgs>? $avg;

  final _i1.PrismaUnion<bool, _i2.ExpensiveGroupByOutputTypeSumArgs>? $sum;

  final _i1.PrismaUnion<bool, _i2.ExpensiveGroupByOutputTypeMinArgs>? $min;

  final _i1.PrismaUnion<bool, _i2.ExpensiveGroupByOutputTypeMaxArgs>? $max;

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'category_id': categoryId,
        'description': description,
        'created_date': createdDate,
        'range': range,
        'price': price,
        'user_id': userId,
        'fuel_type_id': fuelTypeId,
        'liters': liters,
        '_count': $count,
        '_avg': $avg,
        '_sum': $sum,
        '_min': $min,
        '_max': $max,
      };
}

class AggregateExpensive {
  const AggregateExpensive({
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  factory AggregateExpensive.fromJson(Map json) => AggregateExpensive(
        $count: json['_count'] is Map
            ? _i2.ExpensiveCountAggregateOutputType.fromJson(json['_count'])
            : null,
        $avg: json['_avg'] is Map
            ? _i2.ExpensiveAvgAggregateOutputType.fromJson(json['_avg'])
            : null,
        $sum: json['_sum'] is Map
            ? _i2.ExpensiveSumAggregateOutputType.fromJson(json['_sum'])
            : null,
        $min: json['_min'] is Map
            ? _i2.ExpensiveMinAggregateOutputType.fromJson(json['_min'])
            : null,
        $max: json['_max'] is Map
            ? _i2.ExpensiveMaxAggregateOutputType.fromJson(json['_max'])
            : null,
      );

  final _i2.ExpensiveCountAggregateOutputType? $count;

  final _i2.ExpensiveAvgAggregateOutputType? $avg;

  final _i2.ExpensiveSumAggregateOutputType? $sum;

  final _i2.ExpensiveMinAggregateOutputType? $min;

  final _i2.ExpensiveMaxAggregateOutputType? $max;

  Map<String, dynamic> toJson() => {
        '_count': $count?.toJson(),
        '_avg': $avg?.toJson(),
        '_sum': $sum?.toJson(),
        '_min': $min?.toJson(),
        '_max': $max?.toJson(),
      };
}

class AggregateExpensiveCountArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateExpensiveCountArgs({this.select});

  final _i2.ExpensiveCountAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateExpensiveAvgArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateExpensiveAvgArgs({this.select});

  final _i2.ExpensiveAvgAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateExpensiveSumArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateExpensiveSumArgs({this.select});

  final _i2.ExpensiveSumAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateExpensiveMinArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateExpensiveMinArgs({this.select});

  final _i2.ExpensiveMinAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateExpensiveMaxArgs
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateExpensiveMaxArgs({this.select});

  final _i2.ExpensiveMaxAggregateOutputTypeSelect? select;

  @override
  Map<String, dynamic> toJson() => {'select': select};
}

class AggregateExpensiveSelect
    implements _i1.JsonConvertible<Map<String, dynamic>> {
  const AggregateExpensiveSelect({
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  final _i1.PrismaUnion<bool, _i2.AggregateExpensiveCountArgs>? $count;

  final _i1.PrismaUnion<bool, _i2.AggregateExpensiveAvgArgs>? $avg;

  final _i1.PrismaUnion<bool, _i2.AggregateExpensiveSumArgs>? $sum;

  final _i1.PrismaUnion<bool, _i2.AggregateExpensiveMinArgs>? $min;

  final _i1.PrismaUnion<bool, _i2.AggregateExpensiveMaxArgs>? $max;

  @override
  Map<String, dynamic> toJson() => {
        '_count': $count,
        '_avg': $avg,
        '_sum': $sum,
        '_min': $min,
        '_max': $max,
      };
}
